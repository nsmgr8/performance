import subprocess
import logging
import time
import re

import sh

from appliance.dnsbox200 import DNSbox200
from performance.models import TestRun

logger = logging.getLogger('dhcperf')
logger.setLevel(logging.DEBUG)

DHCPERF_BIN = '/usr/local/nom/bin/dhcperf'


class Dhcperf(object):
    CLIENTS_POOL = [2 ** i for i in range(8, 20)]
    HWM_RE = re.compile(r'High-water mark is (\d+) clients/second.')
    DISCOVER_RE = re.compile(r'(\d+) five-way handshakes per second.')
    RENEW_RE = re.compile(r'(\d+) DHCPRENEW transactions per second.')

    def __init__(self, config=None):
        self.config = config or {}

    def main(self):
        self.setup_dnsbox()

        output = []
        for netmask in range(24, 15, -1):
            max_clients = self.d200.dhcpd_setup(netmask)
            pool = {
                'pool': {
                    'low': self.d200.pool.low,
                    'high': self.d200.pool.high,
                    'size': self.d200.pool.size,
                },
                'tests': [],
            }
            output.append(pool)
            for clients in [c for c in self.CLIENTS_POOL if c <= max_clients]:
                runs = []
                pool['tests'].append({'clients': clients, 'runs': runs})
                for i in range(self.config.get('run_per_setup', 10)):
                    discover = self.run('discover', clients)
                    renew = self.run('renew', clients)

                    runs.append({
                        'discover': discover,
                        'renew': renew,
                    })

                    try:
                        run = TestRun.objects.get(pk=self.config['run_pk'])
                        run.script_outputs = output
                        run.save()
                    except:
                        logger.exception('Could not save dhcperf output')

        try:
            self.d200.get_results(path=self.config['outdir'])
        except:
            logger.exception('Could not get CACHEbox data')
        return output

    def _get_value_from_re(self, regex, output):
        m = regex.search(output)
        if m:
            return int(m.groups()[0])
        return -1

    def _get_hwm(self, output):
        return self._get_value_from_re(self.HWM_RE, output)

    def _get_renew(self, output):
        return self._get_value_from_re(self.RENEW_RE, output)

    def _get_discover(self, output):
        return self._get_value_from_re(self.DISCOVER_RE, output)

    def setup_dnsbox(self):
        logger.info('Setup DNSbox200')
        self.d200 = DNSbox200(self.config['ip'], self.config['port'])

    def run(self, mode, clients):
        logger.info('Running DHCP {} for {} clients'.format(mode, clients))
        p = subprocess.Popen([DHCPERF_BIN,
                              '--port', str(self.config['port']),
                              '--server', self.config['ip'],
                              '--clients', str(clients),
                              '--{}'.format(mode)],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        stdout = p.stdout.read()
        stderr = p.stderr.read()
        hwm, qps = -1, -1
        if stderr:
            output = stderr
        elif stdout:
            hwm = self._get_hwm(stdout)
            qps = {
                'discover': self._get_discover,
                'renew': self._get_renew,
            }.get(mode, lambda x: -1)(stdout)
            output = stdout.splitlines()[-1].strip()
        else:
            output = 'dhcperf unknown error: no stdout/stderr'

        logger.info(output)
        time.sleep(30)
        return {'stdout': stdout, 'stderr': stderr, 'hwm': hwm, 'qps': qps}

    @staticmethod
    def stop_dhcperf():
        sh.killall('dhcperf')
        sh.sleep('3')
