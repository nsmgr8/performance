#!/usr/bin/env python

"""
Report summary generation classes
=================================
"""

import sys
import os
import re

from django.template.loader import render_to_string


class ParsePolygraphError(Exception):
    pass


def re_find(data, regex):
    r = re.compile(regex, re.DOTALL)
    return r.findall(data)


class ParserBase(object):
    """
    A simple base class to load the file
    data.
    """
    def __init__(self, filename):
        self.filename = filename
        with open(self.filename) as fh:
            self.data = fh.read()


class Phase(ParserBase):
    """
    This class represents a 'phase' of a polygraph test. Each
    phase is reported in a separate HTML file which is divided
    into sections using <h2> tags. The actual data is displayed
    using <span> tags with a title attribute acting as a label.

    We parse the whole phase, creating a self.sections dictionary
    keyed on the name of the <h2> sections (e.g. 'Load', 'Errors').

    The values in the dictionary are themselves dictionaries of
    key-value pairs extraced from the <span> tags: key=title and
    value=contents.
    """
    def __init__(self, filename, name):
        super(Phase, self).__init__(filename)
        self.name = name
        self.sections = {}

        bits = self.data.split("<h2")
        for bit in bits:
            name = re_find(bit, "^[^<]*<a[^<]*>([^<]+)</a></h2>")
            if not name:
                continue
            name = name[0]

            my_data = {}
            span_info = re_find(bit, '<span.*?title="([^"]+)">(.*?)</span>')
            for k, v in span_info:
                my_data[k] = v

            self.sections[name] = my_data

    def summary(self):
        for i in ('Load', 'Errors', 'Client Side Hit Ratios'):
            print('{} {}'.format(i, self.sections[i]))

    def html_row(self):
        """
        Returns an HTML row designed to work in the table created
        by the 'Everything' class's html_summary method.
        """
        errors = float(
            self.sections.get("Errors", {}).get(
                "portion of erroneous transactions", 0))

        if errors < 0.1:
            tr_class = "ok"
        else:
            tr_class = "error"

        html = "<tr class=\"%s\">\n<td>%s</td>\n" % (tr_class, self.name,)

        for s, k in self.summary_keys():
            html += "<td>%s</td>\n" % (self.sections.get(s, {}).get(k),)

        html += "</tr>\n"

        return html

    def error(self):
        return float(self.sections.get("Errors", {}).get(
            "portion of erroneous transactions", 0))

    def summary_keys(self):
        return (
            ('Load', 'offered request rate'),
            ('Load', 'measured response rate'),
            ('Load', 'request bandwidth'),
            ('Load', 'response bandwidth'),
            ('Errors', 'portion of erroneous transactions'),
            ('Client Side Hit Ratios', 'offered byte hit ratio'),
            ('Client Side Hit Ratios', 'measured byte hit ratio'),
            ('Client Side Hit Ratios', 'offered document hit ratio'),
            ('Client Side Hit Ratios', 'measured document hit ratio'),
        )

    def summary_values(self):
        return [self.sections.get(s, {}).get(k, '')
                for s, k in self.summary_keys()]


class Everything(ParserBase):
    """
    The top-level file in the polygraph report output is called
    'everything.html'. This file contains links to each of the
    phases, in the correct order.

    We parse this file to find the names and html report files of
    all the phases, which we then load using the Phase class.
    """
    def __init__(self, filename):
        super(Everything, self).__init__(filename)

        path = os.path.dirname(filename)
        self.name = os.path.split(filename)[-2]

        load_first_row = re_find(self.data, "<h2>1. Load</h2>.*?<th>client side</th>(.*?)</tr>")
        assert len(load_first_row) == 1

        phase_links = re_find(load_first_row[0], '<a href="(.*?)">(.*?)</a>')

        self.phases = []
        for link, name in phase_links:
            if name == "all phases":
                # This is known to be missing normally
                continue

            fn, junk = link.split("#", 1)
            full_fn = os.path.join(path, fn)
            phase = Phase(full_fn, name)
            self.phases.append(phase)

    def summary(self):
        for i in self.phases:
            print(i.summary())

    def html_summary(self):
        """
        Returns an HTML page containing the extracted
        information from all the phases in a single table.

        Includes a link to the full polygraph report file
        (everything.html).
        """
        results_base = os.path.basename(os.path.dirname(self.filename))
        data = ''
        for ph in self.phases:
            data += ph.html_row()
        context = {'results_base': results_base, 'phases': self.phases,
                   'name': os.path.basename(self.name)}
        return render_to_string('performance/polyrun_summary.html', context)

    def list_summary(self):
        summary = []
        for phase in self.phases:
            values = [float(v or 0) for v in phase.summary_values()]
            summary.append({
                'phase': phase.name,
                'rps': {
                    'offered': values[0],
                    'measured': values[1],
                },
                'mbps': {
                    'offered': values[2],
                    'measured': values[3],
                },
                'errors': values[4],
                'bhr': {
                    'offered': values[5],
                    'measured': values[6],
                },
                'dhr': {
                    'offered': values[7],
                    'measured': values[8],
                },
            })
        return summary


def main(folder, output):
    """
    Takes a folder (as generated by polygraph-report) and
    creates an HTML summary in file 'output'.

    @folder: polygraph report folder
    @output: file to write summary to
    """
    if not os.path.isdir(folder):
        raise ParsePolygraphError("target must be a folder")

    everything = Everything(os.path.join(folder,
                                         "everything.html"))

    html = everything.html_summary()
    open(output, 'w').write(html)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: %s folder output" % (sys.argv[0],))
        sys.exit(1)

    target = sys.argv[1]
    output = sys.argv[2]
    main(target, output)
