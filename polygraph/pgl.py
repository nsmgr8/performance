"""
PGL helper classes
==================

Classes for autogenerating snippets of (web polygraph) PGL code.
"""

import string
import datetime

__ALL__ = ["StaticPglPhase", "RampPglPhase", "IdlePglPhase",
           "ConstantPglPhase", "PglSchedule"]
REPORT_GENERATION_DELAY = 3


class PglPhase(object):
    """
    Phase definition for a PGL
    """

    def __init__(self, name):
        self.name = name
        self.phname = 'ph%s' % name.title().replace(' ', '')

    def prefix(self):
        return "Phase %s = {" % self.phname

    def suffix(self):
        return "};"

    def lines(self):
        raise NotImplementedError("must be overridden")

    def render(self):
        content = ['    %s' % line for line in self.lines()]
        return '\n'.join([self.prefix(), '\n'.join(content), self.suffix()])

    def __repr__(self):
        return "Phase base class - method should be overridden"


class StaticPglPhase(PglPhase):
    """
    simply wait at current load behaviour for {dur} minutes
    """
    def __init__(self, name, dur):
        super(StaticPglPhase, self).__init__(name)
        self.duration = dur

    def lines(self):
        return ['name = "%s";' % self.name,
                "goal.duration = %smin;" % self.duration,
               ]

    def __repr__(self):
        return "Static phase %s: %s minutes" % (self.name, self.duration)


class IdlePglPhase(PglPhase):
    """
    set a very low request rate for {dur} minutes
    don't use zero load to keep the servers from timing out
    """
    def __init__(self, name, dur):
        super(IdlePglPhase, self).__init__(name)
        self.duration = dur

    def lines(self):
        return ['name = "%s";' % self.name,
                "populus_factor_beg = 1.0/count(R.addresses);",
                "goal.duration = %smin;" % self.duration,
               ]

    def __repr__(self):
        return "Idle phase %s: %s minutes" % (self.name, self.duration)


class FillPgl(object):
    """
    :param: cache_size_gb (10)
    :param: fill_rate (750rps)
    """

    def __init__(self, cache_size_gb, fill_rate=750):
        self.cache_size_gb = cache_size_gb
        self.fill_rate = fill_rate
        self.phase_names = ["phFRamp",
                            "phFill",
                            "phFExit"]
        # (Rough) estimated duration
        # 10 min ramp up and down
        # fill the cache (twice over) using av. 28KB objects
        mbps = int(fill_rate) * 0.028  # MB/s
        time_to_fill = 2 * (float(cache_size_gb) * 1024 / mbps)
        self.duration = int(10 + time_to_fill + 10) / 60

    def render(self):
        fill_template = '''
time rampDur = 10min; // ramp phases duration    (and idle phase)
// limit the growth of the URL "working set" size (WSS)
// to 2 * cache size / av obj size
////int wsc = int(2 * ${cache_size}GB / 28KB);
int clientHostCount = clientHostCount(TheBench);
////working_set_cap(wsc / clientHostCount);

float smallFactor = 1.0/count(R.addresses);

Phase phFRamp = {
  name = "framp";
  goal.duration = rampDur;
  recur_factor_beg = 5%/55%;
  special_msg_factor_beg = 0.1;
  populus_factor_beg = smallFactor;
  populus_factor_end = (${fill_rate}/sec)/TheBench.peak_req_rate;
};

Phase phFill = {
  name = "fill";
  goal.fill_size = 2*(${cache_size}GB) / clientHostCount;
  ////wait_wss_freeze = yes; // will finish only if WSS is frozen
};

Phase phFExit = {
  name = "fexit";
  goal.duration = rampDur;
  recur_factor_end = 1;
  special_msg_factor_end = 1;
  populus_factor_end = smallFactor;
};
'''

        t = string.Template(fill_template)
        return t.substitute({"cache_size": self.cache_size_gb,
                             "fill_rate": self.fill_rate})

    def __repr__(self):
        return "Fill phase: %s GB cache size, %s RPS fill rate" % (
            self.cache_size_gb,
            self.fill_rate)


class ConstantPglPhase(PglPhase):
    """
    set load to that given {load}; keep their for
    {dur} minutes.
    """
    def __init__(self, name, dur, load):
        super(ConstantPglPhase, self).__init__(name)
        self.duration = dur
        self.load = float(load)

    def lines(self):
        if self.load > 1.0:
            load_str = '(%s/sec)/TheBench.peak_req_rate;' % self.load
        else:
            load_str = '%.2f;' % self.load
        return ['name = "%s";' % self.name,
                'populus_factor_beg = %s;' % load_str,
                'populus_factor_end = %s;' % load_str,
                "goal.duration = %smin;" % self.duration,
               ]

    def __repr__(self):
        return "Constant phase %s: %s RPS for %s minutes" % (
            self.name,
            self.load,
            self.duration)


class RampPglPhase(PglPhase):
    """
    ramp from previous load level to given {load} over
    {dur} minutes.
    """
    def __init__(self, name, dur, load):
        super(RampPglPhase, self).__init__(name)
        self.duration = dur
        if ':' in load:
            self.start, self.end = (float(x) for x in load.split(':'))
        else:
            self.start = 0
            self.end = float(load)

    def lines(self):
        if self.start > 1.0:
            start_line = 'populus_factor_beg = (%s/sec)/TheBench.peak_req_rate;' % self.start
        else:
            start_line = 'populus_factor_beg = %.2f;' % self.start
        if self.end > 1.0:
            end_line = 'populus_factor_end = (%s/sec)/TheBench.peak_req_rate;' % self.end
        else:
            end_line = 'populus_factor_end = %.2f;' % self.end
        return ['name = "%s";' % self.name,
                start_line,
                end_line,
                "goal.duration = %smin;" % self.duration,
               ]

    def __repr__(self):
        return "Ramp phase %s: %s to %s RPS over %s minutes" % (
            self.name,
            self.start,
            self.end,
            self.duration)


class PglSchedule(list):
    """
    This is simply a list which contains PglPhase elements.
    It provides a render method which returns the PGL code
    for all the contained phases plus the complete schedule.
    """

    def __init__(self, *args):
        self.fill_pgl = None
        super(PglSchedule, self).__init__(*args)

    def include_fill(self, fill_pgl):
        """
        Add the special fill pgl object
        """
        self.fill_pgl = fill_pgl

    def render(self):
        if self.fill_pgl:
            schedule = self.fill_pgl.phase_names
            lines = [self.fill_pgl.render()]
        else:
            lines = []
            schedule = []

        if self:
            for phase in self:
                lines.append(phase.render())
                schedule.append(phase.phname)
            if not any('populus_factor_beg' in line for line in self[0].lines()):
                # ensure we start at low-load
                lines.append("%s.populus_factor_beg = 1.0/count(R.addresses);" % self[0].phname)
            lines.append("schedule(%s);" % ','.join(schedule))
        return '\n'.join(lines) + '\n'

    def eta(self):
        """
        Estimated time to complete
        """
        eta = datetime.datetime.now()
        for ph in [self.fill_pgl] + self:
            if not ph:
                continue

            eta += datetime.timedelta(seconds=int(ph.duration) * 60)

        eta += datetime.timedelta(minutes=REPORT_GENERATION_DELAY)
        return eta

    def __repr__(self):
        ret = "Schedule:\n"
        dur = REPORT_GENERATION_DELAY
        for ph in [self.fill_pgl] + self:
            if not ph:
                continue

            dur += int(ph.duration)
            ret += "\t%s minutes:\t%s\n" % (ph.duration, repr(ph))

        ret += "\t%s minutes: report generation" % (REPORT_GENERATION_DELAY,)

        dur_hours = int(dur / 60)
        dur_mins = dur - dur_hours * 60

        ret += "\nTotal estimated duration: %s hours %s minutes\n" % (dur_hours, dur_mins)

        return ret
