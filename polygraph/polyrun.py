#!/usr/bin/env python
"""
polyrun - automatic running of polygraph tests
==============================================

Supports running polygraph tests and collecting results.

Copyright (c) ApplianSys Ltd 2011-13. All rights reserved.


Expected configuration::

    polyclient(s) <-> cachebox <-> polyserver(s)

All of these should be on the 10.0.0.0/24 network

Also require a DNS server (which will typically be one of the polygraph
machines) and an NTP timeserver.

polyrun assumes that the routing has been setup already; basically static
routes from 10.X.0.0/16 to 10.0.0.X (where the latter is the polygraph
machine name) are configured on the cachebox; also that the cachebox's
DNS server is configured to point to the polygraph DNS server.

A polygraph user is present on each of the polygraph machines -
'polyrun' here. This must have nopasswd sudo access, i.e. the following in
the sudoers file of each polygraph machine::

    polyrun ALL=NOPASSWD: ALL

The test itself consists of setting up the cachebox (optionally deleting
all cached objects and restarting it), creating a 'bundle' of polygraph
scripts, which are copied to each polygraph machine, running the polygraph
process, then finally collecting results from the polygraph machines and
cachebox.

Usage::

    polyrun.py [--cache-init=<state>] [--proxy-type=<type>] \
[--verbose] [--cachebox-ip=<ip>] [--polysrv-ips=<ips>] [--polyclt-ips=<ips>] \
[--cache-size=<GB>] [--fill-phase] [--fill-rate=<rps>] [--timeupdate] \
[--recipe=<name>] [--nameserver=<ip>] [--timeserver=<ip>] \
[--duration=<seconds>] --schedule-type=<type> --schedule-value=<value> \
--outdir=<location> [--name=<name>] [--test-cdn=<bool>]
    polyrun.py [--verbose] [--name=<name>] --last-report --outdir=<location> \
[--cachebox-ip=<ip>]

Options::

    -h --help                   Show this screen
    --version                   Show version
    --verbose                   Verbose output
    --name=<name>               Name the test run [default: polyrun]
    --cache-init=<state>        Initial state of the cachebox
                                    - quickstart: keep state as is
                                    - init: Initialise the cachebox
                                    - reload: Restart squid
                                    - non-cachebox: Not a cachebox
                                [default: quickstart]
    --proxy-type=<type>         Deployment type of proxy
                                    - explicit: explicit proxy
                                    - transparent: transparent proxy
                                    - bridge: bridge mode
                                    - bypass: no-proxy
                                [default: explicit]
    --cachebox-ip=<ip>          CACHEbox IP address [default: 10.0.0.50]
    --polysrv-ips=<ips>         Comma-separated Polygraph server IPS
                                [default: 10.0.0.14,10.0.0.15]
    --polyclt-ips=<ips>         Comma-separated Polygraph client IPS
                                [default: 10.0.0.12,10.0.0.13]
    --nameserver=<ip>           DNS server IP [default: 10.0.0.14]
    --timeserver=<ip>           Time server IP [default: 10.0.0.27]
    --timeupdate                Update polygraphs time
    --duration=<seconds>        Duration in seconds [default: 10]
    --schedule-type=<type>      One of ramp, constant or steps
    --schedule-value=<value>    RPS value for schedule
                                    - an integer for constant
                                    - [start]:end for ramp
                                    - start:end:stepsize for steps
    --cache-size=<GB>           Size of the cache storage in GB [default: 100]
    --fill-phase                Include a fill phase
    --fill-rate=<rps>           RPS at which fill is performed [default: 750]
    --recipe=<name>             Polyrun schedule recipe filename
                                [default: polymix-isp-dynamic.pg]
    --outdir=<location>         Output folder
    --last-report               Fetch last run logs and generate report
    --test-cdn=<bool>           Flag to enable a dummy CDN
                                [default: 0]
"""

import os
import time
import logging
import shutil
import tarfile
import tempfile
from string import Template
from glob import glob
from tempfile import NamedTemporaryFile

import sh

from . import pgl
from appliance.cachebox import CACHEbox
from .report_summary import Everything
from remote import ssh


__version__ = 'polyrun-1.0'

POLYGRAPH_USERNAME = 'polyrun'
""" polygraph machines username """

WORKLOADS_PATH = '/home/polyrun/polygraph,\
/usr/share/polygraph/workloads/include'
""" polygraph workload file search path """

LOG_LOC = '/var/log/polygraph/polyrun'
""" log files location """

QUIET = True
SCHEDULE_MAX_INTERVAL = 100


logger = logging.getLogger('polyrun')
logger.setLevel(logging.DEBUG)


def get_polymix_recipe(proxy_type):
    if proxy_type == 'reverse':
        return 'reverse-polymix-recipe.pg'
    else:
        return 'polymix-recipe.pg'


def log(message, host, level='info'):
    """
    A convenience function for logging

    :param message: the message to log
    :param level: log level, info by default
    """
    logfunc = {
        'info': logger.info,
        'debug': logger.debug,
        'warning': logger.warning,
        'error': logger.error
    }.get(level, logger.info)
    logfunc('{0}: {1}'.format(host, message))


def init_polygraph(config, bundle, servers, clients, host):
    """
    Initialise polygraph machines by clearing previous workloads,
    updating time, stopping polygraph servers/clients.

    :param config: the configuration dict
    :param bundle: the polygraph workload bundle full-path
    """
    with ssh.SSHConnection(address=host) as ssh_client:
        log('Initialising polygraph', host)
        log('Syncing time on polygraph', host)
        ssh_client.sudo('ntpdate -bu {0}'.format(config['timeserver']))

        ssh_client.run('rm -rf /tmp/* /home/polyrun/polygraph')
        bundle_fname = os.path.basename(bundle)
        ssh_client.put(bundle, bundle_fname)
        ssh_client.run('tar -xf {0}'.format(bundle_fname))

        if host in servers:
            ssh_client.sudo('killall polygraph-server')
        if host in clients:
            ssh_client.sudo('killall polygraph-client')
        time.sleep(3)


def reboot_polygraph(host):
    """
    Reboot polygraph machine
    """
    ip = host.split('@')[1]
    with ssh.SSHConnection(address=host) as ssh_client:
        ssh_client.sudo('/sbin/reboot')
    log('Rebooting', host)
    time.sleep(30)
    while True:
        log('Waiting for reboot', host)
        try:
            sh.ping('-c1', '-w1', ip)
            log('Rebooted', host)
            break
        except:
            time.sleep(10)


def stop_polygraph(host):
    """
    Stop all running polygraph
    """
    log('Stop polygraph', host)
    with ssh.SSHConnection(address=host) as ssh_client:
        ssh_client.sudo('killall polygraph-server')
        ssh_client.sudo('killall polygraph-client')
    time.sleep(3)


def run_polygraph(servers, clients, config, host):
    """
    Run polygraph

    :param servers: list of polygraph servers
    :param clients: list of polygraph clients
    """
    with ssh.SSHConnection(address=host) as ssh_client:
        log('Running polygraph', host)
        if config['proxy_type'] == 'transparent':
            gateway = config['cachebox_ip']
        else:
            gateway = None
        hostname, _ = ssh_client.run('hostname')

        log_f = '{0}/polyrun-{1}-{2}.log'.format(LOG_LOC, hostname,
                                                 config['name'])
        console_f = '{0}/stdout-{1}-{2}.log'.format(LOG_LOC, hostname,
                                                    config['name'])
        console_ln = '{0}/stdout-{1}.log'.format(LOG_LOC, hostname)

        polymix_recipe = get_polymix_recipe(config["proxy_type"])

        if host in servers:
            add_routes(clients, gateway, host)
            pg_start = ('polygraph-server',
                        '--config', polymix_recipe,
                        '--cfg_dirs', WORKLOADS_PATH,
                        '--verb_lvl 10',
                        '--log', log_f,
                        '--console', console_f,
                        ' --accept_foreign_msgs yes')
        elif host in clients:
            add_routes(servers, gateway, host)
            if config['proxy_type'] == 'explicit':
                proxy = '--proxy {0}:800'.format(config['cachebox_ip'])
            else:
                proxy = ''
            pg_start = ('polygraph-client',
                        '--config', polymix_recipe,
                        '--cfg_dirs', WORKLOADS_PATH,
                        '--verb_lvl 10',
                        '--log', log_f,
                        '--console', console_f,
                        proxy)
            time.sleep(10)
        else:
            return

        if config.get('repeatable'):
            seed = host.split('.')[-1]
            pg_start = pg_start + ('--unique_world', 'no',
                                   '--local_rng_seed', seed)

        ssh_client.run('touch {0} {1}'.format(log_f, console_f))
        ssh_client.run('ln -fs {0} {1}'.format(console_f, console_ln))
        ssh_client.sudo(' '.join(pg_start))


def get_logs(config, host):
    """
    Collect polygraph logs

    :param config: the configuration dict
    """
    log('Getting polygraph log', host)
    path = config['outdir']
    with ssh.SSHConnection(address=host) as ssh_client:
        log_files = '{0}/polyrun-*{1}.log'.format(LOG_LOC, config['name'])
        log_files, _ = ssh_client.run('ls {}'.format(log_files))
        for log_file in log_files.splitlines():
            log_file = log_file.strip()
            if log_file:
                ssh_client.get(log_file, path)


def start_dns(config, host):
    """
    Start DNS server for polygraph test

    :param config: the configuration dict
    """
    with ssh.SSHConnection(address=host) as ssh_client:
        zone = config['zone']
        log('Starting polygraph DNS', host)

        polymix_recipe = get_polymix_recipe(config["proxy_type"])
        out, err = ssh_client.sudo('ls /etc/nsd3')
        nsd = 'nsd' if err else 'nsd3'

        ssh_client.run('polygraph-dns-cfg --config {} --cfg_dirs {} '
                       '>/dev/null'.format(polymix_recipe, WORKLOADS_PATH))
        ssh_client.sudo('mv {} /etc/{}/bench.zone'.format(zone, nsd))
        ssh_client.sudo('mv {}.rev /etc/{}/10.in-addr.arpa.zone'
                        .format(zone, nsd))

        nsd_tmpl = os.path.join(os.path.dirname(__file__),
                                'templates', 'nsd.conf')
        with open(nsd_tmpl) as rf, NamedTemporaryFile('w') as conf_file:
            conf = Template(rf.read()).substitute(ZONE=zone)
            conf_file.write(conf)
            conf_file.flush()
            ssh_client.put(conf_file.name, 'nsd.conf')
            ssh_client.sudo('mv nsd.conf /etc/{}/nsd.conf'.format(nsd))

        ssh_client.sudo('/usr/sbin/nsdc rebuild')
        out, err = ssh_client.sudo('/usr/sbin/service {0} stop'.format(nsd))
        log(out, host)
        out, err = ssh_client.sudo('/usr/sbin/service {0} start'.format(nsd))
        log(out, host)


def add_route(network, gateway, host):
    """
    Add static routes

    :param network: the network string in CIDR format
    :param gateway: IP address of the gateway
    """
    with ssh.SSHConnection(address=host) as ssh_client:
        old_route = ssh_client.sudo('ip route show to exact {0}'
                                    .format(network))
        if old_route:
            ssh_client.sudo('ip route del {0}'.format(network))
        ssh_client.sudo('ip route add {0} via {1}'.format(network, gateway))


def add_routes(peers, gateway, host):
    """
    Add static routes for peers

    :param peers: list of peer IP addresses
    :param gateway: IP address of the gateway
    """
    for peer in peers:
        network = '10.{}.0.0/16'.format(peer.split('.')[-1])
        gw = gateway if gateway else peer.split('@')[-1]
        add_route(network, gw, host)


def _polyrun_host(host):
    """
    A helper function to create user@host string for polygraph machines
    """
    return '{0}@{1}'.format(POLYGRAPH_USERNAME, host)


def check_progress(name, host):
    """
    Check progress of a polyrun by its name

    :param name: name of the polyrun test

    :returns: the last status line of a polygraph stdout log
    """
    with ssh.SSHConnection(address=host) as ssh_client:
        host, _ = ssh_client.run('hostname')
        console_f = '{0}/stdout-{1}-{2}.log'.format(LOG_LOC, host, name)
        step_re = "'^[0-9.]+\| ([a-z]-[a-z][a-z0-9]+|shutdown )'"
        command = "egrep {0} {1} 2>/dev/null | tail -1".format(step_re, console_f)
        return ssh_client.run(command)[0]


class Polyrun(object):
    """
    Main Polyrun class. This class is responsible to setup and run
    a polyrun test.
    """

    def __init__(self, config):
        """
        The polyrun main runner.

        @param config: a dictionary of test configuration. Following are the
            expected keys:
                cachebox_ip: IP of the testing CACHEbox
                polysrv_ips: IPs of the polygraph servers
                polyclt_ips: IPs of the polygraph clients
                nameserver: IP of the DNS server
                timeserver: IP of the timeserver
                duration: minutes for each phase
                schedule_type: one of ramp, constant or steps
                schedule_value: RPS value for schedule
                                - an integer for constant
                                - [start]:end for ramp
                                - start:end:stepsize for steps
                cache_size: Size of the cache storage for fill stage
                cache_init: Initial state of the cachebox
                                - quickstart: keep state as is
                                - init: Initialise the cachebox
                                - reload: Restart squid
                                - non-cachebox: Not a cachebox
                proxy_type: Deployment type of proxy
                                - explicit: explicit proxy
                                - transparent: transparent proxy
                                - bridge: bridge mode
                                - bypass: no-proxy
                fill_rate: RPS at which fill is performed
                timeupdate: update polygraph machines time
                outdir: output folder
                recipe: name of the schedule template
        """
        self.config = config
        zone = self.config.get('polyrig', '')
        self.config['zone'] = 'bench{}'.format(zone)
        self.servers = [_polyrun_host(s) for s in config['polysrv_ips']]
        self.clients = [_polyrun_host(c) for c in config['polyclt_ips']]
        self.polygraphs = self.servers + self.clients
        self._schedule = None
        self._cachebox = None

    @property
    def cachebox(self):
        """
        The associated cachebox instance for this test run

        :returns: CACHEbox instance if configured
        """
        if not self._cachebox:
            self._cachebox = (
                self.config.get('cache_init') != 'non-cachebox' and
                self.config.get('proxy_type') != 'bypass' and
                CACHEbox(self.config['cachebox_ip'])
            )
        return self._cachebox

    def main(self):
        """
        The main runner. This method should be called to run a complete test
        after instantiating with a valid configuration.

        :returns: a summary list of RPS, BHR, DHR
        """
        logger.info('Starting polyrun')
        self.run_test()
        summary = self.generate_reports()
        logger.info('Test complete.')
        return summary

    def cachebox_init(self):
        """
        Initialise the cachebox as configured
        """
        if not self.cachebox:
            return

        self.cachebox.update_routes(self.polygraphs)
        self.cachebox.api.set_item('network', 'dns_server_1',
                                   self.config['nameserver'])
        self.cachebox.api.set_item('appliance', 'timeservers',
                                   self.config['timeserver'])
        self.cachebox.api.ntp_synchronise_now()

        if self.config.get("proxy_type") != "reverse":
            engine = self.config.get('cache_engine')
            self.cachebox.set_cache_engine(engine)

            if self.config['cache_init'] == 'init':
                self.cachebox.initialise()
            elif self.config['cache_init'] == 'reboot':
                self.cachebox.restart()
            elif self.config['cache_init'] == 'reload':
                self.cachebox.restart_squid()

            self.cachebox.wait_for_cache_engine()

            self.cachebox.enable_cdn(zone=self.config['zone'],
                                     enable=self.config['test_cdn'])
        else:
            self.cachebox.setup_reverse(self.config["polysrv_ips"])

    def run_test(self):
        """
        Start running the test
        """
        self.cachebox_init()
        if self.config.get('cache_init') == 'reboot':
            ssh.execute(reboot_polygraph, hosts=self.polygraphs)

        skel = self._prepare_bundle()
        ssh.execute(init_polygraph, bundle=skel, config=self.config,
                    servers=self.servers, clients=self.clients,
                    hosts=self.polygraphs)
        sh.rm('-rf', os.path.dirname(skel))

        logger.info('Starting DNS')
        ssh.execute(start_dns, config=self.config,
                    hosts=[_polyrun_host(self.config['nameserver'])])

        logger.info('Running test')
        ssh.execute(run_polygraph, servers=self.servers, clients=self.clients,
                    config=self.config, hosts=self.polygraphs)

    def generate_reports(self):
        """
        Generate reports for current test

        :returns: a summary list of RPS, BHR, DHR
        """
        logger.info('Generating reports')
        sh.rm('-rf', self.config['outdir'])
        sh.mkdir('-p', self.config['outdir'])
        ssh.execute(get_logs, config=self.config, hosts=self.polygraphs)

        logger.info('Generating polygraph report')
        path = self.config['outdir']
        html_path = os.path.join(path, 'summary')
        logs = glob('{}/polyrun-*{}.log'.format(path, self.config['name']))

        pg_reporter = sh.Command('polygraph-reporter')
        pg_reporter('--plotter', '/usr/bin/gnuplot',
                    '--label', 'polyrun',
                    '--report_dir', html_path,
                    *logs)
        sh.rm('-f', '{}/polyrun-*.log'.format(path))

        everything = Everything(os.path.join(html_path, 'everything.html'))
        summary = everything.list_summary()

        if self.cachebox:
            try:
                self.cachebox.get_results(path=self.config['outdir'])
            except:
                logger.exception('Could not get CACHEbox data')
        return summary

    def stop_polyrun(self):
        """
        Stop the test
        """
        ssh.execute(stop_polygraph, hosts=self.polygraphs)

    @property
    def schedule(self):
        """
        Create and return a schedule from config

        :returns: an instance of appropriate
            :py:class:`polygraph.pgl.PglSchedule`
        """
        if self._schedule:
            return self._schedule

        self._schedule = pgl.PglSchedule()
        fill_rate = self.config.get('fill_rate', 0)
        if fill_rate > 0:
            self._schedule.include_fill(pgl.FillPgl(self.config['cache_size'],
                                                    fill_rate))

        sched_type = self.config.get('schedule_type')
        sched_value = self.config.get('schedule_value')
        duration = self.config.get('duration')

        if sched_type == 'ramp':
            phase = pgl.RampPglPhase('ramp', duration, sched_value)
            self._schedule.append(phase)
        elif sched_type == 'constant':
            q, r = divmod(duration, SCHEDULE_MAX_INTERVAL)
            for i in range(q):
                phase = pgl.ConstantPglPhase('c{}'.format(i),
                                             SCHEDULE_MAX_INTERVAL,
                                             sched_value)
                self._schedule.append(phase)
            if r > 0:
                phase = pgl.ConstantPglPhase('const', r, sched_value)
                self._schedule.append(phase)
        elif sched_type == 'steps':
            try:
                start, end, stepsize = map(int, sched_value.split(":"))
            except:
                logging.error("Error parsing steps parameter "
                              "(should be start:end:stepsize)",
                              exc_info=True)
                raise Exception('Invalid steps')

            for step in range(start, end + 1, stepsize):
                phase = pgl.ConstantPglPhase('s{}'.format(step), duration,
                                             step)
                self._schedule.append(phase)
                phase = pgl.IdlePglPhase('i{}'.format(step), int(duration) / 4)
                self._schedule.append(phase)
        else:
            raise NotImplementedError('TODO...')

        return self._schedule

    def _prepare_bundle(self):
        """
        Create a set of populated templates, workload files and associated
        scripts to upload to each polygraph machine ready for running the
        tests.

        :param schedule: a PglSchedule instance describing the test to run
        :returns: path to a tgz file storing the required files
        """
        staging_area = tempfile.mkdtemp('-polyrun-bundle')

        TEMPLATE_SOURCE = os.path.join(os.path.dirname(__file__), 'workloads')
        shutil.copytree(TEMPLATE_SOURCE,
                        os.path.join(staging_area, 'polygraph'))

        fname = os.path.join(staging_area, 'polygraph', 'schedule.pg')
        with open(fname, 'w') as f:
            f.write(self.schedule.render())

        schedule_type = self.config['schedule_type']
        schedule_value = self.config['schedule_value']
        if schedule_type == 'ramp':
            peak_rate = schedule_value.split(':')[-1]
        elif schedule_type == 'constant':
            peak_rate = schedule_value
        elif schedule_type == 'steps':
            peak_rate = schedule_value.split(':')[1]
        else:
            peak_rate = 10000  # Default maximum RPS

        def addr_space_bits(machines):
            ips = self.config[machines]
            if len(ips) > 1:
                ips = [ips[0], ips[-1]]
            return '-'.join([ip.split('.')[-1] for ip in ips])

        addr_mask = {1: 22}.get(len(self.config['polysrv_ips']), 16)

        CONFIG = {'CACHEBOX_IP': self.config.get('cachebox_ip'),
                  'SRV_ADDR_SPACE': addr_space_bits('polysrv_ips'),
                  'CLT_ADDR_SPACE': addr_space_bits('polyclt_ips'),
                  'SRV_ADDRS': '{i[0]}.{i[1]}.{i[2]}.{s}'.format(
                      i=self.config['polysrv_ips'][0].split('.'),
                      s=addr_space_bits('polysrv_ips')),
                  'CLT_ADDRS': '{i[0]}.{i[1]}.{i[2]}.{s}'.format(
                      i=self.config['polyclt_ips'][0].split('.'),
                      s=addr_space_bits('polyclt_ips')),
                  'ADDR_MASK': addr_mask,
                  'DNS_SRV': self.config['nameserver'] + ':53',
                  'ZONE': self.config['zone'],
                  'PEAK_REQUEST_RATE': peak_rate,
                  'FILL_REQUEST_RATE': self.config.get('fill_rate') or 750,
                  'RANGE_PERCENTAGE': self.config.get('ranges') or 0.1,
                  'CONTENTS': self.config['recipe'],
                  'PROXY_CACHE_SIZE_GB': self.config.get('cache_size') or 1000}

        polymix_recipe = get_polymix_recipe(self.config["proxy_type"])
        fname = os.path.join(staging_area, 'polygraph', polymix_recipe)
        with open(fname, 'r') as f:
            s = Template(f.read())
            result = s.substitute(**CONFIG)

        with open(fname, 'w') as f:
            f.write(result)

        POLYGRAPH_SKEL = 'polygraph_skel_polyrun.tgz'
        skel_path = os.path.join(staging_area, POLYGRAPH_SKEL)
        tar = tarfile.open(skel_path, 'w:gz')
        tar.add(os.path.join(staging_area, 'polygraph'), 'polygraph')
        tar.close()

        return skel_path


if __name__ == '__main__':
    """
    Support running standalone from console
    """
    from docopt import docopt
    args = docopt(__doc__, version=__version__)
    config = {k[2:].replace('-', '_'): v for k, v in args.items()}
    for k in ('polysrv_ips', 'polyclt_ips'):
        config[k] = config[k].split(',')
    QUIET = not config['verbose']
    from pprint import pprint
    pprint(config)
    poly = Polyrun(config)
    if config.get('last_report'):
        poly.generate_reports()
    else:
        poly.main()
