var gulp = require('gulp');
var gutil = require('gulp-util');
var connect = require('gulp-connect');
var gulpif = require('gulp-if');
var concat = require('gulp-concat');
var tplCache = require('gulp-angular-templatecache');
var jade = require('gulp-jade');
var less = require('gulp-less');
var uglify = require('gulp-uglify');
var cssmin = require('gulp-cssmin');
var filesize = require('gulp-filesize');
var using = require('gulp-using');

var paths = {
    appjs: [
        '!./app/**/*_test.js',
        '!./app/lib/js/*.js',
        './app/d3/*.js',
        './app/app.js',
        './app/**/*.js'
    ],
    libjs: [
        './bower_components/lodash/dist/lodash.min.js',
        './bower_components/jquery/dist/jquery.min.js',
        './bower_components/datatables/media/js/jquery.dataTables.min.js',
        './bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js',
        './bower_components/momentjs/min/moment.min.js',
        './bower_components/showdown/compressed/showdown.js',
        './bower_components/hammerjs/hammer.min.js',
        './bower_components/d3/d3.min.js',
        './bower_components/rickshaw/rickshaw.min.js',
        './bower_components/socket.io-client/dist/socket.io.js',
        './bower_components/angular/angular.min.js',
        './bower_components/angular-animate/angular-animate.min.js',
        './bower_components/angular-sanitize/angular-sanitize.min.js',
        './bower_components/angular-cookies/angular-cookies.min.js',
        './bower_components/angular-aria/angular-aria.min.js',
        './bower_components/angular-material/angular-material.min.js',
        './bower_components/restangular/dist/restangular.min.js',
        './bower_components/angular-filter/dist/angular-filter.min.js',
        './bower_components/angular-ui-router/release/angular-ui-router.min.js',
        './bower_components/angular-ui-bootstrap/dist/ui-bootstrap-custom-0.12.0.min.js',
        './bower_components/angular-markdown-directive/markdown.js',
        './bower_components/angular-socket-io/socket.min.js',
        './app/lib/js/*.js'
    ],
    index: [
        './app/index.jade'
    ],
    templates: [
        '!./app/index.jade',
        './app/**/*.html',
        './app/**/*.jade',
    ],
    appcss: [
        './app/app.less'
    ],
    libcss: [
        './bower_components/angular/angular-csp.css',
        './bower_components/bootstrap/dist/css/bootstrap.css',
        './app/lib/css/AdminLTE.css',
        './bower_components/datatables/media/css/jquery.dataTables.min.css',
        './bower_components/angular-material/angular-material.min.css',
        './bower_components/angular-material/themes/*.css',
        './bower_components/rickshaw/rickshaw.css',
        './bower_components/animate.css/animate.css',
        './bower_components/pygments/css/trac.css',
        './bower_components/font-awesome/css/font-awesome.min.css'
    ],
    images: [
        './bower_components/datatables/media/images/*.png'
    ],
    fonts: [
        './bower_components/font-awesome/fonts/*'
    ],
    testjs: [
        './app/**/*_test.js'
    ]
};

var watched_tasks = Object.keys(paths);
var default_tasks = watched_tasks.concat([
    'connect',
    'watch'
]);

var gulp_dest = function() {
    return gulp.dest('./build/app');
};

gulp.task('appjs', function() {
    gulp.src(paths.appjs)
        // .pipe(using({'prefix': 'appjs'}))
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(filesize())
        .pipe(gulp_dest());
});

gulp.task('libjs', function() {
    gulp.src(paths.libjs)
        // .pipe(using({'prefix': 'libjs'}))
        .pipe(concat('lib.js'))
        .pipe(filesize())
        .pipe(gulp_dest());
});

gulp.task('testjs', function() {
    gulp.src(paths.testjs)
        // .pipe(using({'prefix': 'testjs'}))
        .pipe(gulp_dest());
});

gulp.task('appcss', function() {
    var lessconf = {
        paths: ['./bower_components/bootstrap/less']
    };

    gulp.src(paths.appcss)
        // .pipe(using({'prefix': 'appcss'}))
        .pipe(gulpif(/[.]less$/, less(lessconf).on('error', gutil.log)))
        .pipe(concat('app.css'))
        .pipe(cssmin())
        .pipe(filesize())
        .pipe(gulp_dest());
});

gulp.task('libcss', function() {
    gulp.src(paths.libcss)
        // .pipe(using({'prefix': 'libcss'}))
        .pipe(concat('lib.css'))
        .pipe(cssmin())
        .pipe(filesize())
        .pipe(gulp_dest());
});

gulp.task('index', function() {
    gulp.src(paths.index)
        // .pipe(using({'prefix': 'index'}))
        .pipe(gulpif(/[.]jade$/, jade().on('error', gutil.log)))
        .pipe(gulp.dest('./build'));
});

gulp.task('templates', function() {
    gulp.src(paths.templates)
        // .pipe(using({'prefix': 'templates'}))
        .pipe(gulpif(/[.]jade$/, jade().on('error', gutil.log)))
        .pipe(tplCache('templates.js',{standalone:true}))
        .pipe(filesize())
        .pipe(gulp_dest());
});

gulp.task('images', function() {
    gulp.src(paths.images)
        .pipe(gulp.dest('./build/images'));
});

gulp.task('fonts', function() {
    gulp.src(paths.fonts)
        .pipe(gulp.dest('./build/fonts'));
});

gulp.task('watch', function() {
    var build_files = [
        'build/**/*.html',
        'build/**/*.js',
        'build/**/*.css'
    ];

    gulp.watch(build_files, function(event) {
        return gulp.src(event.path)
            .pipe(connect.reload());
    });

    watched_tasks.map(function(task) {
        gulp.watch(paths[task], [task]);
    });
});

gulp.task('connect', connect.server({
    root: ['build'],
    port: 9000,
    livereload: true
}));

gulp.task('default', default_tasks);