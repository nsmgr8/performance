(function() {
    d3.performance = d3.performance || {};
    d3.performance.progress = d3.performance.progress || {};

    d3.performance.progress.polyrun = function(data) {
        var $ts = d3.select('#progress-ts');
        if ($ts.empty() || !data || data.length === 0)
            return;

        d3.selectAll('#progress-ts > div').remove();
        $ts.append('div').attr('id', 'progress-ts-y');
        $ts.append('div').attr('id', 'progress-ts-graph');
        $ts.append('div').attr('id', 'progress-ts-x');

        var colors = d3.scale.category10();
        var graph = new Rickshaw.Graph({
            element: document.querySelector('#progress-ts-graph'),
            width: $('#progress-ts').width(),
            renderer: 'line',
            series: (function() {
                var series = [];
                for (var i=0; i<data.length; i++) {
                    var ts = [];
                    for (var j=0; j<data[i].data.length; j++) {
                        var row = data[i].data[j];
                        ts.push({x: row.elapsed,
                                y: row.total_replies,
                                phase: row.phase,
                                reply_rate: row.reply_rate,
                                resp_time: row.resp_time,
                                hit_ratio: row.hit_ratio,
                                txn_error: row.txn_error,
                                sockets: row.sockets});
                    }
                    series.push({name: data[i].polygraph,
                                data: ts,
                                color: colors(i)});
                                $('dt.' + data[i].polygraph.replace(/ /g, '-').toLowerCase())
                                .css('color', colors(i));
                }
                return series;
            })()
        });
        var x_axis = new Rickshaw.Graph.Axis.X({
            graph: graph,
            orientation: 'bottom',
            element: document.querySelector('#progress-ts-x')
        });
        var y_axis = new Rickshaw.Graph.Axis.Y({
            graph: graph,
            orientation: 'right',
            tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
            element: document.querySelector('#progress-ts-y')
        });

        graph.render();
        var hoverDetail = new Rickshaw.Graph.HoverDetail({
            graph: graph,
            formatter: function(series, x, y, formattedX, formattedY, d) {
                return series.name + '<br>' +
                    'Phase: <strong>' + d.value.phase + '</strong><br>' +
                    'Total replies: <strong>' + formattedY + '</strong><br>' +
                    'Reply rate: <strong>' + d.value.reply_rate + '</strong><br>' +
                    'Response time: <strong>' + d.value.resp_time + '</strong><br>' +
                    'Hit ratio: <strong>' + d.value.hit_ratio + '</strong><br>' +
                    'Transaction error: <strong>' + d.value.txn_error + '</strong><br>' +
                    'Open sockets: <strong>' + d.value.sockets + '</strong>';
            },
            xFormatter: Rickshaw.Fixtures.Number.formatKMBT,
            yFormatter: Rickshaw.Fixtures.Number.formatKMBT
        });
    };
}());
