(function() {
    d3.dhcperf = d3.dhcperf || {};

    d3.dhcperf.draw = function(elem, original_data) {
        if (!original_data || original_data.length === 0) return;

        var data = d3.merge(original_data.map(function(x) {
            return x.tests.map(function(y) {
                return {
                    pool: x.pool.size,
                    clients: y.clients,
                    discover: {
                        qps: d3.max(y.runs, function(d) { return d.discover.qps; }),
                        hwm: d3.max(y.runs, function(d) { return d.discover.hwm; }),
                    },
                    renew: {
                        qps: d3.max(y.runs, function(d) { return d.renew.qps; }),
                        hwm: d3.max(y.runs, function(d) { return d.renew.hwm; }),
                    }
                };
            });
        }));

        var max_r = d3.max(data.map(function(d) {
            return d3.max([d.discover.hwm, d.renew.hwm]);
        }));
        var nclients = d3.set(data.map(function(d) {
            return d3.max([d.clients, d.pool]);
        })).values();
        var $dhcperf = d3.select(elem),
            width = $(elem).width(),
            height = 400,
            radius = width / (2 * nclients.length) - 10,
            xScale = d3.scale.ordinal().rangeBands([radius, width]).domain(nclients),
            yScale = d3.scale.ordinal().rangeBands([height, radius]).domain(nclients),
            rScale = d3.scale.linear().range([0, radius]).domain([-1, max_r]),
            colors = d3.scale.category20(),
            xAxis = d3.svg.axis().scale(xScale).orient('bottom'),
            yAxis = d3.svg.axis().scale(yScale).orient('right');

        var svg = $dhcperf.append('svg').attr({
            width: width,
            height: height + 50
        })
        .append('g')
        .attr('transform', 'translate(0,0)');

        svg.append('rect').attr({
            width: width,
            height: height + 50,
            fill: '#f9f9f9'
        });

        var draw_circle = function(color, type, value) {
            var offset = type === 'renew' ? radius : 0;
            svg.append('g').selectAll('circle')
            .data(data).enter()
            .append('circle')
            .attr({
                'class': type,
                cx: 0,
                cy: 0,
                r: function(d) {
                    return rScale(d[type][value]);
                },
                fill: color,
                transform: function(d) {
                    return 'translate(' + (xScale(d.pool) + offset) + ',' + yScale(d.clients) + ')';
                }
            });
        };

        draw_circle(colors(1), 'discover', 'hwm');
        draw_circle(colors(2), 'discover', 'qps');
        draw_circle(colors(3), 'renew', 'hwm');
        draw_circle(colors(4), 'renew', 'qps');

        svg.append('g')
        .attr('class', 'x axis')
        .attr('transform', 'translate(-' + radius + ',' + (height - 20) + ')')
        .call(xAxis);
        svg.append('g')
        .attr('class', 'y axis')
        .attr('transform', 'translate(0,-20)')
        .call(yAxis);

        svg.append('text')
        .text('Pool size')
        .attr('transform', 'translate(' + (width / 2) + ',' + (height + 20) + ')');
        svg.append('text')
        .text('No. of clients')
        .attr('transform', 'translate(10,15)');
        svg.append('text')
        .text('Maximum LPS')
        .attr('text-anchor', 'middle')
        .attr('transform', 'translate(' + (width / 2) + ',20)');
        svg.append('text')
        .text('(Pool size vs. No. of clients)')
        .attr('text-anchor', 'middle')
        .attr('transform', 'translate(' + (width / 2) + ',40)');

        svg.selectAll('circle')
        .on('mouseover', function() {
            var $elem = d3.select(this),
            data = $elem.data()[0],
            type = $elem.attr('class');

            $('#tooltip .panel-title span').html(type.toUpperCase());
            $('#dhcp-hwm').html(data[type].hwm);
            $('#dhcp-qps').html(data[type].qps);
            $('#pool-size').html(data.pool);
            $('#nclients').html(data.clients);
            var $tooltip = $('#tooltip'),
            tw = $tooltip.width(), th = $tooltip.height(),
            cx = d3.event.clientX, cy = d3.event.clientY,
            wx = $(window).width(), wy = $(window).height();
            var xpos = 0, ypos = 0;

            if (wx - cx < tw) { xpos = tw + 10; }
            if (wy - cy < th) { ypos = th + 10; }

            $tooltip
            .css('left', cx - xpos)
            .css('top', cy - ypos)
            .show();
        });

        svg.selectAll('circle')
        .on('mouseout', function() {
            $('#tooltip').hide();
        });
    };
}());
