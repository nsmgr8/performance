(function() {
    d3.cdnrun = d3.cdnrun || {};

    var radius = 25, diameter = 2 * radius,
        legendWidth = 100,
        duration = 2000,
        xScale = d3.time.scale(),
        rScale = d3.scale.linear().range([5, radius]).domain([0, 100]),
        xAxis = d3.svg.axis().scale(xScale).orient("bottom"),
        color = d3.scale.category10();

    d3.cdnrun.dots = function(elem) {
        return function(data) {
            var width = $(elem).width(),
                height = diameter * data.length,
                x0 = +(moment().subtract(1, 'days')),
                x1 = +moment().add(15, 'minutes');

            xScale.range([0, width - legendWidth]).domain([x0, x1]);
            xAxis.tickSize(-height);

            var zoom = d3.behavior.zoom()
                .x(xScale)
                .scaleExtent([0, 1])
                .on("zoom", function() {
                    var zdom = xScale.domain(),
                        minPeriod = moment.duration(1, 'days'),
                        xMin = moment().subtract(10, 'days'),
                        xMax = moment().add(15, 'minutes');
                    if (+(zdom[0]) > +xMin) { xMin = zdom[0]; }
                    if (+(zdom[1]) < +xMax) { xMax = zdom[1]; }
                    if (moment(xMax) - moment(xMin) < minPeriod) { return; }
                    xScale.domain([xMin, xMax]);
                    svg.select(".x.axis").call(xAxis);
                    svg.selectAll('.dot').transition()
                        .attr('cx', function(d) { return xScale(d.date); });

                });

            var svg = d3.select(elem).selectAll('svg').data([data]);
            var svgEnter = svg.enter().append('svg')
                .attr({
                    viewBox: '0 0 ' + width + ' ' + height,
                    preserveAspectRatio: 'xMinYMin meet'
                });

            svgEnter.append('g').attr('transform', 'translate(0,0)');
            svgEnter.append('rect').attr({
                width: width - legendWidth,
                height: height,
                fill: '#f9f9f9'
            }).call(zoom);

            svgEnter.append('g')
                .attr({
                    'class': 'x axis',
                    transform: 'translate(0,' + height + ')'
                });

            svg.transition().attr({width: width, height: height + 20});
            svg.select('.x.axis')
                .transition()
                .duration(duration)
                .attr('transform', 'translate(0,' + height + ')')
                .call(xAxis);

            svg.selectAll('.cdn').remove();
            var cdns = svg.selectAll('.cdn')
                .data(function(d) { return d; },
                      function(d) { return d.cdn; });

            var enter = cdns.enter().append('g')
                .attr({
                    transform: function(d, i) {
                        return 'translate(0,' + (i * diameter) + ')';
                    },
                    'class': 'cdn',
                    id: function(d) {
                        return 'cdn-' + d.cdn;
                    }
                });

            enter.append('svg:line')
                .attr('x1', 0)
                .attr('x2', width - legendWidth)
                .attr('y1', radius)
                .attr('y2', radius)
                .style('stroke-dasharray', '0 0 1 1')
                .style('stroke', function(d) { return color(d.cdn); });

            enter.append('a')
                .attr('xlink:href', function(d) { return d.diffUrl; })
                .attr('xlink:type', 'simple')
                .attr('xlink:show', 'new')
                .append('text')
                .attr('x', width - legendWidth + 5)
                .attr('y', 28)
                .style('stroke', function(d) { return color(d.cdn); })
                .style('font-size', '18px')
                .text(function(d) { return d.cdn; });

            cdns.exit().remove();

            var dots = cdns.selectAll('.dot')
                .data(function(d) { return d.data; },
                      function(d) { return d.checked_at; });

            dots.transition().duration(duration)
                .attr('cx', function(d) {
                    return xScale(d.date);
                });

            dots.enter()
                .append('a')
                .attr('xlink:href', function(d) { return '/#/cdn/' + d.id; })
                .attr('xlink:type', 'simple')
                .attr('xlink:show', 'new')
                .append('circle')
                .attr({
                    'class': 'dot',
                    r: 0,
                    cx: function(d) { return xScale(d.date); },
                    cy: radius,
                    fill: function(d) { return color(d.cdn); }
                }).transition()
                .duration(duration)
                .attr('r', function(d) { return rScale(d.percent); });

            dots.exit().remove();

            dots.on('mouseover', function() {
                var data = d3.select(this).data()[0];
                $('#cached').html(
                    d3.format('.1f')(data.percent) + '% (' +
                    d3.format('.4s')(data.cached_size) + ' / ' +
                    d3.format('.4s')(data.total_size) + ')');
                $('#browser').html(data.browser);
                $('#cachebox').html(data.cachebox);
                $('#cdn-id span').html(data.cdn);
                $('#cdn-id small').html(d3.time.format('%c')(data.date));
                $('#vtitle').html(data.title);

                var $tooltip = $('#tooltip'),
                    tw = $tooltip.width(), th = $tooltip.height(),
                    cx = d3.event.clientX, cy = d3.event.clientY,
                    wx = $(window).width(), wy = $(window).height();
                var xpos = 0, ypos = 0;

                if (wx - cx < tw) { xpos = tw + 10; }
                if (wy - cy < th) { ypos = th + 10; }

                $tooltip
                    .css('left', cx - xpos)
                    .css('top', cy - ypos)
                    .show();
            });

            dots.on('mouseout', function() { $('#tooltip').hide(); });
        };
    };
})();
