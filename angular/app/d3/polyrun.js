(function() {
    d3.polyrun = d3.polyrun || {};

    d3.polyrun.draw = function(elem, data) {
        data = data.filter(function(d) { return d.phase.match(/^[cs]\d+$/); });
        if (data.length < 2) return;

        var colors = d3.scale.category10(),
            height = 200,
            scales = {
                'rps': d3.scale.linear().range([0, height]),
                'mbps': d3.scale.linear().range([0, height]),
                'bhr': d3.scale.linear().domain([0, 100])
                         .range([0, height]),
                'dhr': d3.scale.linear().domain([0, 100])
                         .range([0, height])
            };

        var series = ['rps', 'bhr', 'dhr', 'mbps'].map(function(f) {
            var values = data.map(function(d, i) {
                return {x: i, y: d[f].measured};
            });

            if (['rps', 'mbps'].indexOf(f) !== -1) {
                var domain = d3.max(values.map(function(d) { return d.y; }));
                scales[f].domain([0, domain]);
            }
            return {
                name: f,
                data: values,
                color: colors(f),
                scale: scales[f]
            };
        });

        var lines = series.map(function(d) { return $.extend({renderer: 'line'}, d); });
        var dots = series.map(function(d) { return $.extend({renderer: 'scatterplot'}, d); });
        var $graph = document.querySelector(elem);
        try {
            var graph = new Rickshaw.Graph({
                element: $graph,
                width: $($graph).width(),
                height: height,
                stroke: true,
                renderer: 'multi',
                series: lines.concat(dots)
            });
            graph.render();

            var legend = new Rickshaw.Graph.Legend({
                graph: graph,
                element: $graph
            });
            var shelving = new Rickshaw.Graph.Behavior.Series.Toggle({
                graph: graph,
                legend: legend
            });
            var hoverDetail = new Rickshaw.Graph.HoverDetail({
                xFormatter: function(i) { return data[i].phase; },
                graph: graph
            });
        } catch (e) {
            console.log(e);
        }
    };

}());
