(function() {
    angular.module('performance', [
        'ngSanitize',
        'ngAnimate',
        'ngCookies',
        'ngMaterial',
        'angular.filter',
        'ui.router',
        'ui.bootstrap',
        'btford.markdown',
        'btford.socket-io',
        'restangular',
        'templates'
    ])
    .run([
        '$http',
        '$cookies',
        function($http, $cookies) {
            $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
            $http.defaults.headers.put['X-CSRFToken'] = $cookies.csrftoken;
        }
    ])
    .config([
        '$httpProvider',
        'RestangularProvider',
        'rootUrl',
        function($httpProvider, RestangularProvider, rootUrl) {
            RestangularProvider.setBaseUrl(rootUrl + '/api');

            $httpProvider.interceptors.push([
                '$q',
                function($q) {
                    return {
                        responseError: function(response) {
                            var msg;
                            switch(response.status) {
                                case 0:
                                    msg = 'Server not available';
                                    break;
                                case 400:
                                case 401:
                                case 403:
                                    msg = '<h3>Authentication required.</h3>' +
                                          'Please login with your Appliansys mail account.';
                                    break;
                                case 502:
                                    msg = 'Application server is not available';
                                    break;
                                default:
                                    msg = response.status + ': ' + response.statusText;
                            }
                            if (response.data && response.data.detail) {
                                msg += '<br>' + response.data.detail;
                            }
                            noty({text: msg, type: 'error'});
                            return $q.reject(response);
                        }
                    };
                }
            ]);
        }
    ]);
}());