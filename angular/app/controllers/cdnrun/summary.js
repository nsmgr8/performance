(function() {
    angular.module('performance')
    .controller('CDNRunSummaryController', [
        '$scope',
        'rootUrl',
        'perfSocket',
        function($scope, rootUrl, perfSocket) {
            var dataUrl = rootUrl + 'cdn-status/json/',
                plot = d3.cdnrun.dots('#cdn-cache-graph'),
                parseDate = d3.time.format.iso.parse;

            var type = function(data) {
                var cdns = d3.keys(data).sort(),
                    transformed = [];
                cdns.forEach(function(cdn) {
                    d3.map(data[cdn]).forEach(function(i, d) {
                        d.date = parseDate(d.checked_at);
                        d.percent = (d.cached_size / d.total_size) * 100.0;
                    });
                    transformed.push({
                        cdn: cdn,
                        diffUrl: '/cdn-status/' + cdn + '/diff/',
                        data: data[cdn]
                    });
                });
                return transformed;
            };

            var success = function(data) {
                if (data) { plot(type(data)); }
                else { console.log('Could not load data'); }
            };

            d3.json(dataUrl, success);

            perfSocket.on('cdnrun_load', function() {
                d3.json(dataUrl, success);
            });
        }
    ]);
}());
