(function() {
    angular.module('performance')
    .controller('CDNRunDetailController', [
        '$scope',
        '$state',
        '$http',
        'rootUrl',
        '$mdDialog',
        function($scope, $state, $http, rootUrl, $mdDialog) {
            $http.get(rootUrl + 'cdn-status/' + $state.params.cdnId,
                      {headers: {'X-Requested-With': 'XMLHttpRequest'}})
            .success(function(data) {
                if (!data || data.length !== 1) {
                    return;
                }
                $scope.cdnrun = data[0].fields;
            });

            $scope.response_class = response_class;

            $scope.showDetail = function(response) {
                $mdDialog.show({
                    templateUrl: 'template/dialogs/cdn_detail.html',
                    controller: detailDialog(response)
                });
            };
        }
    ]);

    var detailDialog = function(response) {
        return [
            '$scope',
            function($scope) {
                $scope.response = response;
                $scope.response_class = response_class;
            }
        ];
    };

    var response_class = function(status) {
        return 'box-' + {
            'HIT/HIT': 'success',
            'MISS/HIT': 'info',
            'MISS/MISS': 'danger',
            'NA/NA': 'default'
        }[status] || 'default';
    };
}());
