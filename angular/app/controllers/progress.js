(function() {
    angular.module('performance')
    .controller('TestRunProgressController', [
        '$scope',
        '$state',
        'Restangular',
        '$http',
        '$interval',
        'perfSocket',
        'rootUrl',
        function($scope, $state, Restangular, $http, $interval, perfSocket, rootUrl) {
            $scope.running = [];
            $scope.scheduled = [];
            var update_interval = 60 * 1000;

            var update_status = function() {
                Restangular.all('tests').getList({state: 10})
                .then(function(data) {
                    var is_testrun = /^testrun\./.test($state.current.name);

                    $scope.running = data.map(function(d) {
                        if (is_testrun && (d.script_name === 'polyrun') && (d.id === parseInt($state.params.runId))) {
                            $http.get(rootUrl + 'runs/' + d.id + '/progress/')
                            .success(function(data) {
                                if (data) {
                                    d3.select('#progress-wait').remove();
                                    d3.performance.progress.polyrun(data);
                                }
                            });
                        }

                        d.will_complete_on = moment(d.eta).fromNow();
                        d.progress = moment().diff(d.started_on);
                        d.eta_diff = moment(d.eta).diff(d.started_on);
                        d.percent_complete = +(100 * d.progress / d.eta_diff).toFixed(0);
                        return d;
                    });
                });
            };

            update_status();
            $interval(update_status, update_interval);

            perfSocket.on('refresh', function() {
                update_status();
            });
        }
    ]);
}());