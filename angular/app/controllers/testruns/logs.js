(function() {
    angular.module('performance')
    .controller('TestRunLogsController', [
        '$scope',
        '$timeout',
        '$filter',
        function($scope, $timeout, $filter) {
            $scope.log_files = [];
            $scope.log_content = '';
            $scope.selected = {context: 0, search: ''};

            $scope.loadLogFile = function() {
                $scope.run.customGET('log_file', {
                    fname: $scope.selected.log_file.path
                }).then(function(data) {
                    $scope.selected.search = '';
                    $scope.log_content = data;
                }, function() {
                    console.log($scope.selected);
                })
            };

            $scope.searchLogs = function() {
                if ($scope.selected.search.length < 3) {
                    return;
                }
                $scope.selected.log_file = '';
                $scope.log_content = '';
                $timeout(function() {
                    $scope.run.customGET('search_log', {
                        query: $scope.selected.search,
                        context: $scope.selected.context
                    }).then(function(data) {
                        $scope.log_content = data || 'No content found';
                    }, function() {
                        console.log($scope.selected);
                    });
                });
            };

            $scope.$watch('run', function(run) {
                if (run.id) {
                    $timeout(function() {
                        $scope.run.customGET('log_files')
                        .then(function(data) {
                            $scope.log_files = data;
                        }, function() {
                            $scope.log_files = [];
                        });
                    });
                }
            });

            $scope.selectLabel = function(f) {
                return f.name + ' (' + $filter('byteFmt')(f.size, 2) + ')';
            };
        }
    ]);
}());
