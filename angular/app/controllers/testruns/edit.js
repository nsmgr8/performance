(function() {
    angular.module('performance')
    .controller('TestRunEditController', [
        '$scope',
        'Restangular',
        '$state',
        function($scope, Restangular, $state) {
            $scope.saveEdit = function() {
                if (!$scope.editForm.$valid)
                    return;

                $scope.run.save()
                .then(
                    function() {
                        noty({text: 'Changes saved successfully',
                              type: 'success'});
                        $state.go('testrun.detail', {runId: $scope.run.id});
                    },
                    function() {
                        noty({text: 'Could not save changes',
                              type: 'error'});
                    }
                );
            };
        }
    ]);
}());
