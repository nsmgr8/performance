(function() {
    angular.module('performance')
    .controller('TestRunScriptController', [
        '$scope',
        '$state',
        '$timeout',
        'Restangular',
        function($scope, $state, $timeout, Restangular) {
            $scope.start = function(form) {
                if (form.$invalid) {
                    noty({text: 'Please fill required fields', type: 'warning'});
                    return;
                }

                noty({text: 'Please wait, starting the test...', type: 'info'});

                $scope.run.customPOST($scope.script_conf, 'start')
                .then(function(run) {
                    noty({text: 'Test is ' + run.status + ' now...', type: 'info'});
                    $state.go('testrun.detail', {runId: run.id});
                    $scope.updateRun();
                });
            };

            $scope.$watch('run', function(run) {
                if (run.id) {
                    if (run.status !== 'Waiting' || run.script_name === 'custom' || !run.script_name) {
                        $timeout(function() { $state.go('testrun.detail', {runId: run.id}); });
                        return;
                    }

                    $scope.script_conf = $.extend({}, conf_defaults[run.script_name], run.script_inputs);
                    $scope.choices = form_choices[run.script_name];

                    var rtype = resource_types[run.script_name];
                    if (rtype) {
                        Restangular.all('resources').getList({rtype: rtype})
                        .then(function(resources) {
                            $scope.choices[rtype] = resources.map(function(x) {
                                return {
                                    id: x.id.toString(),
                                    title: x.title + ' ... (' + x.ip_address + ')'
                                };
                            });
                        });
                    }
                }
            });
        }
    ]);

    var resource_types = {
        'polyrun': 'cachebox',
        'dhcperf': 'dnsbox'
    };

    var form_choices = {
        polyrun: {
            recipe: [
                {label: 'Recipe 2014', value: 'contents-2014.pg'},
                {label: 'Recipe 2011', value: 'contents-2011.pg'},
                {label: 'Recipe 2014 (LARGE)', value: 'contents-2014-BIG.pg'}
            ],
            polyrig: [
                {value: '1', label: 'Polyrig One'},
                {value: '1-1', label: 'Polyrig (Half of) One'},
                {value: '1-2', label: 'Polyrig (Other Half of) One'},
                {value: '2', label: 'Polyrig Two'},
                {value: '2-1', label: 'Polyrig (Half of) Two'},
                {value: '2-2', label: 'Polyrig (Other Half of) Two'}
            ],
            schedule_type: [
                {value: 'steps', label: 'Steps'},
                {value: 'ramp', label: 'Ramp'},
                {value: 'constant', label: 'Constant'}
            ],
            proxy_type: [
                {value: 'explicit', label: 'Explicit proxy'},
                {value: 'transparent', label: 'Transparent proxy'},
                {value: 'bridge', label: 'Bridge mode'},
                {value: 'bypass', label: 'Bypass proxy'},
                {value: 'reverse', label: 'Reverse proxy'}
            ],
            cache_init: [
                {value: 'quickstart', label: 'Quickstart'},
                {value: 'reload', label: 'Restart squid'},
                {value: 'init', label: 'Initialise CACHEbox'},
                {value: 'reboot', label: 'Reboot All'},
                {value: 'non-cachebox', label: 'Non-CACHEbox'}
            ],
            cache_engine: [
                {value: 'squid2', label: 'Squid 2'},
                {value: 'squid3', label: 'Squid 3'}
            ],
            yes_no: [
                {value: true, label: 'Yes'},
                {value: false, label: 'No'}
            ]
        },
        dhcperf: {
        }
    };

    var conf_defaults = {
        polyrun: {
            recipe: 'contents-2014.pg',
            polyrig: '1',
            schedule_type: 'steps',
            duration: 10,
            proxy_type: 'explicit',
            cache_init: 'quickstart',
            cache_engine: 'squid3',
            cache_size: 1000,
            fill_rate: 0,
            ranges: 0,
            repeatable: false,
            test_cdn: false
        },
        dhcperf: {
            port: 6700
        }
    };
}());
