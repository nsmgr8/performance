(function() {
    angular.module('performance')
    .controller('TestRunResultController', [
        '$scope',
        '$timeout',
        '$mdDialog',
        function($scope, $timeout, $mdDialog) {
            $scope.$watch('run', function() {
                var script = $scope.run.script_name,
                    data = $scope.run.script_outputs,
                    draw_func = {
                        'polyrun': d3.polyrun.draw,
                        'dhcperf': d3.dhcperf.draw
                    }[script];

                if (draw_func) {
                    $timeout(function() {
                        draw_func('.output-graph', data);
                    });
                }
            });

            $scope.polyrunDetail = function() {
                $mdDialog.show({
                    templateUrl: 'template/dialogs/polyrun.html',
                    controller: polyrun_report_dialog($scope.run.id)
                });
            };
        }
    ]);

    var polyrun_report_dialog = function(run_id) {
        return ['$scope', '$mdDialog', 'rootUrl', function($scope, $mdDialog, rootUrl) {
            $scope.frame = {
                url: rootUrl + 'assets/media/output/' + run_id + '/summary/index.html',
                width: $(window).width() - 260,
                height: $(window).height() - 400
            };
        }];
    };
}());
