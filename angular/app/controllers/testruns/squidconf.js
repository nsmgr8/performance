(function() {
    angular.module('performance')
    .controller('TestRunSquidConfController', [
        '$scope',
        'Restangular',
        function($scope, Restangular) {
            $scope.squid_conf = '';

            $scope.$watch('run', function(run) {
                if (run.id) {
                    $scope.run.customGET('squidconf')
                    .then(function(conf) {
                        $scope.squid_conf = conf;
                    }, function() {
                        $scope.squid_conf = 'Not found';
                    });
                }
            });
        }
    ]);
}());
