(function() {
    angular.module('performance')
    .controller('RRDController', [
        '$scope',
        'rootUrl',
        function($scope, rootUrl) {
            $scope.$watch('run', function(run) {
                if (run.id) {
                    var rrd_files = rootUrl + '/runs/' + run.id + '/rrd_files/';
                    d3.json(rrd_files, function(data) {
                        data = data.map(function(d) { return rootUrl + d; });
                        var rrd = d3.rrd.accordion('#rrd-accordion')
                        rrd.setRecipe(data, run.started_on, run.completed_on);
                    });
                }
            });
        }
    ]);
}());
