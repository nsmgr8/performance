(function() {
    angular.module('performance')
    .controller('TestRunDetailController', [
        '$scope',
        '$stateParams',
        '$state',
        'Restangular',
        '$timeout',
        '$mdDialog',
        'isScript',
        'perfSocket',
        function($scope, $stateParams, $state, Restangular, $timeout, $mdDialog, isScript, perfSocket) {
            var runId = $stateParams.runId;
            $scope.run = {};
            $scope.selectedTab = {index: 0};

            $scope.updateRun = function() {
                Restangular.all('tests').get(runId)
                .then(function(run) {
                    $scope.run = run;
                },
                function(response) {
                    if (response.status === 404) {
                        $state.go('not-found');
                    }
                });
            };
            $scope.updateRun();

            perfSocket.on('refresh', function(data) {
                if (data.id === $scope.run.id) {
                    noty({text: 'Test run state changed. Refreshing...',
                          type: 'warning'});
                    $scope.updateRun();
                }
            });

            perfSocket.on('status', function(stat) {
                if (stat.run_pk !== $scope.run.id ) {
                    return;
                }
                d3.select('#progress-wait').remove();
                if (stat.script === 'polyrun') {
                    var pid = stat.title.replace(/ /g, '-').toLowerCase();
                    if ($('dd.' + pid).text() === stat.message) {
                        $('dd.' + pid).css('color', 'red');
                    } else {
                        $('dd.' + pid).css('color', 'black');
                    }
                    var dl = d3.select('#progress-status');
                    var dt = dl.selectAll('dt.'+pid).data([0])
                    .text(stat.title);
                    dt.enter().append('dt').attr('class', pid)
                    .text(stat.title);
                    var dd = dl.selectAll('dd.'+pid).data([0])
                    .text(stat.message);
                    dd.enter().append('dd').attr('class', pid)
                    .text(stat.message);
                }
                else if (stat.script === 'dhcperf') {
                    var prog = d3.select('#progress-ts');
                    prog.selectAll('h3, h4, ul').remove();
                    if (!stat.data.low)
                        return;
                    var pool = 'Pool: ' + stat.data.low + ' - ' + stat.data.high;
                    prog.append('h3').text(pool);
                    prog.append('h4').text(stat.clients);
                    var ul = prog.append('ul');
                    var li = ul.append('li');
                    li.append('strong').text('Discover:');
                    li.append('span').text(stat.result.discover.qps);
                    var li = ul.append('li');
                    li.append('strong').text('Renew:');
                    li.append('span').text(stat.result.renew.qps);
                }
            });

            $scope.resource_state_icon = function(resource) {
                return {
                    'In use': 'spinner',
                    'Idle': 'power-off',
                    'Unavailable': 'ban'
                }[resource.status];
            };

            $scope.resetTest = function() {
                $mdDialog.show({
                    templateUrl: 'template/dialogs/action.html',
                    controller: get_action_dialog($scope.run.status === 'Running')
                }).then(function(action) {
                    noty({text: 'Please wait, test run ' + action + ' is in progress...',
                          type: 'warning'});
                    $scope.run.customPUT($scope.run, 'reset', {action: action})
                    .then(function(run) {
                        noty({text: 'TestRun ' + action + ' is successful',
                              type: 'success'});
                        $scope.run = run;
                    }, function() {
                        noty({text: 'TestRun ' + action + ' failed',
                              type: 'error'});
                    });
                });
            };

            $scope.cloneTest = function() {
                noty({text: 'Please wait, cloning testrun', type: 'warning'});
                $scope.run.customPOST($scope.run, 'clone')
                .then(function(data) {
                    noty({text: 'TestRun cloning is successful', type: 'success'});
                    $state.go('testrun.detail', {runId: data.id});
                }, function() {
                    noty({text: 'TestRun clone failed', type: 'error'});
                });
            };

            $scope.runScript = function() {
                if (isScript($scope.run.script_name)) {
                    $state.go('testrun.runscript', {runId: $scope.run.id});
                }
                else if ($scope.run.script_name === 'custom') {
                    noty({text: 'Please wait, starting the test...', type: 'info'});
                    $scope.run.customPOST({}, 'start')
                    .then(function(run) {
                        noty({text: 'Test is running now...', type: 'info'});
                        $scope.updateRun();
                    });
                }
                else {
                    $mdDialog.show({
                        templateUrl: 'template/dialogs/select_script.html',
                        controller: get_action_dialog()
                    }).then(function(script) {
                        $scope.run.script = script;
                        $scope.run.save().then(function(run) {
                            $scope.run = run;
                            $scope.runScript();
                        });
                    });
                }
            };

            $scope.hasProgress = function() {
                return ($scope.run.status === 'Running' &&
                        isScript($scope.run.script_name));
            };

            $scope.$watch('run', function() {
                $timeout(function() {
                    if (!$scope.run.id) {
                        return;
                    }

                    $scope.run_css_class = get_run_css_class($scope.run.status);
                    $scope.run.theme = get_run_theme($scope.run.status);

                    $scope.tabs = [];
                    $.each(tabs, function(i, x) {
                        if (x.show($scope.run)) {
                            $scope.tabs.push(x);
                        }
                    });
                    $.each($scope.tabs, function(i, x) {
                        if ('testrun.' + x.state === $state.current.name) {
                            $scope.selectedTab.index = i;
                        }
                    });
                }, 10);
            });

            $scope.setState = function(state) {
                if ($scope.run.id) {
                    $state.go('testrun.' + state, {runId: $scope.run.id});
                }
            };

            var tabs = [{
                label: 'Detail',
                state: 'detail',
                show: function(run) {
                    return true;
                }
            },
            {
                label: 'Edit',
                state: 'edit',
                show: function(run) {
                    return true;
                }
            },
            {
                label: 'Run',
                state: 'runscript',
                show: function(run) {
                    return run.status === 'Waiting' && isScript(run.script_name);
                }
            },
            {
                label: 'Result',
                state: 'result',
                show: function(run) {
                    return run.state > 10;
                }
            },
            {
                label: 'RRDGraph',
                state: 'rrd',
                show: function(run) {
                    return run.has_rrd;
                }
            },
            {
                label: 'Logs',
                state: 'logs',
                show: function(run) {
                    return run.has_logs;
                }
            },
            {
                label: 'squid.conf',
                state: 'squidconf',
                show: function(run) {
                    return run.has_squid_conf;
                }
            }];
        }
    ]);

    var get_run_css_class = function(state) {
        var state_cls = {
            'Successful': 'success',
            'Failed': 'danger',
            'Stopped': 'warning',
            'Running': 'primary',
            'Scheduled': 'info',
            'Waiting': 'default'
        };
        return state_cls[state] || 'default';
    };

    var get_run_theme = function(state) {
        var state_cls = {
            'Successful': 'green',
            'Failed': 'red',
            'Stopped': 'orange',
            'Running': 'indigo',
            'Scheduled': 'purple',
            'Waiting': 'grey'
        };
        return state_cls[state] || 'light-blue';
    };

    var get_action_dialog = function(is_running) {
        return ['$scope', '$mdDialog', function($scope, $mdDialog) {
            $scope.is_running = is_running;
            $scope.accept = function() { $mdDialog.hide(); };
            $scope.cancel = function() { $mdDialog.cancel(); };
            $scope.select = function(script) { $mdDialog.hide(script); };
        }];
    };
}());

