(function() {
    angular.module('performance')
    .controller('CompareController', [
        '$scope',
        '$state',
        '$stateParams',
        'Restangular',
        function($scope, $state, $stateParams, Restangular) {
            $scope.testruns = [];
            $scope.gridOptions = {};
            $scope.selected = {index: 0};
            $scope.title = $stateParams.type;

            var qs = {};
            qs[$stateParams.type] = $stateParams.values;

            Restangular.all('tests').customGET('compare', qs)
            .then(
                function(testruns) {
                    var columns = [],
                        inputs = d3.set(),
                        outputs = d3.set();

                    testruns.forEach(function(row) {
                        d3.keys(row.inputs).forEach(function(e) { inputs.add(e); });
                        d3.keys(row.outputs).forEach(function(e) { outputs.add(e); });
                    });
                    inputs.remove('');
                    outputs.remove('');

                    inputs = inputs.values().sort().map(function(d) {
                        return {
                            'class': 'text-right',
                            data: function(row, type, val, meta) {
                                return row.inputs[d] ? row.inputs[d] : '';
                            },
                            title: d
                        };
                    })

                    outputs = outputs.values().sort().map(function(d) {
                        return {
                            'class': 'text-right',
                            data: function(row, type, val, meta) {
                                if (!row.outputs[d])
                                    return '';

                                if (_.contains(['BHR', 'DHR', 'Mbps', 'RPS'], d)) {
                                    var x = row.outputs[d].split('/');
                                    if (type === 'display') {
                                        return x[0] + '<br><small class="text-muted">' + x[1] + '</small>';
                                    } else {
                                        return x[0];
                                    }
                                }
                                else if (d === '% Errors') {
                                    if (type === 'display') {
                                        return row.outputs[d] + '%';
                                    } else {
                                        return row.outputs[d];
                                    }
                                }
                                return row.outputs[d];
                            },
                            title: d
                        };
                    });

                    var columns =  [{
                        data: function(row, type, val, meta) {
                            if (type === 'display') {
                                var url = $state.href('testrun.detail', {runId: row.id});
                                return '<a href="/' + url + '">' + row.title + '</a>';
                            }
                            return row.title;
                        },
                        title: 'Title',
                        'class': 'min-width-cell'
                    },
                    {
                        data: function(row, type, val, meta) {
                            if (type === 'display')
                                return moment(row.completed_on).format('LL');
                            return row.completed_on;
                        },
                        title: 'Date',
                        'class': 'text-right'
                    }];

                    columns = columns.concat(outputs);
                    columns = columns.concat(inputs);

                    var height = $(window).height() - 180;
                    var table = $('.compare-grid').dataTable({
                        data: testruns,
                        columns: columns,
                        scrollX: true,
                        scrollY: height + 'px',
                        scrollCollapse: true,
                        dom: 'ift',
                        language: {info: 'Showing _PAGES_ completed test runs'},
                        paging: false
                    });
                },
                function(response) {
                    console.log(response);
                }
            );
        }
    ]);
}());
