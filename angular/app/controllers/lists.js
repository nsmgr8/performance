(function() {
    angular.module('performance')
    .controller('ListController', [
        '$scope',
        '$state',
        'Restangular',
        '$mdSidenav',
        '$timeout',
        'perfSocket',
        function($scope, $state, Restangular, $mdSidenav, $timeout, perfSocket) {
            $scope.testruns = [];
            $scope.resources = [];
            $scope.tags = [];

            var loadLists = function() {
                Restangular.all('tests').getList()
                .then(function(runs) {
                    $scope.testruns = runs;
                });

                Restangular.all('resources').getList()
                .then(function(resources) {
                    $scope.resources = resources;
                });

                Restangular.all('tags').getList()
                .then(function(tags) {
                    $scope.tags = tags.filter(function(t) {
                        return t.num_items > 0;
                    }).sort(function(a, b) {
                        return b.num_items - a.num_items;
                    });
                });
            };
            loadLists();

            $scope.run_states = [
                {name: 'All', value: '', comparable: true},
                {name: 'Successful', value: 'Successful', comparable: true},
                {name: 'Failed', value: 'Failed', comparable: true},
                {name: 'Stopped', value: 'Stopped', comparable: true},
                {name: 'Running', value: 'Running'},
                {name: 'Scheduled', value: 'Scheduled'},
                {name: 'Waiting', value: 'Waiting'}
            ];
            $scope.run_state = {selected: $scope.run_states[6]};

            $scope.resource_states = [
                {name: 'All', value: ''},
                {name: 'Idle', value: 'Idle'},
                {name: 'In Use', value: 'In Use'},
                {name: 'Unavailable', value: 'Unavailable'}
            ];
            $scope.resource_state = {selected: $scope.resource_states[0]};

            $scope.compareTests = function(type) {
                var values = [];
                for (var val in $scope.selected[type]) {
                    if ($scope.selected[type][val]) {
                        values.push(val);
                    }
                }
                if (values.length > 0) {
                    $state.go('compare', {type: type, values: values.join()});
                }
                else {
                    noty({text: 'Please select some ' + type + ' to compare.'});
                }
            };

            $scope.clearSelection = function() {
                $scope.selected = {tags: {}, runs: {}};
            };
            $scope.clearSelection();

            $scope.closeSidebar = function(cid) {
                $timeout(function() {
                    $mdSidenav(cid).close();
                });
            };

            perfSocket.on('refresh', function(stat) {
                loadLists();
            });
        }
    ]);
}());