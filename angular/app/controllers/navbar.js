(function() {
    angular.module('performance')
    .controller('NavbarController', [
        '$scope',
        '$http',
        '$timeout',
        '$interval',
        '$mdSidenav',
        '$state',
        '$mdDialog',
        'rootUrl',
        'perfSocket',
        function($scope, $http, $timeout, $interval, $mdSidenav, $state, $mdDialog, rootUrl, perfSocket) {
            var reload_timeout = 5 * 1000,
                update_interval = window.location.hostname === 'localhost' ? reload_timeout : 5 * 60 * 1000,
                last_modified = +(moment());

            var get_user = function() {
                $http.get(rootUrl + '/current-user/')
                .success(function(user) {
                    $scope.user = user;
                });
            };

            get_user();
            $interval(get_user, update_interval);

            $interval(function() {
                $http.get(rootUrl + 'ng-last-modified/')
                .success(function(data) {
                    var need_reload = false;
                    angular.forEach(data, function(ts) {
                        if (ts * 1000 > last_modified) {
                            need_reload = true;
                        }
                    });

                    if (need_reload) {
                        noty({text: 'There are site updates available!<br>Page will be refreshed in 5 seconds.',
                              title: 'Reload',
                              type: 'warning',
                              layout: 'bottom'});
                        $timeout(function() {
                            window.location.reload();
                        }, reload_timeout);
                    }
                });
            }, update_interval);

            $scope.status_logs = set_status_logs();
            perfSocket.on('status', function(data) {
                noty({type: data.level, text: '<h4>' + data.title + '</h4>' + data.message});

                $scope.status_logs = set_status_logs({
                    time: +moment(),
                    level: data.level === 'error' ? 'danger' : data.level,
                    title: data.title,
                    message: data.message
                });
            });

            $scope.openSidebar = function(cid) {
                $timeout(function() {
                    $mdSidenav(cid).open();
                });
            };

            $scope.closeSidebar = function(cid) {
                $timeout(function() {
                    $mdSidenav(cid).close();
                });
            };

            $scope.showNewDialog = function() {
                $mdDialog.show({
                    templateUrl: 'template/dialogs/new.html',
                    controller: ['$scope', '$state', '$mdDialog', NewDialogController]
                });
            };
        }
    ]);

    var NewDialogController = function($scope, $state, $mdDialog) {
        $scope.newTestRun = function() {
            $mdDialog.hide();
            $state.go('new_testrun');
        };

        $scope.newResource = function() {
            $mdDialog.hide();
            $state.go('new_resource');
        };
    };

    var set_status_logs = function(log) {
        var perf_logs = JSON.parse(localStorage.getItem('perf_logs')) || [],
            info_logs = JSON.parse(localStorage.getItem('info_logs')) || [];

        if (log && log.level) {
            if (_.contains(['error', 'warning'], log.level)) {
                perf_logs.push(log);
                if (perf_logs.length > 30) {
                    perf_logs.shift();
                }
                localStorage.setItem('perf_logs', JSON.stringify(perf_logs));
            } else {
                info_logs.push(log);
                if (info_logs.length > 30) {
                    info_logs.shift();
                }
                localStorage.setItem('info_logs', JSON.stringify(info_logs));
            }
        }

        return perf_logs.concat(info_logs).sort(function(a, b) {
            return moment(b.time) - moment(a.time);
        });
    };
}());
