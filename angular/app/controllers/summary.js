(function() {
    angular.module('performance')
    .controller('SummaryController', [
        '$scope',
        '$http',
        'rootUrl',
        '$timeout',
        function($scope, $http, rootUrl, $timeout) {
            $scope.summary = {};
            $scope.selected = {index: 0};
            $scope.col_defs = [
                {name: 'model',
                 cellClass: 'ui-grid-cell-contents text-right',
                 cellTemplate: '<div><a href="/#/run/{{row.entity.run_pk}}">{{row.entity.model}}</a></div>'},
                {name: 'version', cellClass: 'text-right'},
                {name: 'engine', displayName: 'Cache engine', cellClass: 'text-right'},
                {name: 'RPS', displayName: 'RPS', cellClass: 'text-right'},
                {name: 'BHR', displayName: 'BHR', cellClass: 'text-right'},
                {name: 'DHR', displayName: 'DHR', cellClass: 'text-right'},
                {name: 'Mbps', displayName: 'Mbps', cellClass: 'text-right'},
            ];

            $http.get(rootUrl + '/performance-summary')
            .success(function(data) {
                $scope.summary = data;
            });
        }
    ]);
}());
