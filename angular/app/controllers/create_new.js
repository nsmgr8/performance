(function() {
    angular.module('performance')
    .controller('NewController', [
        '$scope',
        '$state',
        'Restangular',
        'isScript',
        'perfSocket',
        function($scope, $state, Restangular, isScript, perfSocket) {
            $scope.object = {};
            $scope.createNew = function(endpoint) {
                Restangular.all(endpoint).post($scope.object)
                .then(function(object) {
                    var state = {
                        'resources': {
                            type: 'Resource',
                            name: 'resourcedetail',
                            param: {resourceId: object.id}
                        },
                        'tests': {
                            type: 'Test Run',
                            name: isScript(object.script) ? 'testrun.runscript' : 'testrun.detail',
                            param: {runId: object.id}
                        }
                    }[endpoint];

                    perfSocket.emit('refresh');
                    noty({text: state.type + ' creation successful', type: 'success'});

                    $state.go(state.name, state.param);
                });
            };
        }
    ]);
}());
