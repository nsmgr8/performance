(function() {
    angular.module('performance')
    .controller('ResourceEditController', [
        '$scope',
        'Restangular',
        '$state',
        function($scope, Restangular, $state) {
            $scope.saveEdit = function() {
                if (!$scope.editForm.$valid)
                    return;

                $scope.resource.save()
                .then(
                    function() {
                        noty({text: 'Changes saved successfully',
                              type: 'success'});
                        $state.go('resource.detail', {resourceId: $scope.resource.id});
                    },
                    function() {
                        noty({text: 'Could not save changes',
                              type: 'error'});
                    }
                );
            };
        }
    ]);
}());

