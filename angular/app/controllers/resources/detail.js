(function() {
    angular.module('performance')
    .controller('ResourceDetailController', [
        '$scope',
        '$state',
        'Restangular',
        function($scope, $state, Restangular) {
            $scope.resource = {};
            $scope.runs = [];

            var resourceId = $state.params.resourceId;
            Restangular.all('resources').get(resourceId)
            .then(function(resource) {
                $scope.resource = resource;
                $scope.runs = Restangular.all('tests').getList({resource: resource.id}).$object;
            });

            $scope.selectedTab = {index: 0};
            $scope.setState = function(state) {
                if ($scope.resource.id) {
                    $state.go('resource.' + state, {resourceId: $scope.resource.id});
                }
            };

            $scope.selectedRuns = [];
            $scope.compareTests = function() {
                var values = [];
                for (var val in $scope.selectedRuns) {
                    if ($scope.selectedRuns[val]) {
                        values.push(val);
                    }
                }
                if (values.length > 0) {
                    $state.go('compare', {type: 'runs', values: values.join()});
                }
                else {
                    noty({text: 'Please select some Test Runs to compare.'});
                }
            };

        }
    ]);
}());

