(function() {
    angular.module('performance')
    .factory('isScript', [
        'testScripts',
        function(testScripts) {
            return function(name) {
                return _.contains(testScripts, name);
            };
        }
    ])
    .factory('perfSocket', [
        'socketFactory',
        function(socketFactory) {
            var socketio_location = '/performance';
            if (window.location.hostname === 'localhost') {
                socketio_location = '//localhost:8001' + socketio_location;
            }
            return socketFactory({ioSocket: io.connect(socketio_location)});
        }
    ]);
}());
