(function() {
    angular.module('performance')
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider
            .when('', '/')
            .otherwise('/not-found');

            $stateProvider
            // 404
            .state('not-found', {
                url: '/not-found',
                templateProvider: get_template('template/404.html')
            })
            // home
            .state('main', {
                url: '/',
                templateProvider: get_template('template/summary.html'),
                controller: 'SummaryController'
            })
            // create new
            .state('new_testrun', {
                url: '/new/testrun',
                templateProvider: get_template('template/forms/testrun.html'),
                controller: 'NewController'
            })
            .state('new_resource', {
                url: '/new/resource',
                templateProvider: get_template('template/forms/resource.html'),
                controller: 'NewController'
            })
            // testrun
            .state('testrun', {
                abstract: true,
                url: '/run/{runId:[0-9]+}',
                templateProvider: get_template('template/testruns/main.html'),
                controller: 'TestRunDetailController'
            })
            .state('testrun.detail', {
                url: '',
                templateProvider: get_template('template/testruns/detail.html')
            })
            .state('testrun.edit', {
                url: '/edit',
                templateProvider: get_template('template/testruns/edit.html'),
                controller: 'TestRunEditController'
            })
            .state('testrun.result', {
                url: '/result',
                templateProvider: get_template('template/testruns/result.html'),
                controller: 'TestRunResultController'
            })
            .state('testrun.rrd', {
                url: '/rrd',
                templateProvider: get_template('template/testruns/rrd.html'),
                controller: 'RRDController'
            })
            .state('testrun.logs', {
                url: '/logs',
                templateProvider: get_template('template/testruns/logs.html'),
                controller: 'TestRunLogsController'
            })
            .state('testrun.squidconf', {
                url: '/squidconf',
                templateProvider: get_template('template/testruns/squidconf.html'),
                controller: 'TestRunSquidConfController'
            })
            .state('testrun.runscript', {
                url: '/run',
                templateProvider: get_template('template/testruns/script.html'),
                controller: 'TestRunScriptController'
            })
            // compare, analyse
            .state('compare', {
                url: '/compare/{type:.+}/{values:.+}',
                templateProvider: get_template('template/compare_testruns.html'),
                controller: 'CompareController'
            })
            // resource
            .state('resource', {
                abstract: true,
                url: '/resource/{resourceId:[0-9]+}',
                templateProvider: get_template('template/resources/main.html'),
                controller: 'ResourceDetailController'
            })
            .state('resource.detail', {
                url: '',
                templateProvider: get_template('template/resources/detail.html')
            })
            .state('resource.edit', {
                url: '/edit',
                templateProvider: get_template('template/resources/edit.html'),
                controller: 'ResourceEditController'
            })
            .state('cdnrun', {
                url: '/cdn',
                abstract: true,
                templateProvider: get_template('template/cdnrun/main.html'),
            })
            .state('cdnrun.summary', {
                url: '',
                templateProvider: get_template('template/cdnrun/summary.html'),
                controller: 'CDNRunSummaryController'
            })
            .state('cdnrun.detail', {
                url: '/{cdnId:[0-9]+}',
                templateProvider: get_template('template/cdnrun/detail.html'),
                controller: 'CDNRunDetailController'
            })
            ;
        }
    ]);

    var get_template = function(path) {
        return [
            '$templateCache',
            function($templateCache) {
                return $templateCache.get(path);
            }
        ];
    };
}());


