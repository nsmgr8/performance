(function() {
    angular.module('performance')
    .constant('rootUrl', window.location.port === '9000' ? '//localhost:8000' : '')
    .constant('testScripts', ['polyrun', 'dhcperf']);
}());

