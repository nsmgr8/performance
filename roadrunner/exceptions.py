"""
(c) 2015, ApplianSys Ltd

General user defined exceptions
"""


class RoadrunnerException(Exception):
    """
    A base exception for errors raised by roadrunner.
    """
    pass


class ApplianceApiError(RoadrunnerException):
    """
    A base exception for errors raised by the appliance_api - errors which are
    expected and can be shown to the user as information - as opposed to errors
    which should not normally happen and which represent a bug in the system.
    """
    pass


class PluginLoadError(ApplianceApiError):
    """
    A plugin could not be loaded or initialised correctly
    """
    pass


class MethodUnknownError(ApplianceApiError, AttributeError):
    """
    API dispatcher could not find the requested method name

    This subclasses AttributeError so that hasattr will work properly.
    """
    pass


class NetworkConfigurationError(RoadrunnerException):
    """
    Raised when the proposed network configuration is not sane
    """
    pass


class DatabaseNotReadyError(RoadrunnerException):
    """
    Raised if trying to initialise a connection to a database
    but can't because a database server (e.g. postgres) isn't
    yet ready
    """
    pass


class CorruptOrInvalidBundle(RoadrunnerException):
    """
    Raised for a corrupt or invalid information bundle (i.e. a set of
    files representing some object
    """
    pass


class MissingReportError(RoadrunnerException):
    """
    Raised when a requested report is not available
    """
    pass


class CannotPerformAction(RoadrunnerException):
    """
    A general error when an action cannot be performed
    """
    pass


class OperationFailed(RoadrunnerException):
    """
    A general error when an action was attempted but failed
    """
    pass


class LicenceException(RoadrunnerException):
    """
    An error due to licensing or licence restriction
    """
    pass


class LicenceRestrictionError(LicenceException):
    """
    Licence (or lack thereof) prohibition
    """
    pass


class InvalidLicenceException(LicenceException):
    """
    A licence is invalid for some reason
    """
    pass


class ExpiredLicenceException(InvalidLicenceException):
    """
    A licence has expired.
    """
    pass


class InvalidSyntax(RoadrunnerException):
    """
    Raised if the API has tried to parse a string and the syntax is invalid
    """
    pass


class InvalidFile(RoadrunnerException):
    """
    Raise if an uploaded file was invalid
    """
    pass


class NoReportError(RoadrunnerException):
    """
    Raise no report has been generated.

    This is not the same as a MissingReportError.
    """
    pass


class CertsError(RoadrunnerException):
    """
    Raised when certificate related error generated.
    """


class DataError(ApplianceApiError):
    """
    An operation is unavailable because of missing or incorrect data.
    """
    pass


class ConfigBackupError(ApplianceApiError):
    """
    An error which occur during configuration backup.
    """
    pass


class ConfigRestoreError(ApplianceApiError):
    """
    An error which occur during configuration restoration.
    """
    pass


class ConfigRestoreFileNotFound(ConfigRestoreError):
    """
    An error when trying to restore a non-existent backup
    """
    pass


class LogReadError(ApplianceApiError):
    """
    raised on failure to read a log file
    """
    pass


class TraceReadError(ApplianceApiError):
    """
    raised on failure to read a backtrace file
    """


class FirmwareInstallError(ApplianceApiError):
    """
    raised on failure to install a firmware
    """


class PatchInstallError(ApplianceApiError):
    """
    raised on failure to install a firmware
    """


class FirmwareDownloadError(FirmwareInstallError):
    """
    raised on failure to download a firmware
    """


class PatchDownloadError(FirmwareInstallError):
    """
    raised on failure to download a firmware
    """


class ServiceNotReadyError(ApplianceApiError):
    """
    raised if API asked to do something and a required service is not ready
    """


class UserDoesNotExistError(ApplianceApiError):
    """
    raise if trying to do something with a user but that user doesn't exist
    """


class SendEmailError(ApplianceApiError):
    """
    raise if sending an email fails, e.g. for "save and test" in Network / Email settings
    """


class DhcpClientError(ApplianceApiError):
    """
    raised if communication with a dhcp server fails.
    """


class PatchError(ApplianceApiError):
    """
    Patch server errors
    """


class UpgradeHookException(FirmwareInstallError):
    """
    Custom exception for raising upgrade hook errors
    """
    pass


class SlaveSSHKnownHost(RoadrunnerException):
    """
    Raised if the remote server SSH known hosts check fails.
    """
    def __init__(self, msg=None):
        self.message = ("Remote server host key has changed. "
                        "If the remote server has been replaced or "
                        "the IP address has changed, this is expected. "
                        "Otherwise this might indicate "
                        "network or security errors.")

    def __str__(self):
        return repr(self.message)


class ServiceLockTimeout(RoadrunnerException):
    """
    Raised when the service lock cannot be acquired
    """
    pass


class ServiceUnknown(RoadrunnerException):
    """
    This service is not known on this appliance
    """
    pass


class RecordNotFound(RoadrunnerException):
    """
    The desired record is not in the database
    """
    pass


class TimeError(RoadrunnerException):
    """
    The time is invalid
    """
    pass
