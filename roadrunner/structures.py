
# (c) 2009, ApplianSys. All Rights Reserved

# Useful data structures
import queue
from collections import deque


class Bunch(dict):
    """
    An extension to dict that also acts like object
    """
    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            raise AttributeError(repr(key))

    def __setattr__(self, key, value):
        if callable(value):
            # by not allowing this to store callables, we can
            # prevent methods being assigned to.
            raise ValueError("%s object cannot store callables" % (self.__class__.__name__,))
        if hasattr(self, key) and callable(getattr(self, key)):
            raise AttributeError("%s is readonly" % (repr(key)))
        self[key] = value

    def __delattr__(self, key):
        try:
            del self[key]
        except KeyError:
            raise AttributeError(repr(key))

    def __dir__(self):
        return list(self.keys())

    def __call__(self, **kwargs):
        """
        Clones the current Bunch and updates the specified fields
        """
        new = Bunch(**self)
        new.update(**kwargs)
        return new


class LossyQueue(queue.Queue):
    # see http://bugs.python.org/issue7337
    def _init(self, maxsize):
        if maxsize > 0:
            # build the deque with maxsize limit
            self.queue = deque(maxlen=maxsize)
        else:
            # same as normal Queue instance
            self.queue = deque()
        # deque alone handles maxsize,
        # so we pretend we have none.
        self.maxsize = 0
