import atexit
import os
from multiprocessing.dummy import Pool as ThreadPool

import paramiko


class SSHConnection(object):
    def __init__(self, *, address, username=None, password=None):
        self.address = address
        self.username = username
        self.password = password

        if '@' in address and username is None:
            self.username, self.address = address.split('@')

        self.ssh_client = paramiko.SSHClient()
        self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        atexit.register(self.close)

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def open(self):
        if self.is_alive():
            return

        kw = {'hostname': self.address}
        if self.username:
            kw['username'] = self.username
        if self.password:
            kw['password'] = self.password

        self.ssh_client.connect(**kw)

    def close(self):
        if self.is_alive():
            self.ssh_client.close()

    def is_alive(self):
        if self.ssh_client._transport:
            return self.ssh_client._transport.is_active()

    def run(self, command):
        self.open()
        _, stdout, stderr = self.ssh_client.exec_command(command)
        stdout = stdout.read().decode().strip()
        stderr = stderr.read().decode().strip()
        return stdout, stderr

    def sudo(self, command):
        return self.run('sudo {}'.format(command))

    def get(self, remote_path, local_path):
        self.open()
        with self.ssh_client.open_sftp() as ftp:
            if os.path.isdir(local_path):
                local_path = os.path.join(local_path,
                                          os.path.basename(remote_path))
            ftp.get(remote_path, local_path)

    def put(self, local_path, remote_path):
        self.open()
        with self.ssh_client.open_sftp() as ftp:
            ftp.put(local_path, remote_path)


def execute(func, hosts, **kwargs):
    pool = ThreadPool(4)
    results = pool.map(lambda host: func(host=host, **kwargs), hosts)
    pool.close()
    pool.join()
    return zip(hosts, results)
