# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import taggit.managers
from django.conf import settings
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('taggit', '0002_auto_20150616_2121'),
    ]

    operations = [
        migrations.CreateModel(
            name='Resource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('title', models.CharField(max_length=300)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('ip_address', models.IPAddressField(null=True, blank=True)),
                ('rtype', models.CharField(verbose_name='Type', max_length=300, blank=True)),
                ('description', models.TextField()),
                ('state', models.IntegerField(default=0, choices=[(0, 'Idle'), (10, 'In use'), (-1, 'Unavailable')])),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('ip_address', 'rtype', 'title'),
            },
        ),
        migrations.CreateModel(
            name='TestRun',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('title', models.CharField(max_length=300)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('description', models.TextField()),
                ('started_on', models.DateTimeField(null=True, blank=True)),
                ('completed_on', models.DateTimeField(null=True, blank=True)),
                ('eta', models.DateTimeField(null=True, blank=True)),
                ('estimated_days', models.FloatField(default=1.0)),
                ('queue', models.PositiveIntegerField(null=True, blank=True)),
                ('state', models.IntegerField(default=0, choices=[(0, 'Waiting'), (5, 'Scheduled'), (10, 'Running'), (30, 'Successful'), (25, 'Stopped'), (20, 'Failed')])),
                ('inputs', jsonfield.fields.JSONField(blank=True, default=dict)),
                ('outputs', jsonfield.fields.JSONField(blank=True, default=dict)),
                ('script', models.CharField(max_length=300, blank=True)),
                ('script_inputs', jsonfield.fields.JSONField(blank=True, default=dict)),
                ('script_outputs', jsonfield.fields.JSONField(blank=True, default=dict)),
                ('report', models.TextField(blank=True)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('resources', models.ManyToManyField(blank=True, to='performance.Resource')),
                ('tags', taggit.managers.TaggableManager(to='taggit.Tag', help_text='A comma-separated list of tags.', verbose_name='Tags', blank=True, through='taggit.TaggedItem')),
                ('team', models.ManyToManyField(null=True, blank=True, to=settings.AUTH_USER_MODEL, related_name='team')),
            ],
            options={
                'ordering': ('-state', '-completed_on', '-started_on', 'queue'),
            },
        ),
    ]
