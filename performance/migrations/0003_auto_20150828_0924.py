# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0002_auto_20150828_0923'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testrun',
            name='team',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, blank=True, related_name='team'),
        ),
    ]
