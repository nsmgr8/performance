"""
Django admin for performance
============================

Registers performance models to django admin
"""

from django.contrib import admin

# from reversion_compare.admin import CompareVersionAdmin
from suit.admin import SortableModelAdmin
from attachments.admin import AttachmentInlines

from . import models
from .forms import TestRunChangeForm


class ResourceAdmin(admin.ModelAdmin):
    """
    Resource admin options
    """
    list_display = ('title', 'created_by', 'created_on', 'state')
    list_filter = ('state',)


class TestRunAdmin(SortableModelAdmin):
    """
    TestRun admin options
    """
    list_display = ('title', 'created_by', 'created_on', 'started_on',
                    'completed_on', 'estimated_days', 'state')
    list_filter = ('created_by', 'created_on', 'state')
    sortable = 'queue'
    form = TestRunChangeForm
    inlines = [AttachmentInlines]


admin.site.register(models.Resource, ResourceAdmin)
admin.site.register(models.TestRun, TestRunAdmin)
