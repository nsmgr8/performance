"""
Template context processors
===========================
"""


def context(request):
    """
    Default context processor that is used in all templates

    :returns: dictionary of template context
    """
    active_tab = request.path_info.split('/', 2)[1]
    return {'active_tab': active_tab}
