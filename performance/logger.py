"""
Custom logger/handlers
======================
"""
import logging

from performance import socketioclient


class SocketIOHandler(logging.Handler):
    """
    Socket.IO logger

    It assumes that socket.io server is running at localhost:8001
    """
    def emit(self, record):
        """
        Log record emitter

        Emits a json object with level, title and message keys
        """
        try:
            extra_message = record.socketio
        except:
            extra_message = None

        try:
            with socketioclient.SocketIO('localhost', 8001) as sock:
                perf_sock = sock.define(socketioclient.BaseNamespace,
                                        '/performance')
                level = record.levelname.lower()
                perf_sock.emit('status', {'level': level,
                                          'title': record.name,
                                          'message': record.message})
                if extra_message:
                    perf_sock.emit(extra_message['emit'],
                                   extra_message.get('data'))
        except:
            pass
