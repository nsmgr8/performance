"""
Script runners
==============

Helper functions for setup and run scripts via celery
"""
import logging
from datetime import datetime

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.conf import settings

from .models import TestRun, Resource, TestRunError, ResourceInUse

from performance.tasks import polyrun, dhcperf
from polygraph.polyrun import Polyrun


def _get_appliance_ip(run, config, appliance):
    """
    Find a valid resource IP address

    :param run: :py:class:`performance.models.TestRun` object
    :param config: a dictionary of script inputs
    :param appliance: string for :py:attr:`performance.models.Resource.rtype`

    :returns: IP address of the matched resource

    :raises: :py:class:`performance.models.TestRunError`
        if multiple resource found or any other unknown error (logged)
    """
    try:
        appl = run.resources.get(rtype__iexact=appliance)
        if appl.pk != config[appliance]:
            run.resources.remove(appl)
            raise ObjectDoesNotExist
    except MultipleObjectsReturned:
        raise TestRunError('The test has mulitple {}'.format(appliance))
    except ObjectDoesNotExist:
        appl = Resource.objects.get(pk=config[appliance])
        run.resources.add(appl)
    except:
        logging.exception('Cannot start test')
        raise TestRunError('An unknown error occured')
    return appl.ip_address


def run_polyrun(run, config=None):
    """
    Send polyrun script to celery worker

    :param run: :py:class:`performance.models.TestRun` object
    :param config: a dictionary of script inputs
    """
    if config:
        name = '{0}_{1}'.format(datetime.now().strftime('%Y%m%d%H%M'), run.pk)
        config['name'] = name
        config['outdir'] = run.output_dir
        config['timeserver'] = settings.TIMESERVER

        if config['proxy_type'] != 'bypass':
            config['cachebox_ip'] = _get_appliance_ip(run, config, 'cachebox')
        else:
            run.resources.remove(
                *list(run.resources.filter(rtype__iexact='cachebox')))

        config.update(settings.POLYRIG_SETUP.get(config['polyrig']))

        run.resources.remove(
            *list(run.resources.filter(rtype__iexact='polygraph')))

        for poly in (config['polysrv_ips'] + config['polyclt_ips']):
            try:
                run.resources.add(Resource.objects.get(ip_address=poly))
            except ObjectDoesNotExist:
                raise TestRunError('No resource found at {0}'.format(poly))
        run.set_inputs(script='polyrun', inputs=config)
    else:
        config = run.script_inputs

    try:
        run.start()
        task = polyrun.delay(config, run.pk)

        poly = Polyrun(config)
        run.set_inputs(script='polyrun', task_id=task.id, inputs=config,
                       eta=poly.schedule.eta())
    except ResourceInUse:
        run.schedule_later()


def run_dhcperf(run, config=None):
    """
    Send dhcperf script to celery worker

    :param run: :py:class:`performance.models.TestRun` object
    :param config: a dictionary of script inputs
    """
    if config:
        config['outdir'] = run.output_dir
        config['ip'] = _get_appliance_ip(run, config, 'dnsbox')
    else:
        config = run.script_inputs
    config['run_pk'] = run.pk

    try:
        run.start()
        task = dhcperf.delay(config, run.pk)
        run.set_inputs(script='dhcperf', task_id=task.id, inputs=config)
    except ResourceInUse:
        run.schedule_later()


def run_custom(run, config=None):
    """
    """
    run.set_inputs('custom')
    run.start()


def start_scheduled_run():
    """
    Send scheduled run start message
    """
    for run in TestRun.objects.scheduled():
        try:
            start_script_run(run)
        except:
            logging.exception('Not starting scheduled run {}'.format(run.pk))


def start_script_run(run, config=None):
    """
    """
    runner = {
        'polyrun': run_polyrun,
        'dhcperf': run_dhcperf,
        'custom': run_custom,
    }.get(run.script_name, run_custom)
    runner(run, config)
