from django.core.management.base import BaseCommand

from polygraph.polyrun import Polyrun
from performance.models import TestRun


class Command(BaseCommand):
    """
    """
    args = '<run_pk>'
    help = 'Fetch polyrun reports for a given run'

    def handle(self, *args, **kwargs):
        """
        """
        try:
            run_pk = args[0]
            run = TestRun.objects.get(pk=run_pk)
            run.fetch_report()
            print('Report generated successfully')
        except Exception as e:
            print('Cannot generate report: {}'.format(e))
