from datetime import date, timedelta

from django.core.management.base import BaseCommand
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives

from performance.models import TestRun


class Command(BaseCommand):
    """
    """
    args = '<period>'
    help = 'Send periodic digest'

    def handle(self, *args, **kwargs):
        """
        """
        period = int(args[0])
        sender = 'developers@appliansys.com'
        try:
            recipient = args[1]
        except IndexError:
            recipient = sender

        today = date.today()
        start = today - timedelta(days=period)
        completed = TestRun.objects.completed().order_by('completed_on')
        completed = completed.filter(completed_on__gt=start)
        running = TestRun.objects.running()
        scheduled = TestRun.objects.scheduled()
        html = render_to_string('performance/email_digest.html',
                                {'tests': {'completed': completed,
                                           'running': running,
                                           'scheduled': scheduled},
                                 'from': start, 'to': today})
        msg = EmailMultiAlternatives('Performance digest', '',
                                     sender, [recipient])
        msg.attach_alternative(html, 'text/html')
        msg.send()
