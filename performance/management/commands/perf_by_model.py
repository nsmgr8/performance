from django.core.management.base import BaseCommand

from performance.models import TestRun


class Command(BaseCommand):
    """
    """
    help = 'Find the optimum results for polyrun by model'

    def handle(self, *args, **kwargs):
        """
        """
        runs = TestRun.objects.successful()
        runs = runs.filter(resources__rtype__iexact='cachebox')
        for run in runs:
            run.update_perf_by_model()
