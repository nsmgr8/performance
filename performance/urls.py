from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView

# from rest_framework import routers

from . import views
# from . import viewsets

admin.autodiscover()

# router = routers.DefaultRouter(trailing_slash=False)
# router.register(r'resources', viewsets.ResourceViewSet)
# router.register(r'tests', viewsets.TestRunViewSet)
# router.register(r'tags', viewsets.TagViewSet)

urlpatterns = patterns(
    '',
    url(r'^$', TemplateView.as_view(template_name='index.html'),
        name='ng-app'),
    url(r'^home/$', views.HomeView.as_view(), name='home'),
    url(r'^status/$', views.current_status, name='current_status'),

    url(r'^analyse/$', views.CompareView.as_view(), name='analyse'),
    url(r'^analyse/runs/$', views.CompareTestRunListView.as_view(),
        name='runs_analyse'),

    url(r'^resources/$', views.ResourceList.as_view(), name='resources'),
    url(r'^runs/$', views.TestRunList.as_view(), name='runs'),
    url(r'^tags/$', views.TagList.as_view(), name='tags'),
    url(r'^tags/(?P<slug>.*)/$', views.TaggedTestRun.as_view(), name='tags'),

    url(r'^resources/(?P<pk>\d+)/$', views.ResourceDetail.as_view(),
        name='resource'),
    url(r'^runs/(?P<pk>\d+)/$', views.TestRunDetail.as_view(), name='run'),
    url(r'^runs/clone/$', views.CloneRunView.as_view(), name='clone_run'),

    url(r'^resources/create/$', views.ResourceCreate.as_view(),
        name='resource_create'),
    url(r'^runs/create/$', views.TestRunCreate.as_view(), name='run_create'),

    url(r'^runs/(?P<pk>\d+)/polyrun/summary/(?P<page>.*)$',
        views.PolyrunSummaryView.as_view(), name='run-polyrun-summary'),
    url(r'^runs/(?P<pk>\d+)/state$', views.testrun_state, name='run-state'),
    url(r'^runs/(?P<pk>\d+)/diag_log/$', views.diag_log, name='diag_log'),
    url(r'^runs/(?P<pk>\d+)/rrd_files/$', views.rrd_files, name='rrd_files'),
    url(r'^runs/(?P<pk>\d+)/progress/$', views.progress_ts,
        name='run-progress'),
    url(r'^runs/(?P<pk>\d+)/dhcp-lps/$', views.dhcp_lps, name='dhcp-lps'),
    url(r'^runs/(?P<pk>\d+)/(?P<scr>\w+)/$', views.ScriptsFormView.as_view(),
        name='run_script'),

    url(r'^start-scheduled/$', views.start_scheduled, {'pk': 'all'},
        name='start_scheduled'),
    url(r'^start-scheduled/(?P<pk>\d+)/$', views.start_scheduled,
        name='start_scheduled'),

    url(r'^attachments/', include('attachments.urls')),
    url(r'^social_auth/', include('social.apps.django_app.urls',
                                  namespace='social')),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^cdn-status/', include('cdnrun.urls')),
    url(r'^calamaris/', include('rrtools.calamaris.urls')),
    # url(r'^eslog/', include('rrtools.eslog.urls')),

    url(r'^ng-last-modified/$', views.ng_last_modified),
    url(r'^current-user/$', views.current_user),
    url(r'^performance-summary', views.performance_summary),
    # url(r'^api/', include(router.urls)),
    # url(r'^api-auth/', include('rest_framework.urls',
    #                            namespace='rest_framework')),
)

if settings.DEBUG:
    import os
    from django.conf.urls.static import static

    NG_APP_ROOT = os.path.join(settings.NG_ROOT, 'app')
    NG_FONTS_ROOT = os.path.join(settings.NG_ROOT, 'fonts')
    NG_IMAGES_ROOT = os.path.join(settings.NG_ROOT, 'images')

    urlpatterns = urlpatterns + static(settings.MEDIA_URL,
                                       document_root=settings.MEDIA_ROOT)
    urlpatterns = urlpatterns + static('/app/', document_root=NG_APP_ROOT)
    urlpatterns = urlpatterns + static('/fonts/', document_root=NG_FONTS_ROOT)
    urlpatterns = urlpatterns + static('/images/', document_root=NG_IMAGES_ROOT)
