(function() {
    d3.rrd = d3.rrd || {};

    var width = $('.container').width() - 50, height = 200,
        xScale = d3.time.scale().range([0, width]),
        xAxis = d3.svg.axis().scale(xScale).tickSize(-height).ticks(10)
            .orient('bottom'),
        colors = {line: d3.scale.category10(),
                  area: d3.scale.category20()},
        errors = {dataMissing: false};

    $(window).resize(function() {
        width = $('.container').width() - 50;
        d3.selectAll('.rickshaw_graph svg')
            .attr('width', width);
    });

    var rrd_downloader = function(url) {
        var d = jQuery.Deferred();
        $.ajax({
            url: url,
            dataType: 'text',
            mimeType: 'text/plain; charset=x-user-defined',
            xhr: function() {
                this._nativeXhr = jQuery.ajaxSettings.xhr();
                return this._nativeXhr;
            },
            complete: function(jqXHR, textStatus) {
                this._nativeXhr = null;
                delete this._nativeXhr;
            },
            success: function(data, textStatus, jqXHR) {
                if(typeof(this._nativeXhr.responseBody) !== 'undefined') {
                    data = this._nativeXhr.responseBody;
                }
                try {
                    d.resolve(new RRDFile(new BinaryFile(data)));
                } catch(err) {
                    d.reject(err);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                d.reject(new Error(textStatus + ':' + xhr.status));
            }
        });
        return d;
    };

    var rrd_query = function(options) {
        var self = {},
            rrd_file = null,
            default_options = options || {};

        self.load = function(options) {
            options = options || default_options;
            var d = jQuery.Deferred();

            if (rrd_file && !options.reload) {
                self.data = populate(options);
                d.resolve(self.data);
            } else {
                rrd_downloader(options.url)
                    .then(function(result) {
                        rrd_file = result;
                        self.data = populate(options);
                        self.error = null;
                        d.resolve(self.data);
                    }).fail(function(error) {
                        self.data = null;
                        self.error = error;
                        d.reject(error);
                    });
            }

            return d;
        };

        var populate = function(options) {
            options = $.extend({}, default_options, options);

            var ds = rrd_file.getDS(options.ds || 0),
                cf = options.cf || 'AVERAGE',
                transform = options.transform || function(v) { return v; };

            var lastUpdated = rrd_file.getLastUpdate(),
                startTime = +(moment($('[data-run-start]').data('run-start'))) / 1000,
                endTime = +(moment($('[data-run-end]').data('run-end'))) / 1000;

            if (options.start) startTime = start / 1000;
            if (options.end) endTime = end / 1000;

            if (!endTime) endTime = lastUpdated;
            if (!startTime) startTime = +(moment(endTime*1000).subtract('days', 1)) / 1000;
            if (lastUpdated < startTime) {
                errors.dataMissing = true;
                errors.lastUpdated = lastUpdated;
                return [];
            }

            var rra, step, rraRowCount, lastRowTime, firstRowTime;

            for(var i=0; i<rrd_file.getNrRRAs(); i++) {
                rra = rrd_file.getRRA(i);
                if(rra.getCFName() !== cf) continue;

                step = rra.getStep();
                rraRowCount = rra.getNrRows();
                lastRowTime = lastUpdated - lastUpdated % step;
                firstRowTime = lastRowTime - rraRowCount * step;

                if(firstRowTime <= startTime) break;
            }

            var startRowTime = Math.max(firstRowTime, startTime - startTime % step);
            var endRowTime = Math.min(lastRowTime, endTime - endTime % step);
            startRowTime = Math.min(startRowTime, endRowTime);

            var startRowIndex = rraRowCount - (lastRowTime - startRowTime)  / step;
            var endRowIndex = rraRowCount - (lastRowTime - endRowTime)  / step;

            var data = [], val, timestamp = startRowTime, dsIndex = ds.getIdx();
            for(i=startRowIndex; i<endRowIndex; i++) {
                val = transform(rra.getEl(i, dsIndex));
                data.push({x: timestamp, y: val || 0});
                timestamp += step;
            }

            return data;
        };

        return self;
    };

    var rickshaw_plot = function(elem) {
        elem.each(function(data) {
            var n = data.data[0].values.length,
                renderer = data.gtype;
            for (var i=0; i<data.data.length; i++) {
                if (data.data[i].values.length > 0 && n !== data.data[i].values.length) {
                    // area graph requires exact same number of data points
                    renderer = 'line';
                }
            }

            try {
                var graph = new Rickshaw.Graph({
                    element: this,
                    renderer: renderer,
                    width: width,
                    height: height,
                    stroke: true,
                    series: (function() {
                        var series = [];
                        for (var i=0; i<data.data.length; i++) {
                            if (data.data[i].values.length > 0) {
                                series.push({
                                    name: data.data[i].title,
                                    data: data.data[i].values,
                                    color: colors[data.gtype](i)
                                });
                            }
                        }
                        return series;
                    })()
                });
                graph.render();
                graph.vis.attr('viewBox', '0 0 ' + width + ' ' + height)
                    .attr('preserveAspectRatio', 'none');
                xScale.domain(d3.extent(data.data[0].values, function(d) {
                    return d.x * 1000;
                }));
                graph.vis.append("g")
                    .attr("class", "x_grid_d3")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis);

                var yaxis = new Rickshaw.Graph.Axis.Y({
                    graph: graph,
                    pixelsPerTick: 40,
                    tickFormat: (data.unit === 'Hours' ? formatHours : d3.format('s'))
                });
                yaxis.render();
                var legend = new Rickshaw.Graph.Legend({
                        graph: graph,
                        element: this
                });
                var shelving = new Rickshaw.Graph.Behavior.Series.Toggle({
                        graph: graph,
                        legend: legend
                });
                var hoverDetail = new Rickshaw.Graph.HoverDetail({
                        graph: graph
                });
            } catch (e) {
                d3.select(this).append('div')
                    .attr('class', 'text-center alert-warning')
                    .text('An error occured, cannot render graph');
                console.log(e);
                console.log(data);
            }
        });
    };

    var plotGroup = function(elem, data) {
        var wrap = elem.selectAll('.plot-wrapper')
            .data(function(d) { return d.data; });
        wrap.enter().append('div')
            .attr('class', 'panel panel-default plot-wrapper');
        wrap.append('h3')
            .attr('class', 'text-center')
            .text(function(d) { return d.title; });
        wrap.append('div').call(rickshaw_plot);
        d3.selectAll('.x_grid_d3 text').attr('dy', '-.7em')
            .style('opacity', '0.5');
    };

    d3.rrd.accordion = function(elem) {
        var self = {};

        var setup = function(data) {
            var panel = d3.select(elem)
                .selectAll('.panel').data(data);
            panel.enter().append('div')
                .attr('class', 'panel panel-default');
            panel.append('div')
                .attr('class', 'panel-heading')
              .append('a')
                .attr('class', 'accordion-toggle')
                .attr('data-toggle', 'collapse')
                .attr('href', function(d, i) { return '#collapse-' + i; })
              .append('div')
                .attr('class', 'panel-title')
                .text(function(d) { return d.title; });
            panel.append('div')
                .attr('class', 'panel-collapse collapse in')
                .attr('id', function(d, i) { return 'collapse-' + i; })
              .append('div')
                .attr('class', 'panel-body')
                .call(plotGroup, data);
        };

        setRecipe = function(rrd_list_url) {
            d3.json(rrd_list_url, function(data) {
                var rrd_blob = data.join('\n'), recipes = [],
                    re, match, plots, rrd_files, q,
                    rrd_loaders = [];
                $(rrd_config).each(function(i, group) {
                    plots = [];
                    $(group.data).each(function(j, plot) {
                        rrd_files = [];
                        $(plot.data).each(function(k, rrd_conf) {
                            re = new RegExp('.*/' + rrd_conf.rrdfile, 'gm');
                            match = rrd_blob.match(re);
                            if (match) {
                                rrd_conf.url = match[0];
                                rrd_files.push(rrd_conf);
                                var d = jQuery.Deferred();
                                rrd_loaders.push(d);
                                q = rrd_query(rrd_conf);
                                q.load().then(function(result) {
                                    rrd_conf.values = result;
                                    d.resolve(result);
                                }).fail(function(err) {
                                    console.log(err);
                                });
                            }
                        });
                        if (rrd_files.length > 0) {
                            plots.push({title: plot.title, data: rrd_files,
                                        gtype: plot.gtype || 'line', unit: plot.unit});
                        }
                    });
                    if (plots.length > 0) {
                        recipes.push({title: group.title, data: plots});
                    }
                });
                jQuery.when.apply(null, rrd_loaders).then(function() {
                    setup(recipes);
                    if (errors.dataMissing) {
                        noty({type: 'error',
                              text: 'RRD data was not recorded during the test run. ' +
                                    'Data was last updated at ' +
                                    new Date(errors.lastUpdated * 1000) + '.'});
                    }
                });
            });
        };

        setRecipe($(elem).data('rrd-list-url'));

        return self;
    };

    var formatHours = function(d) {
        var hours = Math.floor(d / 3600),
            mins = Math.floor((d % 3600) / 60);
        return hours + ':' + d3.format('02d')(mins);
    };

    var defs = {
        cpu: function(num_cores) {
            var config = [], name, data, idle_data = [],
                files = 'nice steal user interrupt softirq system wait'.split(' ');
            var invert_func = function(v) { return 100 - v; };
            for(var i=0; i<num_cores; i++) {
                name = 'cpu-' + i;
                idle_data.push({
                    rrdfile: name + '/cpu-idle.rrd',
                    ds: 0,
                    title: name,
                    transform: invert_func
                });
                data = [];
                for(var ii=0; ii<files.length; ii++) {
                    data.push({
                        rrdfile: name + '/cpu-' + files[ii] + '.rrd',
                        ds: 0,
                        title: files[ii],
                    });
                }
                config.push({title: 'CPU ' + i, data: data, gtype: 'area', unit: '%'});
            }
            return [{title: 'Total CPU', data: idle_data, unit: '%'}].concat(config);
        }
    };

    var bytes_to_bits = function(v) { return v * 8; };

    var rrd_config = [
        {
            title: 'System',
            data: defs.cpu(8).concat([
                {
                    title: 'Memory',
                    gtype: 'area',
                    unit: 'B',
                    data: [
                        {
                            rrdfile: 'memory/memory-buffered.rrd',
                            ds: 0,
                            title: 'Buffered'
                        },
                        {
                            rrdfile: 'memory/memory-used.rrd',
                            ds: 0,
                            title: 'Used'
                        },
                        {
                            rrdfile: 'memory/memory-cached.rrd',
                            ds: 0,
                            title: 'Cached'
                        },
                        {
                            rrdfile: 'memory/memory-free.rrd',
                            ds: 0,
                            title: 'Free'
                        }
                    ]
                },
                {
                    title: 'Swap',
                    gtype: 'area',
                    unit: 'B',
                    data: [
                        {
                            rrdfile: 'swap/swap-used.rrd',
                            ds: 0,
                            title: 'Used'
                        },
                        {
                            rrdfile: 'swap/swap-cached.rrd',
                            ds: 0,
                            title: 'Cached'
                        },
                        {
                            rrdfile: 'swap/swap-free.rrd',
                            ds: 0,
                            title: 'Free'
                        }
                    ]
                },
                {
                    title: 'Load',
                    data: [
                        {
                            rrdfile: 'load/load.rrd',
                            ds: 'shortterm',
                            title: 'Short Term'
                        },
                        {
                            rrdfile: 'load/load.rrd',
                            ds: 'midterm',
                            title: 'Medium Term'
                        },
                        {
                            rrdfile: 'load/load.rrd',
                            ds: 'longterm',
                            title: 'Long Term'
                        }
                    ]
                }
            ])
        },
        {
            title: 'Network',
            data: 'eth0 eth1 eth2 eth3 eth4 eth5 eth6 eth7 eth8 eth9 bond0'.split(' ').map(function(nic) {
                return {
                    title: nic + ' Throughput',
                    unit: 'bit/s',
                    data: d3.merge([
                        [['tx', 'Transmit'], ['rx', 'Recieve']].map(function(label) {
                            return {
                                rrdfile: 'interface-' + nic + '/if_octets.rrd',
                        ds: label[0],
                        title: label[1],
                        transform: bytes_to_bits
                            };
                        }),
                        [['tx', 'Transmit (MAX)'], ['rx', 'Recieve (MAX)']].map(function(label) {
                            return {
                                rrdfile: 'interface-' + nic + '/if_octets.rrd',
                        ds: label[0],
                        title: label[1],
                        cf: 'MAX',
                        transform: bytes_to_bits
                            };
                        })
                    ])
                };
            })
        },
        {
            title: 'Disk',
            data: [
                {
                    title: 'Disk IO',
                    unit: 'B/s',
                    data: (function(disk_names) {
                        var config = [];
                        var name;
                        for(var i=0; i<disk_names.length; i++) {
                            name = disk_names[i];
                            config.push({
                                rrdfile: 'disk-' + name  + '/disk_octets.rrd',
                                ds: 'read',
                                title: name + ' (read)'
                            },
                            {
                                rrdfile: 'disk-' + name  + '/disk_octets.rrd',
                                ds: 'write',
                                title: name + ' (write)'
                            });
                        }
                        return config;
                    })('sda sdb sdc sdd sde sdf'.split(' '))
                },
                {
                    title: 'Disk Operations',
                    unit: 'B/s',
                    data: (function(disk_names) {
                        var config = [];
                        var name;
                        for(var i=0; i<disk_names.length; i++) {
                            name = disk_names[i];
                            config.push({
                                rrdfile: 'disk-' + name  + '/disk_ops.rrd',
                                ds: 'read',
                                title: name + ' (read)'
                            });
                            config.push({
                                rrdfile: 'disk-' + name  + '/disk_ops.rrd',
                                ds: 'write',
                                title: name + ' (write)'
                            });
                        }
                        return config;
                    })('sda sdb sdc sdd sde sdf'.split(' '))
                },
                {
                    title: 'SSD Health',
                    unit: 'Errors',
                    data: (function(disk_names) {
                        var config = [];
                        var name;
                        for(var i=0; i<disk_names.length; i++) {
                            name = disk_names[i];
                            config.push({
                                rrdfile: 'exec-diskmon-' + name  + '/gauge-media_wearout.rrd',
                                title: name + ' media wearout'
                            });
                        }
                        return config;
                    })('sda sdb sdc sdd sde sdf'.split(' '))
                }
            ]
        },
        {
            title: 'Squid',
            data: [
                {
                    title: 'Requests Per Second',
                    unit: 'RPS',
                    data: [
                        {
                            rrdfile: 'exec-squid/gauge-requestspersecond.rrd',
                            title: 'Requests Per Second'
                        },
                        {
                            rrdfile: 'exec-squid/gauge-requestspersecond.rrd',
                            title: 'Requests Per Second (Max)',
                            cf: 'MAX'
                        }
                    ]
                },
                {
                    title: 'Hit Ratio',
                    unit: '%',
                    data: [
                        {
                            rrdfile: 'exec-squid/gauge-bytehitratio.rrd',
                            title: 'Byte Hit Ratio (Max)',
                            cf: 'MAX'
                        },
                        {
                            rrdfile: 'exec-squid/gauge-dochitratio.rrd',
                            title: 'Document Hit Ratio (Max)',
                            cf: 'MAX'
                        },
                        {
                            rrdfile: 'exec-squid/gauge-bytehitratio.rrd',
                            title: 'Byte Hit Ratio'
                        },
                        {
                            rrdfile: 'exec-squid/gauge-dochitratio.rrd',
                            title: 'Document Hit Ratio'
                        }
                    ]
                },
                {
                    title: 'Service Times',
                    unit: 's',
                    data: [
                        {
                            rrdfile: 'exec-squid/gauge-dnssvctime.rrd',
                            title: 'DNS'
                        },
                        {
                            rrdfile: 'exec-squid/gauge-httphitsvctime.rrd',
                            title: 'HTTP hits'
                        },
                        {
                            rrdfile: 'exec-squid/gauge-httpmisssvctime.rrd',
                            title: 'HTTP misses'
                        },
                        {
                            rrdfile: 'exec-squid/gauge-httpnmsvctime.rrd',
                            title: 'HTTP hits not-modified'
                        },
                        {
                            rrdfile: 'exec-squid/gauge-httpnhsvctime.rrd',
                            title: 'HTTP refresh hit'
                        },
                        {
                            rrdfile: 'exec-squid/gauge-httpallsvctime.rrd',
                            title: 'HTTP all'
                        },
                        {
                            rrdfile: 'exec-squid/gauge-icpquerysvctime.rrd',
                            title: 'ICP query'
                        }
                    ]
                },
                {
                    title: 'Cache Directories',
                    unit: 'B',
                    gtype: 'area',
                    data: (function(partitions, fstypes) {
                        var config = [];
                        var i, j, k, part, fs, dir;
                        var dirs = ['squid', 'squid_loc'];
                        for(i=0; i<partitions.length; i++) {
                            part = partitions[i];
                            for (j=0; j<fstypes.length; j++) {
                                fs = fstypes[j];
                                for (k=0; k<dirs.length; k++) {
                                    dir = dirs[k];
                                    config.push({
                                        rrdfile: 'exec-squid/gauge-storedirsize-' + fs + '--var-tmp-disks-squid_' + part + '-' + dir + '-' + fs + '.rrd',
                                        title: 'squid_' + part + '/' + dir + '/' + fs
                                    });
                                }
                            }
                        }
                        return config;
                    })('data1 data2 datalo'.split(' '), 'aufs rock'.split(' '))
                },
                {
                    title: 'Cache Usage',
                    unit: 'B',
                    gtype: 'area',
                    data: [
                        {
                           rrdfile: 'exec-squid/gauge-cache-size-small.rrd',
                            title: 'Small object cache'
                        },
                        {
                           rrdfile: 'exec-squid/gauge-cache-size-medium.rrd',
                            title: 'Medium object cache'
                        },
                        {
                           rrdfile: 'exec-squid/gauge-cache-size-large.rrd',
                            title: 'Large object cache'
                        }
                    ]
                },
                {
                    title: 'File Descriptors',
                    unit: '',
                    data: [
                        {
                            rrdfile: 'exec-squid/gauge-filedescriptors.rrd',
                            title: 'File Descriptors'
                        }
                    ]
                },
                {
                    title: 'Memory Use',
                    unit: 'B',
                    gtype: 'area',
                    data: [
                        {
                            rrdfile: 'exec-squid/gauge-memory.rrd',
                            title: 'Memory use'
                        }
                    ]
                },
                {
                    title: 'Uptime',
                    unit: 'Hours',
                    data: [
                        {
                            rrdfile: 'exec-squid/gauge-squiduptime.rrd',
                            title: 'Uptime'
                        }
                    ]
                }
            ]
        }
    ];

})();

$(function() {
    if ($('#rrd-accordion').length == 1) {
        d3.rrd.accordion('#rrd-accordion');
    }
});
