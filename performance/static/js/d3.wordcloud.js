(function() {
    d3.wordcloud = function(tags, elem) {
        var $tagcloud = $(elem),
            width = $tagcloud.width(),
            height = width / 2,
            color = d3.scale.category10(),
            scale = d3.scale.linear().range(['14px', '40px']);

        scale.domain(d3.extent(tags, function(d) { return d.num_items; }));

        var svg = d3.select(elem).append('svg')
            .attr({width: width, height: height, viewBox: '0, 0, ' + width + ', ' + height})
            .on('mousedown', mousedown);

        var force = d3.layout.force()
            .nodes(tags)
            .charge(-200)
            .size([width, height])
            .on("tick", tick)
            .start();

        var node = svg.selectAll(".node")
            .data(tags)
            .enter().append("g")
            .classed('node', true)
            .attr('transform', function(d) {
                return 'translate(' + d.x + ',' + d.y + ')';
            })
            .call(force.drag)
            .on('click', function(d) { location.href = d.url; })
            .on("mousedown", function() { d3.event.stopPropagation(); })
            .on('mouseover', function() {
                var g = d3.select(this);
                g.select('.tag')
                    .transition()
                    .style('font-size', '50px');
                g.select('.num-items')
                    .transition()
                    .style('font-size', '40px');
            })
            .on('mouseout', function(d) {
                var g = d3.select(this);
                g.select('.tag')
                    .transition()
                    .style('font-size', scale(d.num_items));
                g.select('.num-items')
                    .transition()
                    .style('font-size', '14px');
            });

        node.append('text')
            .classed('tag', true)
            .text(function(d) { return d.name; })
            .style({
                fill: function(d, i) { return color(i); },
                'font-size': function(d) { return scale(d.num_items); }
            })
            .attr('text-anchor', 'middle')
            .append('tspan')
            .classed('num-items', true)
            .attr({dy: '.5em', 'text-anchor': 'end'})
            .text(function(d) { return d.num_items; });

        function tick() {
            node.attr('transform', function(d) {
                return 'translate(' + d.x + ',' + d.y + ')';
            });
        }

        function mousedown() {
            tags.forEach(function(o, i) {
                o.x += (Math.random() - 0.5) * 40;
                o.y += (Math.random() - 0.5) * 40;
            });
            force.resume();
        }
    };
})();
