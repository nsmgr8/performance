$(function() {
    var show_hide_bypass = function() {
        var $cache_init = $('#div_id_cache_init'),
            $cachebox = $('#div_id_cachebox'),
            $cache_engine = $('#div_id_cache_engine'),
            $test_cdn = $('#div_id_test_cdn'),
            $proxy_type = $('#id_proxy_type');
        if ($proxy_type.val() === 'bypass') {
            $cache_init.hide('fast');
            $cachebox.hide('fast');
            $cache_engine.hide('fast');
            $test_cdn.hide('fast');
        } else {
            $cache_init.show('fast');
            show_hide_cachebox();
        }
    };

    var show_hide_cachebox = function() {
        var $cachebox = $('#div_id_cachebox'),
            $cache_engine = $('#div_id_cache_engine'),
            $test_cdn = $('#div_id_test_cdn'),
            $cache_init = $('#id_cache_init');
        if ($cache_init.val() === 'non-cachebox') {
            $cachebox.hide('fast');
            $cache_engine.hide('fast');
            $test_cdn.hide('fast');
        } else {
            $cachebox.show('fast');
            $cache_engine.show('fast');
            $test_cdn.show('fast');
        }
    };

    show_hide_cachebox();
    show_hide_bypass();
    $('#id_proxy_type').on('change', show_hide_bypass);
    $('#id_cache_init').on('change', show_hide_cachebox);
});
