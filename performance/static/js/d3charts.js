(function() {
d3.pchart = d3.pchart || {};

d3.pchart.meter = function(elem, start, end, size) {
    if (!start || !end) return null;

    var self = {};

    var twoPi = Math.PI / 2,
        x = d3.time.scale().domain([moment(start), moment(end)]).range([-twoPi, twoPi]),
        arc = d3.svg.arc()
            .startAngle(-twoPi)
            .innerRadius(size/3)
            .outerRadius(size/2);

    var svg = d3.select(elem).append('svg')
        .attr('width', size)
        .attr('height', size/2)
      .append("g")
        .attr("transform", "translate(" + size / 2 + "," + size / 2 + ")");

    var meter = svg.append("g")
        .attr("class", "progress-meter");

    meter.append("path")
        .attr("class", "background")
        .attr("d", arc.endAngle(twoPi));

    self.foreground = meter.append("path")
        .attr("class", "foreground")
        .style('fill', 'green')
        .attr("d", arc.endAngle(x(moment())));

    self.text = meter.append("text")
        .attr("text-anchor", "middle")
        .attr('dy', '-.6em')
        .text(moment(end).fromNow());

    self.state = meter.append('text')
        .attr('text-anchor', 'middle')
        .attr('dy', '-1.6em')
        .text('ETA');

    self.update = function() {
        self.foreground.attr("d", arc.endAngle(x(moment())))
            .style('fill', function() {
                return (x(moment()) > twoPi) ? 'orange' : 'green';
            });
        self.text.text(moment(end).fromNow());
        if (x(moment()) > twoPi) {
            self.state.text('Due');
        }
    };

    return self;
};
})();
