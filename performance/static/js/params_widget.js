(function() {
    var row_template = '' +
        '<div class="form-group">' +
            '<div class="col-xs-4">' +
                '<input class="form-control text-right" type="text" ' +
                    'name="<name>_new_row_label" placeholder="Param name">' +
            '</div>' +
            '<div class="col-xs-6">' +
                '<input class="form-control" type="text" ' +
                    'name="<name>_new_row_value" placeholder="Param value">' +
            '</div>' +
        '</div>';

    $(function() {
        $('.params-add-row').on('click', function() {
            var $wrapper = $(this).siblings('.params-add-row-wrap');
            var name = $wrapper.data('name');
            $wrapper.append(row_template.replace(/<name>/g, name));
            $('[name=' + name + '_new_row_label]').last().focus();
        });
    });
})();
