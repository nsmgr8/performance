$.noty.defaults = {
    layout: 'bottomLeft',
    theme: 'defaultTheme',
    type: 'alert',
    text: '', // can be html or string
    dismissQueue: true, // If you want to use queue feature set this true
    template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
    animation: {
        open: {height: 'toggle'},
        close: {height: 'toggle'},
        easing: 'swing',
        speed: 500 // opening & closing animation speed
    },
    timeout: 5000, // delay for closing event. Set false for sticky notifications
    force: false, // adds notification to the beginning of queue when set to true
    modal: false,
    maxVisible: 3, // you can set max visible notification for dismissQueue true option,
    killer: false, // for close all notifications before show
    closeWith: ['click'], // ['click', 'button', 'hover']
    callback: {
        onShow: function() {},
        afterShow: function() {},
        onClose: function() {},
        afterClose: function() {}
    },
    buttons: false // an array of buttons
};

function get_socketio() {
    var socketio_location = '//' + window.location.hostname + ':8001/performance';
    return io.connect(socketio_location);
}

var socket = get_socketio();

jQuery.fn.removeAttributes = function() {
    return this.each(function() {
        var attributes = $.map(this.attributes, function(item) {
            return item.name;
        });
        var $elem = $(this);
        $.each(attributes, function(i, item) {
            $elem.removeAttr(item);
        });
    });
};

var fineUploader = function(elem, updateUrl) {
    $(elem).fineUploader({
        request: {
            inputName: 'attachment_file',
            endpoint: updateUrl
        },
        text: {
            uploadButton: '<div><i class="icon-upload"></i> Attach a file</div>'
        },
        template: '<div class="qq-uploader">' +
                    '<pre class="qq-upload-drop-area"><span>{dragZoneText}</span></pre>' +
                    '<div class="qq-upload-button btn btn-success">{uploadButtonText}</div>' +
                    '<span class="qq-drop-processing"><span>{dropProcessingText}</span><span class="qq-drop-processing-spinner"></span></span>' +
                    '<ul class="qq-upload-list"></ul>' +
                    '</div>',
        classes: {
            success: 'alert alert-success',
            fail: 'alert alert-error'
        }
    });
};

$(function() {
    socket.on('status', function(stat) {
        if (!stat.title && !stat.message)
            return;
        noty({type: stat.level, text: '<h4>' + stat.title + '</h4>' + stat.message});
        var log = {
            time: moment(),
            level: stat.level === 'error' ? 'danger' : stat.level,
            title: stat.title,
            message: stat.message
        };
        if (['error', 'warning'].indexOf(stat.level) > -1) {
            perf_logs.push(log);
            if (perf_logs.length > 30) {
                perf_logs.shift();
            }
            localStorage.setItem('perf_logs', JSON.stringify(perf_logs));
        } else {
            info_logs.push(log);
            if (info_logs.length > 30) {
                info_logs.shift();
            }
            localStorage.setItem('info_logs', JSON.stringify(info_logs));
        }
        log_lines();
    });

    var log_lines = function() {
        var table = d3.select('#log-lines'),
            logs = perf_logs.concat(info_logs).sort(function(a, b) {
                return moment(b.time) - moment(a.time);
            });
        table.selectAll('div').remove();
        var rows = table.selectAll('div').data(logs)
            .enter().append('div');
        rows.append('dt').text(function(d) {
            return moment(d.time).format('MMM D YY, hh:mm:ss');
        });
        rows.append('dd')
            .attr('class', function(d) {
                return 'text-' + d.level;
            })
            .html(function(d) {
                return '<strong>' + d.title + ':</strong> '+ d.message;
            });
        $('#log-count').text(logs.length);
    };
    var perf_logs = JSON.parse(localStorage.getItem('perf_logs')) || [],
        info_logs = JSON.parse(localStorage.getItem('info_logs')) || [];
    log_lines();

    socket.on('resource_status', function(stat) {
        var $target = $('#resource-status-' + stat.res_pk);
        $target.text(stat.status);
        if (stat.status.toLowerCase() === 'unreachable') {
            $target.removeClass('text-success');
            $target.addClass('text-danger');
        } else {
            $target.removeClass('text-danger');
            $target.addClass('text-success');
        }
    });

    socket.on('refresh', function(stat) {
        if (stat) {
            if (stat.url && stat.url !== location.pathname) {
                return;
            }
            if (stat.level) {
                noty({type: stat.level, text: '<h4>' + stat.title + '</h4>' + stat.message});
            }
        }
        var reload_timeout = 10000, timeout_sec = reload_timeout / 1000;
        var reload = setTimeout(function() { location.reload(); }, reload_timeout);
        noty({
            type: 'error',
            text: 'The page will refresh in ' + timeout_sec + ' seconds.\n' +
                  'Click me to cancel refreshing the page.',
            buttons: [{
                addClass: 'btn btn-primary',
                text: 'cancel',
                onClick: function($noty) {
                    $noty.close();
                    clearTimeout(reload);
                    noty({type: 'success', text: 'Page refreshing is canceled'});
                }
            }],
            timeout: false
        });
    });
});
