"""
Helpers and utilities
=====================
"""

from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages


class DetailAndEditMixin(object):
    """
    A mixin that provides an edit form in a detail view.
    """
    def get_context_data(self, *args, **kwargs):
        """
        Inserts the edit form into template context
        """
        context = super(DetailAndEditMixin, self).get_context_data(*args,
                                                                   **kwargs)
        obj = kwargs.get('object')
        context['edit_form'] = self.get_form(instance=obj)
        return context

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        """
        Posts edit form and updates the model object.
        Protected by authentication.
        """
        self.object = self.get_object()
        form = self.get_form(instance=self.object, data=request.POST)
        if form.is_valid():
            form.save()
            messages.info(request, 'Changes saved successfully')
            return HttpResponseRedirect(self.object.get_absolute_url())
        else:
            messages.error(request, 'Could not save edit')
            context = self.get_context_data()
            context['edit_form'] = form
            context['show_edit'] = True
            return self.render_to_response(context)


class LoginRequiredDispatch(object):
    """
    A mixin for authentication required views
    """
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredDispatch, self).dispatch(*args, **kwargs)


class CreatorFormMixin(LoginRequiredDispatch):
    """
    A mixin for CreateView that adds the current user as
    the creator of the model object.
    """
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.created_by = self.request.user
        self.object.save()
        form.save_m2m()
        return HttpResponseRedirect(self.get_success_url())
