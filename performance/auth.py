from rest_framework.authentication import SessionAuthentication


class CSRFExemptAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        pass
