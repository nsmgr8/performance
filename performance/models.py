"""
Performance models
==================
"""

from datetime import datetime, timedelta
import os
import json
import logging
from collections import deque


from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.core.files import File
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.core.cache import cache

from taggit.managers import TaggableManager

from attachments.models import Attachment
from jsonfield import JSONField
from performance import socketioclient
from polygraph.polyrun import Polyrun, check_progress as polyrun_progress
from remote.ssh import execute


logger = logging.getLogger('django')


class ResourceConst(object):
    IDLE = 0
    IN_USE = 10
    UNAVAILABLE = -1

    RESOURCE_STATUS = (
        (IDLE, 'Idle'),
        (IN_USE, 'In use'),
        (UNAVAILABLE, 'Unavailable'),
    )


class TestRunConst(object):
    WAITING = 0
    SCHEDULED = 5
    RUNNING = 10
    FAIL = 20
    STOPPED = 25
    SUCCESS = 30

    RUN_STATES = (
        (WAITING, 'Waiting'),
        (SCHEDULED, 'Scheduled'),
        (RUNNING, 'Running'),
        (SUCCESS, 'Successful'),
        (STOPPED, 'Stopped'),
        (FAIL, 'Failed'),
    )


class TestRunError(Exception):
    """
    """


class ResourceInUse(Exception):
    """
    """


def send_notification(status, data=None):
    """
    Send a notification via socket.io
    """
    try:
        with socketioclient.SocketIO('localhost', 8001) as sock:
            perf_sock = sock.define(socketioclient.BaseNamespace,
                                    '/performance')
            perf_sock.emit(status, data)
    except:
        logger.exception('send_notification')


class TimestampedModel(models.Model):
    title = models.CharField(max_length=300)
    created_by = models.ForeignKey(User)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.title


class Resource(TimestampedModel):
    ip_address = models.GenericIPAddressField(blank=True, null=True)
    rtype = models.CharField(blank=True, max_length=300, verbose_name='Type')
    description = models.TextField()
    state = models.IntegerField(choices=ResourceConst.RESOURCE_STATUS,
                                default=ResourceConst.IDLE)

    class Meta:
        ordering = ('ip_address', 'rtype', 'title')

    @models.permalink
    def get_absolute_url(self):
        return ('resource', (self.pk,))

    @property
    def status(self):
        return dict(ResourceConst.RESOURCE_STATUS).get(self.state)

    @property
    def status_icon(self):
        return {
            ResourceConst.IDLE: 'icon-off',
            ResourceConst.IN_USE: 'icon-spinner',
            ResourceConst.UNAVAILABLE: 'icon-ban-circle',
        }.get(self.state, ResourceConst.UNAVAILABLE)

    @property
    def idle(self):
        return self.state == ResourceConst.IDLE

    @property
    def in_use(self):
        return self.state == ResourceConst.IN_USE

    @property
    def unavailable(self):
        return self.state == ResourceConst.UNAVAILABLE

    def set_idle(self):
        if self.testrun_set.running().count() == 0:
            self.state = ResourceConst.IDLE
            self.save()

    def set_available(self):
        if self.testrun_set.running().count() > 0:
            self.state = ResourceConst.IN_USE
            self.save()
        else:
            self.set_idle()

    def set_unavailable(self):
        self.state = ResourceConst.UNAVAILABLE
        self.save()


class RunManager(models.Manager):
    def waiting(self):
        return self.get_queryset().filter(state=TestRunConst.WAITING)

    def scheduled(self):
        return self.get_queryset().filter(state=TestRunConst.SCHEDULED)

    def running(self):
        return self.get_queryset().filter(state=TestRunConst.RUNNING)

    def completed(self):
        return self.get_queryset().filter(state__gt=TestRunConst.RUNNING)

    def successful(self):
        return self.get_queryset().filter(state=TestRunConst.SUCCESS)

    def failed(self):
        return self.get_queryset().filter(state=TestRunConst.FAIL)


class TestRun(TimestampedModel):
    resources = models.ManyToManyField(Resource, blank=True)
    team = models.ManyToManyField(User, blank=True, related_name='team')

    description = models.TextField()

    started_on = models.DateTimeField(null=True, blank=True)
    completed_on = models.DateTimeField(null=True, blank=True)
    eta = models.DateTimeField(null=True, blank=True)
    estimated_days = models.FloatField(default=1.0)

    queue = models.PositiveIntegerField(null=True, blank=True)
    state = models.IntegerField(choices=TestRunConst.RUN_STATES,
                                default=TestRunConst.WAITING)

    inputs = JSONField(blank=True)
    outputs = JSONField(blank=True)

    script = models.CharField(blank=True, max_length=300)
    script_inputs = JSONField(blank=True)
    script_outputs = JSONField(blank=True)

    report = models.TextField(blank=True)

    objects = RunManager()
    tags = TaggableManager(blank=True)

    class Meta:
        ordering = ('-state', '-completed_on', '-started_on', 'queue')

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('run', (self.pk,))

    @property
    def status(self):
        return dict(TestRunConst.RUN_STATES).get(self.state)

    @property
    def waiting(self):
        return self.state == TestRunConst.WAITING

    @property
    def scheduled(self):
        return self.state == TestRunConst.SCHEDULED

    @property
    def ready_to_run(self):
        return self.state in (TestRunConst.WAITING, TestRunConst.SCHEDULED)

    @property
    def running(self):
        return self.state == TestRunConst.RUNNING

    @property
    def finished(self):
        return self.state not in (TestRunConst.WAITING, TestRunConst.RUNNING)

    @property
    def script_name(self):
        """
        Name of the script that is run for this test
        """
        if self.script:
            return self.script.split(':')[0]
        return None

    @property
    def script_id(self):
        """
        Celery task ID for the script
        """
        if self.script:
            return self.script.split(':')[-1]
        return None

    @property
    def cache_engine(self):
        """
        Get cache engine used in this run
        """
        return self.inputs.get('Cache engine')

    def set_inputs(self, script, task_id=None, inputs=None, eta=None):
        """
        Save script inputs

        :param script: script name
        :param task_id: celery task id
        :param inputs: inputs the for script
        :param eta: estimated time for task to be completed
        """
        self.script = '{}:{}'.format(script, task_id or '')
        self.script_inputs = inputs
        self.script_outputs = None
        self.eta = eta or datetime.now() + timedelta(days=self.estimated_days)
        self.save()

    def schedule_later(self):
        """
        Schedule the test run to run later
        """
        self.state = TestRunConst.SCHEDULED
        self.save()
        send_notification('refresh', {'url': self.get_absolute_url(),
                                      'id': self.pk})

    def start(self):
        """
        Start the test run

        :raises: :py:class:`performance.models.TestRunError`
            if its not waiting or required resource is busy
        """
        if not self.ready_to_run:
            raise TestRunError('Not waiting to be run')
        for resource in self.resources.all():
            if not resource.idle:
                raise ResourceInUse('{r.title} is "{r.status}"'
                                    .format(r=resource))
        self.state = TestRunConst.RUNNING
        self.started_on = datetime.now()
        self.save()
        for resource in self.resources.all():
            resource.state = ResourceConst.IN_USE
            resource.save()

        send_notification('refresh', {'url': self.get_absolute_url(),
                                      'id': self.pk})

    def stop(self, state):
        """
        Stop the test

        :raises: :py:class:`performance.models.TestRunError`
            if not running
        """
        if not self.running:
            raise TestRunError('Not running')
        self.state = state
        self.completed_on = datetime.now()
        self.save()
        for resource in self.resources.all():
            resource.set_idle()

        send_notification('refresh', {'url': self.get_absolute_url(),
                                      'id': self.pk})

        self.clear_cached_values('progress')
        self.clear_cached_values('progress-ts')

    def success(self, output=None):
        """
        Stop the test and mark it successful
        """
        self.script_outputs = output
        self.stop(TestRunConst.SUCCESS)
        self.attach_logs()
        self.update_params()

        from .runners import start_scheduled_run
        start_scheduled_run()
        send_notification('success')

    def fail(self):
        """
        Stop the test and mark it failed
        """
        self.stop(TestRunConst.FAIL)
        send_notification('fail')

    def force_stop(self):
        """
        Stop the test but do not mark it as fail
        """
        self.stop(TestRunConst.STOPPED)
        send_notification('fail')

    def reset(self):
        """
        Reset the test
        """
        self.state = TestRunConst.WAITING
        self.started_on = None
        self.completed_on = None
        self.eta = None
        self.script_outputs = ''
        self.save()

        for resource in self.resources.all():
            resource.set_idle()

        send_notification('refresh', {'url': self.get_absolute_url(),
                                      'id': self.pk})

        self.clear_cached_values('progress')
        self.clear_cached_values('progress-ts')

    def fetch_report(self):
        """
        Fetch reports
        """
        if self.script_name == 'polyrun':
            poly = Polyrun(self.script_inputs)
            self.script_outputs = poly.generate_reports()
            self.save()
        self.update_params()

    def make_cache_key(self, ns, key):
        """
        Make a namespaced cache key
        """
        return '{}:{}:{}:{}'.format(ns, self.pk, self.script_name, key)

    def get_cache_keys(self, ns, key='*'):
        """
        Get cache keys of a given namespace
        """
        return cache.keys(self.make_cache_key(ns, key))

    def get_cached_value(self, ns, key):
        """
        Get cached value for given namespace and key
        """
        return cache.get(self.make_cache_key(ns, key))

    def get_cached_values(self, ns):
        """
        Get cached values for given namespace
        """
        keys = self.get_cache_keys(ns)
        return [cache.get(key) for key in keys]

    def clear_cached_values(self, ns):
        """
        Delete cached values for given namespace
        """
        keys = self.get_cache_keys(ns)
        for key in keys:
            cache.delete(key)

    def cache_value(self, ns, key, value):
        """
        Cache a value in the given namespace
        """
        cache.set(self.make_cache_key(ns, key), value)

    def clone(self, creator=None):
        """
        Clone the test run

        :returns: a new TestRun cloned
        """
        clone = TestRun.objects.create(title='{0} (cloned)'.format(self.title),
                                       description=self.description,
                                       script=self.script,
                                       script_inputs=self.script_inputs,
                                       created_by=creator or self.created_by)
        for resource in self.resources.all():
            clone.resources.add(resource)
        for crew in self.team.all():
            clone.team.add(crew)

        return clone

    def update_params(self):
        """
        Update control and output params
        """
        try:
            getattr(self, '_update_{}_params'.format(self.script_name))()
        except AttributeError:
            logger.exception('Error in update params')

    def _update_dhcperf_params(self):
        """
        Update control/output params for a polyrun test
        """
        self._update_dhperf_outputs()
        self._update_appliance_params()
        self.save()
        self.update_tags()

    def _update_dhperf_outputs(self):
        """
        Update dhperf outputs
        """
        def max_key(type_, value):
            return lambda x: x[type_][value]

        discover_lps = []
        discover_hwm = []
        renew_lps = []
        renew_hwm = []
        for p in self.script_outputs:
            for t in p['tests']:
                discover_lps.append(max(t['runs'],
                                        key=max_key('discover',
                                                    'qps'))['discover']['qps'])
                discover_hwm.append(max(t['runs'],
                                        key=max_key('discover',
                                                    'hwm'))['discover']['hwm'])
                renew_lps.append(max(t['runs'],
                                     key=max_key('renew',
                                                 'qps'))['renew']['qps'])
                renew_hwm.append(max(t['runs'],
                                     key=max_key('renew',
                                                 'hwm'))['renew']['hwm'])
        self.outputs.update({
            'DISCOVER LPS': max(discover_lps),
            'DISCOVER HWM': max(discover_hwm),
            'RENEW LPS': max(renew_lps),
            'RENEW HWM': max(renew_hwm),
        })

    def _update_polyrun_params(self):
        """
        Update control/output params for a polyrun test
        """
        self._update_polyrun_inputs()
        self._update_polyrun_outputs()
        self._update_appliance_params()
        self.save()
        self.update_perf_by_model()
        self.update_tags()

    def _update_polyrun_inputs(self):
        """
        Update input params
        """
        from .forms import POLYRUN_RECIPES

        self.inputs.update({
            'Cache size': '{} GB'.format(self.script_inputs['cache_size']),
            'Proxy type': self.script_inputs['proxy_type'],
            'Fill phase': self.script_inputs['fill_rate'],
            'CDN test': self.script_inputs.get('test_cdn'),
            'Recipe': dict(POLYRUN_RECIPES).get(self.script_inputs['recipe'],
                                                'Recipe 2011'),
            'Schedule': '{0}: {1} RPS'.format(
                self.script_inputs['schedule_type'],
                self.script_inputs['schedule_value']),
        })

        squid_conf = self.squid_conf()
        if squid_conf:
            engine = 'Squid 3' if 'squid3' in squid_conf else 'Squid 2'
            self.inputs['Cache engine'] = engine
            self.inputs['No logging'] = True
            storage = set([])
            for line in squid_conf.splitlines():
                if line.startswith('cache_dir '):
                    storage.add(line.split()[1])
                if line.startswith('maximum_object_size '):
                    self.inputs['Max object size'] = line.split(' ', 1)[1]
                if line.startswith('access_log '):
                    self.inputs['No logging'] = False
                if line.strip().lower() == 'cache deny all':
                    self.inputs['No cache'] = True
            self.inputs['Storage'] = '+'.join(storage)

    def _update_polyrun_outputs(self):
        """
        Update output params
        """
        if self.script_inputs['schedule_type'] in ('ramp', 'constant'):
            max_rps = self.script_outputs[-1]
        elif self.script_inputs['schedule_type'] == 'steps':
            rows = [r for r in self.script_outputs
                    if r['phase'][0] == 's'][::-1]
            max_rps = rows[0]
            for row in rows[1:]:
                if max_rps['errors'] > row['errors']:
                    max_rps = row
        else:
            return

        self.outputs.update({
            'RPS': '{rps[measured]}/{rps[offered]}'.format(**max_rps),
            'Mbps': '{mbps[measured]}/{mbps[offered]}'.format(**max_rps),
            'BHR': '{bhr[measured]}/{bhr[offered]}'.format(**max_rps),
            'DHR': '{dhr[measured]}/{dhr[offered]}'.format(**max_rps),
            '% Errors': max_rps['errors'],
        })

    def _update_appliance_params(self):
        """
        Update control params from appliance info
        """
        diag_path = os.path.join(self.output_dir, 'diag')
        filepaths = {}
        for root, dirs, files in os.walk(diag_path):
            for f in files:
                fname = f.rsplit('/', 1)[-1]
                if fname in ('cpuinfo', 'meminfo', 'firmware', 'model'):
                    filepaths[fname] = os.path.join(root, f)
        inp_files = (
            ('cpuinfo', (('model name', 'CPU'),),),
            ('meminfo', (('MemTotal:', 'RAM'),),),
            ('firmware',
             (('firmware_marketing_version:', 'Version'),
              ('firmware_version:', 'Build'),
              ('firmware_edition:', 'Product'),),),
        )

        for file, params in inp_files:
            try:
                with open(filepaths[file], 'r') as f:
                    for line in f:
                        for key, val in params:
                            if line.startswith(key):
                                self.inputs[val] = line.split(':')[1].strip()
            except (IOError, KeyError):
                logger.exception('Could not read appliance param')

        if 'RAM' in self.inputs:
            ram_gb = round(float(self.inputs['RAM'].split()[0]) / 1e6)
            self.inputs['RAM'] = '{} GB'.format(ram_gb)

        try:
            with open(filepaths['model'], 'r') as f:
                model = [l.split(':')[1].strip() for l in f if ':' in l]
                if model[0] != model[1]:
                    model = '{0}/{1}'.format(*model)
                else:
                    model = model[0]
                self.inputs['Model'] = model
        except (IOError, KeyError):
            logger.exception('I/O error')

    def update_perf_by_model(self):
        """
        Update Key performance by model
        """
        model = self.inputs.get('Model')
        if not model:
            return

        if (float(self.outputs.get('% Errors', 0.0)) != 0 or
                self.inputs.get('Proxy type') == 'bypass'):
            return

        perf_keys = ('RPS', 'BHR', 'DHR', 'Mbps')
        values = {'run_pk': self.pk}
        try:
            for key in perf_keys:
                values[key] = float(self.outputs.get(key).split('/')[0])
        except:
            return

        perf_by_model = cache.get('perf_by_model', '{}')
        perf_by_model = json.loads(perf_by_model)

        data = perf_by_model.get(model)
        if not data:
            perf_by_model[model] = {}
            for key in perf_keys:
                perf_by_model[model][key] = values
        else:
            for key in perf_keys:
                if data[key][key] < values[key]:
                    perf_by_model[model][key] = values

        cache.set('perf_by_model', json.dumps(perf_by_model), timeout=None)

    def update_tags(self):
        """
        Update tags from inputs, outputs
        """
        tags = []
        for key in ('Product', 'Model', 'Version', 'Cache engine'):
            value = self.inputs.get(key)
            if value:
                tags.append(value)

        version = self.inputs.get('Version')
        if version:
            tags.append(version.rsplit('.', 1)[0])

        recipe = self.inputs.get('Recipe')
        if recipe:
            tags.append(recipe)

        if self.script_name:
            tags.append(self.script_name)

        if tags:
            self.tags.add(*tags)

    @property
    def progress(self):
        """
        Test run progress value

        :returns: 0 if waiting, 100 if finished,
            (100 * (now - started) / eta) otherwise
        """
        if self.waiting:
            return 0
        if self.running:
            estimate = (self.eta - self.started_on).total_seconds()
            tdelta = (datetime.now() - self.started_on).total_seconds()
            return 100.0 * tdelta / estimate
        return 100

    def check_progress(self, io=None):
        """
        """
        try:
            getattr(self, '_{}_progress'.format(self.script_name))(io)
        except:
            logger.exception('Error in progress check')

    def _polyrun_progress(self, io):
        """
        """
        polyrun = Polyrun(self.script_inputs)
        progress = execute(polyrun_progress,
                           name=self.script_inputs['name'],
                           hosts=polyrun.polygraphs)
        polygraphs = self.resources.filter(rtype__iexact='polygraph')
        polygraphs = {p.ip_address: p.title for p in polygraphs}

        for k, v in progress:
            if not v:
                continue
            k = k.split('@')[-1]
            pg = polygraphs.get(k, k)
            self.cache_value('progress', k, {'title': pg, 'message': v})
            if io:
                io.emit('status', {'level': 'info',
                                   'run_pk': self.pk,
                                   'script': 'polyrun',
                                   'title': pg,
                                   'message': v})

            if 'shutdown' not in v:
                values = v.split()
                row = {
                    'elapsed': float(values[0].strip('|')),
                    'phase': values[1],
                    'total_replies': int(values[2]),
                    'reply_rate': float(values[3]),
                    'resp_time': float(values[4]),
                    'hit_ratio': float(values[5]),
                    'txn_error': int(values[6]),
                    'sockets': int(values[7]),
                }
                ts = self.get_cached_value('progress-ts', k)
                if not ts:
                    ts = {'polygraph': pg, 'data': deque([row], 600)}
                elif ts['data'][-1]['elapsed'] < row['elapsed']:
                    ts['data'].append(row)
                elif ts['data'][-1]['elapsed'] > row['elapsed']:
                    ts['data'] = deque([row], 600)
                self.cache_value('progress-ts', k, ts)

    def _dhcperf_progress(self, io):
        """
        """
        pool = {}
        try:
            pool = self.script_outputs[-1]['pool']
            pool['clients'] = self.script_outputs[-1]['tests'][-1]['clients']
            result = self.script_outputs[-1]['tests'][-1]['runs'][-1]
            pool['result'] = {
                'discover': {
                    'hwm': result['discover']['hwm'],
                    'qps': result['discover']['qps'],
                },
                'renew': {
                    'hwm': result['renew']['hwm'],
                    'qps': result['renew']['qps'],
                },
            }
        except (IndexError, KeyError):
            pass

        if io:
            io.emit('status', {'level': 'info',
                               'run_pk': self.pk,
                               'script': 'dhcperf',
                               'data': pool})

    @property
    def output_dir(self):
        """
        Location of the output files
        """
        return os.path.join(settings.MEDIA_ROOT, 'output', str(self.pk))

    def attach_logs(self):
        """
        Attach log files
        """
        for f in ('rrd.tgz', 'diag.zip'):
            fname = os.path.join(self.output_dir, f)
            if not os.path.exists(fname):
                continue
            attachment = Attachment()
            attachment.creator = self.created_by
            attachment.content_type = ContentType.objects.get_for_model(self)
            attachment.object_id = self.pk
            attachment.attachment_file = File(open(fname, 'rb'))
            attachment.save()
            os.remove(fname)

    @property
    def polyrun_summary_url(self):
        """
        """
        return settings.MEDIA_URL + '/'.join(('output', str(self.pk),
                                              'summary'))

    def polyrun_htmls(self):
        """
        :returns: a list of polygraph generated html report page URLs
        """
        return ((reverse('run-polyrun-summary', args=(self.pk, fname)), title)
                for fname, title in (('index.html', 'Polyrun index'),
                                     ('everything.html', 'Full results'),
                                     ('one-page.html', 'One page summary'),
                                     ('errors.html', 'Errors'),
                                     ('workload.html', 'Workload file')))

    def has_rrd(self):
        """
        :returns: Boolean, True if it has rrd files in output folder
        """
        rrd_path = os.path.join(self.output_dir, 'localhost')
        return self.finished and os.path.isdir(rrd_path)

    def rrd_files(self):
        """
        :returns: a list of RRD file URLs
        """
        rrd_path = os.path.join(self.output_dir, 'localhost')
        rrds = []
        if os.path.exists(rrd_path) and os.path.isdir(rrd_path):
            for root, dirs, files in os.walk(rrd_path):
                if files:
                    prefix = root.split(settings.MEDIA_URL)[-1]
                    rrds.extend([os.path.join(settings.MEDIA_URL, prefix, f)
                                 for f in files])
        return rrds

    def has_diag_files(self):
        """
        """
        log_path = os.path.join(self.output_dir, 'diag')
        return self.finished and os.path.isdir(log_path)

    def diag_files(self, as_objects=False):
        """
        :returns: a list of diagnostic (log) files
        """
        file_list = []

        if not self.finished:
            return file_list

        log_path = os.path.join(self.output_dir, 'diag')
        for root, dirs, files in os.walk(log_path):
            key = root.replace(log_path, '').strip('/')
            files = sorted(files)
            entries = []
            for f in files:
                if '.tgz' in f:
                    continue

                size = os.path.getsize(os.path.join(root, f))
                if size == 0:
                    continue

                value = os.path.join(key, f)
                label = '{} ({})'.format(f, size)
                if as_objects:
                    file_list.append({'group': key or '/',
                                      'path': value,
                                      'name': f,
                                      'size': size})
                else:
                    entries.append((value, label))

            if entries:
                file_list.append((key or '/', entries))

        return file_list

    def has_squid_conf(self):
        """
        """
        return (self.script_name == 'polyrun' and
                os.path.isfile(os.path.join(self.output_dir,
                                            'squid_conf.txt')))

    def squid_conf(self):
        """
        :returns: content of attached squid.conf
        """
        conf = ''

        if not self.finished:
            return conf

        squid_path = os.path.join(self.output_dir, 'squid_conf.txt')
        if os.path.exists(squid_path):
            with open(squid_path, 'r') as f:
                conf = f.read()
        return conf
