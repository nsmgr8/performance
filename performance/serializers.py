"""
Model serialisers for API
=========================
"""
from django.contrib.auth.models import User

from rest_framework import serializers
from taggit.models import Tag

from .models import TestRun, Resource


class TestRunSerializer(serializers.ModelSerializer):
    """
    """
    class Meta:
        model = TestRun
        fields = ('id', 'title', 'started_on', 'completed_on', 'inputs',
                  'outputs')


class UserSerializer(serializers.ModelSerializer):
    """
    """
    fullname = serializers.Field(source='get_full_name')

    class Meta:
        model = User
        fields = ('username', 'fullname')


class TagListSerializer(serializers.ModelSerializer):
    """
    """
    num_items = serializers.Field(source='taggit_taggeditem_items.all.count')

    class Meta:
        model = Tag
        fields = ('name', 'slug', 'num_items')


class ResourceListSerializer(serializers.HyperlinkedModelSerializer):
    """
    """
    created_by = UserSerializer()
    status = serializers.Field(source='status')

    class Meta:
        model = Resource
        fields = ('url', 'id', 'title', 'created_by', 'created_on',
                  'ip_address', 'rtype', 'status')


class ResourceDetailSerializer(serializers.ModelSerializer):
    """
    """
    created_by = UserSerializer()
    status = serializers.Field(source='status')

    class Meta:
        model = Resource


class ResourceCreateSerializer(serializers.ModelSerializer):
    """
    """
    class Meta:
        model = Resource
        fields = ('id', 'title', 'description', 'ip_address', 'rtype')


class TestRunListSerializer(serializers.HyperlinkedModelSerializer):
    """
    """
    created_by = UserSerializer()
    status = serializers.Field(source='status')
    script_name = serializers.Field(source='script_name')

    class Meta:
        model = TestRun
        fields = ('url', 'id', 'title', 'created_by', 'created_on',
                  'started_on', 'completed_on', 'eta', 'status', 'script_name')


class TestRunDetailSerializer(serializers.ModelSerializer):
    """
    """
    created_by = UserSerializer()
    resources = ResourceListSerializer(many=True)
    team = UserSerializer(many=True)
    status = serializers.Field(source='status')
    script_name = serializers.Field(source='script_name')
    has_squid_conf = serializers.Field(source='has_squid_conf')
    has_rrd = serializers.Field(source='has_rrd')
    has_logs = serializers.Field(source='has_diag_files')
    tags = TagListSerializer(many=True)

    class Meta:
        model = TestRun


class TestRunCreateSerializer(serializers.ModelSerializer):
    """
    """
    class Meta:
        model = TestRun
        fields = ('id', 'title', 'description', 'script')


class TestRunCompareSerializer(serializers.ModelSerializer):
    """
    """
    class Meta:
        model = TestRun
        fields = ('id', 'title', 'started_on', 'completed_on', 'inputs',
                  'outputs')
