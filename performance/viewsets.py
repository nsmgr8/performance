import os
import json
from operator import or_ as OR
import subprocess

from django.contrib.auth.models import User
from django.db.models import Q
from django.http import StreamingHttpResponse

from rest_framework import viewsets
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from taggit.models import Tag
# from django_pygments.utils import pygmentify_html

from . import serializers
from .models import TestRun, Resource
from .runners import start_script_run
from .forms import PolyrunForm, DhcperfForm
from performance.tasks import stop_task


class ResourceViewSet(viewsets.ModelViewSet):
    model = Resource
    paginate_by = None

    def get_serializer_class(self):
        return {
            'retrieve': serializers.ResourceDetailSerializer,
            'create': serializers.ResourceCreateSerializer,
        }.get(self.action, serializers.ResourceListSerializer)

    def pre_save(self, obj):
        obj.created_by = self.request.user

    def get_queryset(self):
        qs = Resource.objects.all()
        rtype = self.request.QUERY_PARAMS.get('rtype')
        if rtype:
            qs = qs.filter(rtype__iexact=rtype.lower())
        return qs


class TestRunViewSet(viewsets.ModelViewSet):
    queryset = TestRun.objects.all()
    paginate_by = None

    def get_serializer_class(self):
        return {
            'retrieve': serializers.TestRunDetailSerializer,
            'create': serializers.TestRunCreateSerializer,
            'list': serializers.TestRunListSerializer,
            'compare': serializers.TestRunCompareSerializer,
        }.get(self.action, serializers.TestRunDetailSerializer)

    def pre_save(self, obj):
        obj.created_by = self.request.user

    def filter_queryset(self, qs):
        resource = self.request.QUERY_PARAMS.get('resource')
        if resource:
            resource = Resource.objects.get(pk=int(resource))
            return resource.testrun_set.completed()[:50]

        state = self.request.QUERY_PARAMS.get('state')
        if state:
            qs = qs.filter(state=state)

        run_pks = self.request.QUERY_PARAMS.get('runs')
        if run_pks:
            run_pks = [int(x) for x in run_pks.split(',')]
            qs = qs.filter(pk__in=run_pks)

        tags = self.request.QUERY_PARAMS.get('tags')
        if tags:
            tags = [Q(tags__slug=tag) for tag in tags.split(',')]
            qs = qs.filter(reduce(OR, tags))

        return qs.distinct()

    @detail_route(methods=['put'])
    def reset(self, request, pk):
        action = request.QUERY_PARAMS.get('action')
        if action not in ('stop', 'reset'):
            return Response({'error': 'Unknown action'}, 400)
        run = self.get_object()
        if run.running:
            stop_task.delay(run, action != 'reset')
        if action == 'reset':
            run.reset()
        elif action == 'stop':
            run.force_stop()
        serializer = self.get_serializer(run)
        return Response(serializer.data)

    @detail_route(methods=['post'])
    def clone(self, request, pk):
        run = self.get_object().clone()
        serializer = self.get_serializer(run)
        return Response(serializer.data)

    @detail_route(methods=['post'])
    def start(self, request, pk):
        run = self.get_object()

        form_class = {
            'polyrun': PolyrunForm,
            'dhcperf': DhcperfForm,
        }.get(run.script_name)

        if form_class:
            form = form_class(run=run, data=request.DATA)
            if form.is_valid():
                start_script_run(run, form.cleaned_data)
            else:
                return Response({'errors': json.dumps(form.errors)}, 400)
        else:
            start_script_run(run)

        serializer = self.get_serializer(self.get_object())
        return Response(serializer.data)

    @list_route(methods=['get'])
    def compare(self, request):
        instance = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(instance)
        if page is not None:
            serializer = self.get_pagination_serializer(page)
        else:
            serializer = self.get_serializer(instance, many=True)
        return Response(serializer.data)

    @detail_route(methods=['get'])
    def squidconf(self, request, pk):
        run = self.get_object()
        as_html = pygmentify_html('<pre lang="squidconf">{}</pre>'
                                  .format(run.squid_conf()))
        return Response(as_html)

    @detail_route(methods=['get'])
    def log_files(self, request, pk):
        run = self.get_object()
        return Response(run.diag_files(True))

    @detail_route(methods=['get'])
    def log_file(self, request, pk):
        run = self.get_object()
        fname = request.QUERY_PARAMS.get('fname')
        if not fname:
            return Response({'error': 'No file requested'}, status=400)

        path = os.path.join(run.output_dir, 'diag', fname)
        def reader():
            with open(path) as f:
                for line in f.readlines():
                    yield line

        return StreamingHttpResponse(reader())


    @detail_route(methods=['get'])
    def search_log(self, request, pk):
        run = self.get_object()
        query = request.QUERY_PARAMS.get('query')
        query_context = request.QUERY_PARAMS.get('context', 0)
        if not query or len(query) < 3:
            return Response({'error': 'No search query requested'}, status=400)

        path = os.path.join(run.output_dir, 'diag')
        p = subprocess.Popen(['ag', '-C', str(query_context), query, path],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        def reader():
            for line in p.stdout.readlines():
                yield line.replace(path, '')

        return StreamingHttpResponse(reader())


class TagViewSet(viewsets.ReadOnlyModelViewSet):
    model = Tag
    serializer_class = serializers.TagListSerializer

User.__unicode__ = lambda self: self.get_full_name() or self.username
User.__str__ = User.__unicode__
