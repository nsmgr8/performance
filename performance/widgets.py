"""
Custom widgets
==============
"""

from itertools import count
import json

from django.forms import widgets
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe


class ParamsWidget(widgets.Widget):
    """
    Parameters widget

    Renders a list of text inputs from a json object.
    Keys are rendered as labels while values are text inputs.
    """
    class Media:
        js = ('js/params_widget.js',)

    def decompress(self, value):
        if isinstance(value, str):
            value = json.loads(value)
        if value:
            value = {k: v for k, v in value.items() if str(k).strip()}
        return value or {}

    def render(self, name, value, attrs=None):
        value = self.decompress(value)
        return mark_safe(render_to_string('performance/params_widget.html',
                                          {'fields': value,
                                           'name': name}))

    def value_from_datadict(self, data, files, name):
        params = {}
        new_label = '{}_new_row_label'.format(name)
        new_value = '{}_new_row_value'.format(name)
        for label, value in zip(data.getlist(new_label),
                                data.getlist(new_value)):
            if label:
                params[label] = value

        for i in count():
            label = data.get('label_{0}_{1}'.format(name, i))
            value = data.get('{0}_{1}'.format(name, i))
            if value is None:
                break
            params[label] = value

        return json.dumps(params)
