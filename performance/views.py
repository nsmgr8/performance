"""
Performance views
=================
"""
import logging
import os
import json
from datetime import datetime, timedelta
from collections import defaultdict

from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.views.generic import ListView, DetailView, FormView, View
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.contrib.auth.views import logout as auth_logout
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.core.exceptions import PermissionDenied
from django.db.models import Count
from django.core.cache import cache
from django.conf import settings

from rest_framework import generics
from taggit.models import Tag
from bs4 import BeautifulSoup

from .models import Resource, TestRun, TestRunError
from .forms import (ResourceForm, StopForm, DiagLogsForm,
                    TestRunCreateForm, TestRunEditForm,
                    PolyrunForm, DhcperfForm)
from .serializers import TestRunSerializer
from . import helpers
from .runners import (run_polyrun, run_dhcperf, start_scheduled_run,
                      start_script_run)

from .tasks import available_tasks
from performance.tasks import (stop_task, progress, resource_status,
                               cached_resource_status)


class HomeView(TemplateView):
    """
    """
    template_name = 'home.html'

    def get_context_data(self, *args, **kwargs):
        """
        """
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        objs = TestRun.objects
        context.update({
            'running': objs.running(),
            'scheduled': objs.scheduled()[:5],
            'waiting': objs.waiting()[:5],
            'succeed': objs.successful().order_by('-completed_on')[:5],
            'failed': objs.failed().order_by('-completed_on')[:5],
            'resources': Resource.objects.filter(state=0).order_by('title'),
        })

        perf_by_model = cache.get('perf_by_model', '{}')
        perf_by_model = json.loads(perf_by_model)
        perf_by_keys = defaultdict(list)
        for model, keys in perf_by_model.items():
            for key, val in keys.items():
                perf_by_keys[key].append(val['run_pk'])

        context['perf_by_model'] = {}
        for key, val in perf_by_keys.items():
            context['perf_by_model'][key] = objs.filter(pk__in=val)

        return context


def performance_summary(request):
    """
    """
    perf_by_model = cache.get('perf_by_model', '{}')
    perf_by_model = json.loads(perf_by_model)
    perf_by_keys = defaultdict(list)
    run_pks = set()
    for model, keys in perf_by_model.items():
        for key, val in keys.items():
            run_pks.add(val['run_pk'])
            perf_by_keys[key].append(val)

    runs = {run.pk: run for run in TestRun.objects.filter(pk__in=run_pks)}
    for key, rows in perf_by_keys.items():
        for row in rows:
            run = runs.get(row['run_pk'])
            if run:
                row['model'] = run.inputs['Model']
                row['version'] = run.inputs['Version']
                row['engine'] = run.cache_engine

    return HttpResponse(json.dumps(perf_by_keys),
                        content_type='application/json')

def current_user(request):
    if not request.user.is_authenticated():
        user = {'username': None}
    else:
        user = {
            'full_name': request.user.get_full_name(),
            'username': request.user.username,
        }
    return HttpResponse(json.dumps(user),
                        content_type='application/json')


def logout(request):
    """
    """
    referer = request.META.get('HTTP_REFERER', reverse('home'))
    return auth_logout(request, next_page=referer)


class ResourceList(ListView):
    """
    """
    model = Resource


class TestRunList(ListView):
    """
    """
    model = TestRun


class TagList(ListView):
    """
    """
    model = Tag

    def get_queryset(self):
        """
        """
        qs = super(TagList, self).get_queryset()
        qs = qs.filter(taggit_taggeditem_items__gt=0)
        qs = qs.annotate(num_items=Count('taggit_taggeditem_items'))
        qs = qs.values()
        return qs


class TaggedTestRun(DetailView):
    """
    """
    model = Tag

    def get_context_data(self, *args, **kwargs):
        """
        """
        context = super(TaggedTestRun, self).get_context_data(*args, **kwargs)
        tag = self.get_object()
        context['runs'] = TestRun.objects.filter(tags__name=tag.name)
        return context


class ResourceDetail(helpers.DetailAndEditMixin, DetailView):
    """
    """
    model = Resource

    def get_form(self, **kwargs):
        """
        """
        return ResourceForm(**kwargs)

    def get_context_data(self, *args, **kwargs):
        """
        """
        context = super(ResourceDetail, self).get_context_data(*args, **kwargs)
        context['types'] = json.dumps(list(set([r.rtype for r in
                                                Resource.objects.all()])))
        return context


class TestRunDetail(DetailView):
    """
    """
    model = TestRun

    def get_context_data(self, *args, **kwargs):
        """
        """
        context = super(TestRunDetail, self).get_context_data(*args, **kwargs)
        run = self.get_object()

        context['edit_form'] = TestRunEditForm(instance=run)
        context['stop_form'] = StopForm()

        if run.finished:
            context['squid_conf'] = run.squid_conf()
            diag_logs = run.diag_files()
            if diag_logs:
                form = DiagLogsForm()
                form.fields['files'].choices = diag_logs
                context['diag_logs'] = form

        try:
            context['scripts'] = [t for t in available_tasks()
                                  if t in ('polyrun', 'dhcperf')]
        except:
            logging.exception('Could not find any script')

        if run.running:
            context['progress'] = run.get_cached_values('progress')

        return context

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        """
        """
        run = self.get_object()

        action = request.POST.get('action')
        if action:
            try:
                if action == 'start' and run.waiting:
                    run.set_inputs(script='custom')
                    run.start()
                elif action in ('stop', 'reset') and run.running:
                    if run.script_name == 'custom':
                        form = StopForm(request.POST)
                        if form.is_valid():
                            run.stop(form.cleaned_data['result'])
                    else:
                        stop_task.delay(run, action != 'reset')
                        if action == 'reset':
                            run.reset()
                        else:
                            run.force_stop()
            except TestRunError as e:
                messages.error(request, str(e))
        else:
            form = TestRunEditForm(instance=run, data=request.POST)
            if form.is_valid():
                run = form.save()
                run.inputs = form.cleaned_data['inputs']
                run.outputs = form.cleaned_data['outputs']
                run.save()
                messages.info(request, 'Changes saved successfully')
            else:
                self.object = run
                context = self.get_context_data()
                context['edit_form'] = form
                context['show_edit'] = True
                messages.error(request, 'Could not save edit')
                return self.render_to_response(context)

        return HttpResponseRedirect(run.get_absolute_url())


class ResourceCreate(helpers.CreatorFormMixin, CreateView):
    """
    """
    model = Resource
    form_class = ResourceForm

    def get_context_data(self, *args, **kwargs):
        """
        """
        context = super(ResourceCreate, self).get_context_data(*args, **kwargs)
        context['types'] = json.dumps(list(set([r.rtype for r in
                                                Resource.objects.all()])))
        return context


class TestRunCreate(helpers.CreatorFormMixin, CreateView):
    """
    """
    model = TestRun
    form_class = TestRunCreateForm


class CloneRunView(helpers.LoginRequiredDispatch, View):
    """
    """
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        """
        """
        run = get_object_or_404(TestRun, pk=request.POST.get('run_id'))
        submit = request.POST.get('_submit')
        if submit == 'reset':
            run.reset()
        else:
            run = run.clone(request.user)
        return HttpResponseRedirect(run.get_absolute_url())


class ScriptsFormView(helpers.LoginRequiredDispatch, FormView):
    """
    """
    template_name = 'performance/run_script.html'

    def get_form(self, form_class):
        """
        """
        form_kwargs = self.get_form_kwargs()
        script = self.kwargs.get('scr')
        form_kwargs['run'] = TestRun.objects.get(pk=self.kwargs.get('pk'))
        if not form_kwargs['run'].waiting:
            raise PermissionDenied
        form_kwargs['initial'] = form_kwargs['run'].script_inputs
        return {
            'polyrun': PolyrunForm,
            'dhcperf': DhcperfForm,
        }.get(script)(**form_kwargs)

    def form_valid(self, form):
        """
        """
        script = self.kwargs.get('scr')
        run = TestRun.objects.get(pk=self.kwargs.get('pk'))
        config = form.cleaned_data

        try:
            runner = {
                'polyrun': run_polyrun,
                'dhcperf': run_dhcperf,
            }.get(script)

            runner(run, config)
            messages.info(self.request,
                          '{} is scheduled to run'.format(script))
        except TestRunError as e:
            messages.error(self.request, str(e))

        return HttpResponseRedirect(run.get_absolute_url())


class CompareView(TemplateView):
    """
    """
    template_name = 'performance/compare.html'

    def get_context_data(self, *args, **kwargs):
        """
        """
        context = super(CompareView, self).get_context_data(*args, **kwargs)
        qs = Tag.objects.filter(taggit_taggeditem_items__gt=0)
        context['tags'] = qs.distinct()
        return context


class CompareTestRunListView(generics.ListAPIView):
    """
    """
    serializer_class = TestRunSerializer
    model = TestRun

    def get_queryset(self):
        """
        """
        getqs = self.request.GET
        if 'tags' not in getqs:
            return []

        tags = getqs.getlist('tags')
        if len(tags) == 1 and ',' in tags[0]:
            tags = tags[0].split(',')

        if not tags:
            return []

        qs = TestRun.objects.successful()
        for tag in tags:
            qs = qs.filter(tags__slug=tag)
        return qs


class PolyrunSummaryView(TemplateView):
    """
    """
    template_name = 'performance/polyrun_summary.html'

    def get_context_data(self, *args, **kwargs):
        """
        """
        context = super(PolyrunSummaryView, self).get_context_data(*args,
                                                                   **kwargs)
        run = get_object_or_404(TestRun, pk=self.kwargs['pk'])
        html_path = os.path.join(run.output_dir, 'summary',
                                 self.kwargs['page'])
        try:
            with open(html_path) as f:
                soup = BeautifulSoup(f.read())
        except IOError:
            raise Http404

        for table in soup.find_all('table'):
            table['class'] = 'table table-bordered table-striped'
        for img in soup.find_all('img'):
            src = img['src']
            img['src'] = '/'.join((run.polyrun_summary_url, src))
        for bq in soup.find_all('blockquote'):
            contents = bq.contents
            div = soup.new_tag('div')
            div.contents = contents
            bq.replace_with(div)

        context.update({
            'run': run,
            'report_title': soup.body.h1.text,
            'content': soup.body.prettify(),
        })
        return context


def testrun_state(request, pk):
    """
    """
    testrun = get_object_or_404(TestRun, pk=pk)
    return HttpResponse(testrun.state, content_type='text/plain')


def current_status(request):
    """
    """
    now = datetime.now()
    if now - current_status.last_progress_check > timedelta(minutes=1):
        progress.delay()
        resource_status.delay()
        current_status.last_progress_check = datetime.now()

    cached_resource_status.delay()

    runs = [{'title': run.title, 'id': run.pk, 'script': run.script_name,
             'start': run.started_on.isoformat(), 'eta': run.eta.isoformat(),
             'url': run.get_absolute_url()}
            for run in TestRun.objects.running()]
    response = HttpResponse(json.dumps(runs))
    response['content-type'] = 'application/json'
    return response
current_status.last_progress_check = datetime.now() - timedelta(minutes=10)


def progress_ts(request, pk):
    """
    """
    testrun = get_object_or_404(TestRun, pk=pk)
    ts = testrun.get_cached_values('progress-ts')
    ts = [{'polygraph': x['polygraph'], 'data': list(x['data'])}
          for x in ts]
    return HttpResponse(json.dumps(ts), content_type='application/json')


def diag_log(request, pk):
    """
    """
    run = get_object_or_404(TestRun, pk=pk)
    logfile = request.GET.get('logfile')
    logname = logfile.rsplit('/', 1)[-1]
    content = ''
    if logfile:
        path = os.path.join(run.output_dir, 'diag', logfile)
        if os.path.exists(path):
            with open(path, 'r') as f:
                content = f.read()

    if logname in ('alert_manager.log', 'appliance_api.log',
                   'bypass_watchdog.log', 'cmc_client.log', 'collectd.log',
                   'disk_manager.log', 'hardware_rescan.log', 'watchdog.log',
                   'web_ui.log'):
        logtype = 'Roadrunner'
    elif logname in ('cache.log', 'squidstart.log'):
        logtype = 'Squid'
    elif logname in ('cpuinfo', 'firmware', 'meminfo', 'model'):
        logtype = 'KeyValue'
    else:
        logtype = 'text'
    response = HttpResponse(json.dumps({'content': content,
                                        'logtype': logtype}))
    response['content-type'] = 'application/json'
    return response


def rrd_files(request, pk):
    """
    """
    run = get_object_or_404(TestRun, pk=pk)
    return HttpResponse(json.dumps(run.rrd_files()))


def start_scheduled(request, pk):
    """
    """
    if pk == 'all':
        start_scheduled_run()
    else:
        run = get_object_or_404(TestRun, pk=pk)
        start_script_run(run)

    return HttpResponseRedirect(request.META.get('HTTP_REFERER',
                                                 reverse('home')))


def dhcp_lps(request, pk):
    """
    """
    run = get_object_or_404(TestRun, pk=pk)
    if run.script_name != 'dhcperf':
        raise Http404

    def max_key(type_, value):
        return lambda x: x[type_][value]

    lps = []
    for p in run.script_outputs:
        pool = p['pool']['size']
        for t in p['tests']:
            clients = t['clients']
            discover_max = max(t['runs'], key=max_key('discover', 'qps'))
            renew_max = max(t['runs'], key=max_key('renew', 'qps'))
            lps.append({
                'pool': pool,
                'clients': clients,
                'discover': {
                    'hwm': discover_max['discover']['hwm'],
                    'qps': discover_max['discover']['qps'],
                },
                'renew': {
                    'hwm': renew_max['renew']['hwm'],
                    'qps': renew_max['renew']['qps'],
                },
            })

    return HttpResponse(json.dumps(lps), content_type='application/json')


def ng_last_modified(request):
    """
    Returns last modification time of angular app files
    """
    files = {f: os.stat(os.path.join(settings.NG_ROOT, f)).st_mtime
             for f in (
                 'index.html',
                 'app/app.css',
                 'app/app.js',
                 'app/lib.css',
                 'app/lib.js',
                 'app/templates.js',
             )}

    return HttpResponse(json.dumps(files),
                        content_type='application/json')
