# Django settings for performance project.

import os

PROJECT_PATH = os.path.dirname(__file__)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Nasimul Haque', 'nasim.haque@appliansys.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_PATH, 'performance.db'),
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['localhost', 'performance.coventry', 'performance.devnet',
                 'performance.appliansys.net']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/London'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-gb'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(os.path.dirname(PROJECT_PATH), 'assets', 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/assets/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(os.path.dirname(PROJECT_PATH), 'assets', 'static')

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/assets/static/'

NG_ROOT = os.path.join(os.path.dirname(PROJECT_PATH), 'angular', 'build')

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'compressor.finders.CompressorFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '5!bdv%ru=@r40y9y(rz7pla4_fna$nqi*#_!%yc9elx8!f1$9_'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    # 'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'performance.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'performance.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates"
    # or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    NG_ROOT,
)

TEMPLATE_CONTEXT_PROCESSORS = [
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    'performance.context_processors.context',
]

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'suit',
    'django.contrib.admin',
    'raven.contrib.django.raven_compat',
    'jsonfield',

    'django_markup',
    'social.apps.django_app.default',

    'crispy_forms',
    # 'rest_framework',
    'django_extensions',
    'django_pygments',
    'compressor',
    'taggit',
    # 'corsheaders',

    'attachments',
    'performance',
    'cdnrun',
    'rrtools.calamaris',
    # 'rrtools.eslog',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
    },
    'formatters': {
        'default': {
            'format': '%(asctime)s %(levelname)-8s %(name)-15s %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S',
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'tasks_log_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(os.path.dirname(PROJECT_PATH), 'assets',
                                     'logs', 'tasks.log'),
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 5,
            'formatter': 'default',
        },
        'log_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(os.path.dirname(PROJECT_PATH), 'assets',
                                     'logs', 'django.log'),
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 5,
            'formatter': 'default',
        },
        'sentry': {
            'level': 'DEBUG',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        },
        'socket.io': {
            'level': 'DEBUG',
            'class': 'performance.logger.SocketIOHandler',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins', 'log_file'],
            'level': 'ERROR',
            'propagate': True,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['log_file'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['log_file'],
            'propagate': False,
        },
        'celery': {
            'level': 'WARNING',
            'handlers': ['tasks_log_file', 'sentry'],
            'propagate': False,
        },
        'dhcperf': {
            'level': 'DEBUG',
            'handlers': ['tasks_log_file', 'socket.io'],
            'propagate': False,
        },
        'polyrun': {
            'level': 'DEBUG',
            'handlers': ['tasks_log_file', 'socket.io'],
            'propagate': False,
        },
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://127.0.0.1:6379/1',
    }
}

EMAIL_HOST = 'aspmx.l.google.com'
EMAIL_PORT = 25
EMAIL_HOST_USER = 'developers@appliansys.com'
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False

AUTHENTICATION_BACKENDS = (
    'social.backends.google.GoogleOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.social_auth.associate_by_email',
    'performance.pipeline.get_username',
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details'
)

LOGIN_URL = '/social_auth/login/google-oauth2/'
LOGIN_REDIRECT_URL = '/'
LOGIN_ERROR_URL = '/'

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '25140150348-bks3enuei386o6jgrvce5k7n9ssvuk8m.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'PHvtCI6rrxi4ejrXOQ000vpc'
SOCIAL_AUTH_USE_DEPRECATED_API = True

CRISPY_TEMPLATE_PACK = 'bootstrap3'

SUIT_CONFIG = {
    'ADMIN_NAME': 'Performance Admin',
    'MENU_ICONS': {
        'sites': 'icon-leaf',
        'auth': 'icon-lock',
        'social_auth': 'icon-off',
        'performance': 'icon-gift',
    },
    'MENU': (
        'performance',
        {'app': 'auth', 'label': 'Authorization', 'models': ('user', 'group')},
        {'app': 'social_auth', 'label': 'Social authentication'},
        'sites',
    ),
}

BROKER_URL = 'redis://localhost:6379/0'

RAVEN_CONFIG = {
    'dsn': 'http://2f110de82e5e4ac49eca771101e4cdc5:6f748b636c5b4219ab30cb8ea2d62473@sentry.coventry/1',
}

CACHEBOX_ADDRESS = '10.0.0.200:800'

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'performance.auth.CSRFExemptAuthentication',
    ],
}

CORS_ORIGIN_WHITELIST = ('localhost:9000',)

POLYRIG_SETUP = {
    '1': {
        'polysrv_ips': ['10.0.0.14', '10.0.0.15'],
        'polyclt_ips': ['10.0.0.12', '10.0.0.13'],
        'nameserver': '10.0.0.14',
        'title': 'Polyrig one',
    },
    '1-1': {
        'polysrv_ips': ['10.0.0.14'],
        'polyclt_ips': ['10.0.0.12'],
        'nameserver': '10.0.0.14',
        'title': 'Polyrig (half of) one',
    },
    '1-2': {
        'polysrv_ips': ['10.0.0.15'],
        'polyclt_ips': ['10.0.0.13'],
        'nameserver': '10.0.0.15',
        'title': 'Polyrig (other half of) one',
    },
    '2': {
        'polysrv_ips': ['10.0.0.18', '10.0.0.19'],
        'polyclt_ips': ['10.0.0.16', '10.0.0.17'],
        'nameserver': '10.0.0.18',
        'title': 'Polyrig two',
    },
    '2-1': {
        'polysrv_ips': ['10.0.0.18'],
        'polyclt_ips': ['10.0.0.16'],
        'nameserver': '10.0.0.18',
        'title': 'Polyrig (half of) two',
    },
    '2-2': {
        'polysrv_ips': ['10.0.0.19'],
        'polyclt_ips': ['10.0.0.17'],
        'nameserver': '10.0.0.19',
        'title': 'Polyrig (other half of) two',
    },
    '4x4-1': {
        'polysrv_ips': ['10.0.0.16', '10.0.0.17', '10.0.0.18', '10.0.0.19'],
        'polyclt_ips': ['10.0.0.12', '10.0.0.13', '10.0.0.14', '10.0.0.15'],
        'nameserver': '10.0.0.18',
        'title': 'Polyrig 4x4',
    },
}

TIMESERVER = '10.0.0.10'

CELERYD_CONCURRENCY = 20
CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']

try:
    from .settings_local import *
except ImportError:
    pass
