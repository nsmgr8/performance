"""
Celery tasks and helpers
========================
"""
import subprocess as sp
import logging

import celery
from celery import shared_task, signals
# import gevent
from django.core.cache import cache
from django.core.urlresolvers import reverse

from polygraph.polyrun import Polyrun
from dhcperf.dhcperf import Dhcperf

from performance.models import TestRun, Resource
from performance import socketioclient

logger = logging.getLogger('celery')


@shared_task
def polyrun(config, run_pk):
    """
    """
    poly = Polyrun(config)
    summary = poly.main()
    r = TestRun.objects.get(pk=run_pk)
    r.success(summary)


@shared_task
def dhcperf(config, run_pk):
    """
    """
    dhc = Dhcperf(config)
    output = dhc.main()
    r = TestRun.objects.get(pk=run_pk)
    r.success(output)


@shared_task
def progress():
    """
    """
    with socketioclient.SocketIO('localhost', 8001) as sock:
        perf_sock = sock.define(socketioclient.BaseNamespace, '/performance')
        runs = TestRun.objects.running()
        for run in runs:
            try:
                run.check_progress(perf_sock)
            except:
                logger.exception('Error in progress check')


@shared_task
def cached_resource_status():
    """
    """
    with socketioclient.SocketIO('localhost', 8001) as sock:
        perf_sock = sock.define(socketioclient.BaseNamespace,
                                '/performance')
        resource_status = cache.get('resource:status') or {}
        for resource, status in resource_status.items():
            perf_sock.emit('resource_status', {'res_pk': resource,
                                               'status': status})


@shared_task
def resource_status():
    """
    """
    return
    jobs = [gevent.spawn(check_status, resource)
            for resource in Resource.objects.all()]
    gevent.joinall(jobs)


def check_status(resource):
    """
    """
    if not resource.ip_address:
        return

    with socketioclient.SocketIO('localhost', 8001) as sock:
        perf_sock = sock.define(socketioclient.BaseNamespace,
                                '/performance')
        try:
            sp.check_call('ping -c1 -w1 {}'
                          .format(resource.ip_address).split(),
                          stdout=sp.PIPE, stderr=sp.PIPE)
            message = 'Reachable'
            if resource.unavailable:
                resource.set_available()
                perf_sock.emit('refresh',
                               {'level': 'info',
                                'title': 'Resource',
                                'message': '{} is now available'
                                .format(resource.title),
                                'url': reverse('resources'),
                                })
        except sp.CalledProcessError:
            message = 'Unreachable'
            if resource.idle:
                resource.set_unavailable()
                perf_sock.emit('refresh',
                               {'level': 'warning',
                                'title': 'Resource',
                                'message': '{} is unavailable'
                                .format(resource.title),
                                'url': reverse('resources'),
                                })

            if resource.in_use:
                msg = ('{} is unreachable, '
                       'but currently required by test').format(resource.title)
                perf_sock.emit('status', {'level': 'error',
                                          'title': 'Lost resource',
                                          'message': msg})

        resource_status = cache.get('resource:status') or {}
        resource_status[resource.pk] = message
        cache.set('resource:status', resource_status)
        perf_sock.emit('resource_status', {'res_pk': resource.pk,
                                           'status': message})


@signals.task_failure.connect()
def task_failure_handler(*args, **kwargs):
    """
    """
    exc = kwargs.get('exception')
    if exc:
        logger.error(exc)
    try:
        p = TestRun.objects.get(pk=kwargs['args'][1])
        p.fail()
    except IndexError:
        pass
    except:
        logger.exception('args: {} kwargs: {}'.format(args, kwargs))


def available_tasks():
    """
    """
    i = celery.current_app.control.inspect()
    task_names = set()
    for host, tasks in i.registered().items():
        if not host.endswith('performance.coventry'):
            continue
        for t in tasks:
            if t.startswith('performance.'):
                task_names.add(t.split('.')[-1])
    return task_names


@shared_task
def stop_task(run, collect_report=False):
    """
    """
    celery.current_app.control.revoke(run.script_id, terminate=True)
    if run.script_name == 'polyrun':
        poly = Polyrun(run.script_inputs)
        poly.stop_polyrun()
        if collect_report:
            # Refresh the run object from db
            run = TestRun.objects.get(pk=run.pk)
            run.fetch_report()
    if run.script_name == 'dhcperf':
        Dhcperf.stop_dhcperf()


@shared_task
def fetch_report(run):
    """
    """
    run.fetch_report()
