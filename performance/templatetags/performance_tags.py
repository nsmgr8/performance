# -*- coding: utf-8 -*-

import math
import re

from django import template
from django.utils.safestring import mark_safe
from django.template.defaultfilters import stringfilter, floatformat

from performance.forms import CACHE_INIT
from polygraph.polyrun import Polyrun

register = template.Library()
ORDERS = u'yzafpnµm kMGTPEZY'


@register.inclusion_tag('performance/_script_result.html')
def script_result(testrun):
    name = testrun.script_name
    if not name or not (testrun.script_inputs or testrun.script_outputs):
        return {}

    try:
        result = {
            'polyrun': _polyrun_values,
            'dhcperf': _dhcperf_values,
        }[name](testrun)
    except KeyError:
        return {}

    return {'testrun': testrun,
            'script_result': 'performance/_{}_result.html'.format(name),
            name: result}


def _dhcperf_values(run):
    return run.script_outputs


def _polyrun_values(run):
    inputs = []

    _inputs = run.script_inputs
    if _inputs:
        try:
            duration = Polyrun(_inputs).schedule
        except:
            duration = '-'

        inputs = (
            ('CACHEbox IP', _inputs.get('cachebox_ip')),
            ('Initialisation',
             dict(CACHE_INIT).get(_inputs.get('cache_init'))),
            ('Polygraph Servers', ', '.join(_inputs.get('polysrv_ips',
                                                        []))),
            ('Polygraph Clients', ', '.join(_inputs.get('polyclt_ips',
                                                        []))),
            ('Proxy Type', _inputs.get('proxy_type', '-').title()),
            ('Fill Phase', '{} RPS'.format(_inputs.get('fill_rate', '-'))),
            ('CDN test', _inputs.get('test_cdn')),
            ('Cache Size', '{} GB'.format(_inputs.get('cache_size', '-'))),
            (_inputs.get('schedule_type', '').title(),
                '{} RPS'.format(_inputs.get('schedule_value', '-'))),
            ('Phase duration',
                '{} minutes'.format(_inputs.get('duration', '-'))),
            ('Total duration', '<pre>{}</pre>'.format(duration)),
        )

    return {'inputs': inputs, 'outputs': run.script_outputs}


@register.filter
@stringfilter
def param_split_val(value):
    values = value.split('/')
    if len(values) == 1:
        return value
    return mark_safe('{0} <small class="text-muted">{1}</small>'
                     .format(*values))


@register.filter
def si_format(number):
    """Format a number to engineer scale"""
    order = number and int(math.floor(math.log(abs(number)) / math.log(1000)))
    human_readable = ORDERS.split(" ")[int(order > 0)]
    if order == 0 or order > len(human_readable):
        return floatformat(number / (1000 ** int(order)))
    return (floatformat(number / (1000 ** int(order))) +
            human_readable[int(order) - int(order > 0)])


@register.filter
def tag_class(value):
    return re.sub(r' ', '-', value.lower())
