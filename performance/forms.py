"""
Performance forms
=================
"""
from django import forms
from django.conf import settings

from suit.widgets import SuitSplitDateTimeWidget
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Reset, Submit
from crispy_forms.bootstrap import InlineRadios, AppendedText

from .models import Resource, TestRun, TestRunConst
from .widgets import ParamsWidget


POLYRUN_SCHEDULES = (
    ('steps', 'Steps'),
    ('ramp', 'Ramp'),
    ('constant', 'Constant'),
)
""" polyrun schedules choices """

POLYRUN_RECIPES = [
    ('contents-2014.pg', 'Recipe 2014'),
    ('contents-2011.pg', 'Recipe 2011'),
    ('contents-2014-BIG.pg', 'Recipe 2014 (BIG objects)'),
    ('contents-2015-LARGE.pg', 'Recipe 2015 (LARGE objects)'),
]
""" polyrun recipe choices """

CACHE_INIT = (
    ('quickstart', 'Quickstart'),
    ('reload', 'Restart squid'),
    ('init', 'Initialise CACHEbox'),
    ('reboot', 'Reboot All'),
    ('non-cachebox', 'Non-CACHEbox'),
)
""" cache Initialisation choices """

PROXY_TYPES = (
    ('explicit', 'Explicit proxy'),
    ('transparent', 'Transparent proxy'),
    ('bridge', 'Bridge mode'),
    ('bypass', 'Bypass proxy'),
    ('reverse', 'Reverse proxy'),
)
""" proxy types choices """

POLYRIG_CHOICES = sorted([(k, v['title'])
                          for k, v in settings.POLYRIG_SETUP.items()])
""" polyrig choices """

CACHE_ENGINES = (
    ('squid2', 'Squid 2'),
    ('squid3', 'Squid 3'),
)
""" cache engine choices """

BOOLEAN_CHOICES = (
    (True, 'Yes'),
    (False, 'No'),
)


def resources(rtype):
    """
    Resource choices generator

    :returns: a generator of tuple (pk, title)
    """
    for r in Resource.objects.filter(rtype__iexact=rtype):
        yield (r.pk, '{} ({})'.format(r.title, r.ip_address))


class PanelFormHelper(FormHelper):
    def __init__(self, *fields):
        super(PanelFormHelper, self).__init__()
        self.form_class = 'form-horizontal'
        self.label_class = 'col-sm-3'
        self.field_class = 'col-sm-8'
        self.layout = Layout(
            Div(*fields, css_class='panel-body'),
            Div(
                Div(
                    Submit('submit', 'Submit'),
                    Reset('reset', 'Reset'),
                    css_class='col-sm-offset-3 col-sm-8'),
                Div(css_class='clearfix'),
                css_class='panel-footer')
        )


class ResourceForm(forms.ModelForm):
    """
    """
    class Meta:
        model = Resource
        fields = ('title', 'ip_address', 'rtype', 'description')

    helper = PanelFormHelper(*Meta.fields)


class TestRunCreateForm(forms.ModelForm):
    """
    """
    inputs = forms.CharField(required=False, widget=ParamsWidget,
                             label='Control parameters')

    class Meta:
        model = TestRun
        fields = ('title', 'tags', 'inputs', 'resources',
                  'team', 'estimated_days', 'description')

    helper = PanelFormHelper(*Meta.fields)


class TestRunEditForm(forms.ModelForm):
    """
    """
    inputs = forms.CharField(required=False, widget=ParamsWidget,
                             label='Control parameters')
    outputs = forms.CharField(required=False, widget=ParamsWidget,
                              label='Output parameters')

    class Meta:
        model = TestRun
        fields = ('title', 'tags', 'description', 'report', 'resources',
                  'team', 'inputs', 'outputs')

    helper = PanelFormHelper(*Meta.fields)


class StopForm(forms.Form):
    """
    """
    result = forms.ChoiceField(required=True, initial=TestRunConst.SUCCESS,
                               choices=((TestRunConst.SUCCESS, 'Successful'),
                                        (TestRunConst.FAIL, 'Failed')))


class TestRunChangeForm(forms.ModelForm):
    """
    """
    class Meta:
        model = TestRun
        fields = ('title', 'created_by', 'tags', 'description',
                  'resources', 'team', 'started_on', 'completed_on', 'eta',
                  'estimated_days', 'queue', 'state', 'inputs', 'outputs',
                  'script', 'script_inputs', 'script_outputs', 'report')
        widgets = {
            'started_on': SuitSplitDateTimeWidget,
            'completed_on': SuitSplitDateTimeWidget,
        }


class DiagLogsForm(forms.Form):
    files = forms.ChoiceField(required=False, label='Select a log file')

    def __init__(self, *args, **kwargs):
        super(DiagLogsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-8'
        self.helper.layout = Layout(
            Div('files', css_class='panel-body'),
        )


class PolyrunForm(forms.Form):
    """
    """
    SCHED_VAL_ERR = 'Enter integers with : as separator'
    SCHED_VAL_HELP = ('Enter [start:[end]] for ramp, [int] for constant, '
                      '[start:end:stepsize] for steps (RPS)')
    REPEATABLE_HELP = ('To generate a repeatable traffic select "Yes"')
    RANGES_HELP = ('Enter a positive integer to generate HTTP range requests')
    FILL_RATE_HELP = ('Enter a positive integer to include a fill phase')

    recipe = forms.ChoiceField(choices=POLYRUN_RECIPES)
    polyrig = forms.ChoiceField(choices=POLYRIG_CHOICES,
                                label='Polygraph setup')

    schedule_type = forms.ChoiceField(choices=POLYRUN_SCHEDULES,
                                      widget=forms.RadioSelect)
    schedule_value = forms.RegexField(regex=r'^\d+(:\d+){0,2}$',
                                      help_text=SCHED_VAL_HELP,
                                      error_messages={'invalid':
                                                      SCHED_VAL_ERR})
    duration = forms.IntegerField(initial=10, label='Phase duration')
    repeatable = forms.TypedChoiceField(required=False, initial=False,
                                        help_text=REPEATABLE_HELP,
                                        choices=BOOLEAN_CHOICES,
                                        coerce=lambda x: x == 'True',
                                        widget=forms.RadioSelect)

    cache_size = forms.IntegerField(initial=1000, label='Cache size')
    fill_rate = forms.IntegerField(required=True, initial=0, min_value=0,
                                   label='Fill rate', help_text=FILL_RATE_HELP)

    proxy_type = forms.ChoiceField(choices=PROXY_TYPES)
    cache_init = forms.ChoiceField(choices=CACHE_INIT)
    cachebox = forms.ChoiceField(choices=resources('cachebox'), required=False)
    cache_engine = forms.ChoiceField(choices=CACHE_ENGINES, required=False)
    test_cdn = forms.TypedChoiceField(required=False, initial=False,
                                      choices=BOOLEAN_CHOICES,
                                      coerce=lambda x: x == 'True',
                                      widget=forms.RadioSelect)
    ranges = forms.IntegerField(initial=0, label='Partial content',
                                max_value=70, min_value=0,
                                help_text=RANGES_HELP)

    helper = PanelFormHelper('recipe', 'polyrig',
                             InlineRadios('schedule_type'),
                             AppendedText('schedule_value', 'RPS'),
                             AppendedText('duration', 'minutes'),
                             AppendedText('cache_size', 'GB'),
                             AppendedText('fill_rate', 'RPS'),
                             AppendedText('ranges', '%'),
                             InlineRadios('repeatable'),
                             'proxy_type', 'cache_init', 'cachebox',
                             InlineRadios('cache_engine'),
                             InlineRadios('test_cdn'))

    limitation = 'performance/_polyrun_limitation.html'

    class Media:
        js = ('js/polyrun.js',)

    def __init__(self, *args, **kwargs):
        """
        """
        run = kwargs.pop('run')
        super(PolyrunForm, self).__init__(*args, **kwargs)
        try:
            self.fields['cachebox'].choices = list(resources('cachebox'))
            appliance = run.resources.get(rtype__iexact='cachebox')
            if appliance:
                self.fields['cachebox'].initial = appliance.pk
        except:
            pass

    def clean_schedule_value(self):
        """
        """
        stype = self.cleaned_data['schedule_type']
        sval = self.cleaned_data['schedule_value']
        if not sval:
            return sval

        vlen = len(sval.split(':'))

        if stype == 'steps' and vlen != 3:
            raise forms.ValidationError('Steps type requires '
                                        '[start:end:step] value')
        if stype == 'ramp' and vlen > 2:
            raise forms.ValidationError('Ramp type requires '
                                        '[[start]:end] value')
        if stype == 'constant' and vlen > 1:
            raise forms.ValidationError('Constant type requires '
                                        'a single integer')

        return sval

    def clean(self):
        """
        """
        cleaned_data = super(PolyrunForm, self).clean()
        fill_rate = cleaned_data.get('fill_rate')
        stype = cleaned_data.get('schedule_type')
        sval = cleaned_data.get('schedule_value')
        if not sval:
            return cleaned_data

        if stype == 'ramp':
            peak_rate = sval.split(':')[-1]
        elif stype == 'constant':
            peak_rate = sval
        elif stype == 'steps':
            peak_rate = sval.split(':')[1]
        else:
            peak_rate = 10000  # Default maximum RPS

        if int(peak_rate) <= fill_rate:
            msg = 'Fill rate must be less than peak rate {}'.format(peak_rate)
            self._errors['fill_rate'] = self.error_class([msg])
            del cleaned_data['fill_rate']

        return cleaned_data


class DhcperfForm(forms.Form):
    """
    """
    dnsbox = forms.ChoiceField(choices=resources('dnsbox'), label='DNSbox')
    port = forms.IntegerField(initial=6700, min_value=6000, max_value=7000,
                              label='dhcpd port', help_text='Please enter the '
                              'dhcpd port number. Note that, it requires to '
                              'be in [6000, 7000]')

    helper = PanelFormHelper('dnsbox', 'port')

    def __init__(self, *args, **kwargs):
        """
        """
        run = kwargs.pop('run')
        super(DhcperfForm, self).__init__(*args, **kwargs)
        try:
            self.fields['dnsbox'].choices = list(resources('dnsbox'))
            appliance = run.resources.get(rtype__iexact='dnsbox')
            if appliance:
                self.fields['dnsbox'].initial = appliance.pk
        except:
            pass
