def get_username(details, user=None, *args, **kwargs):
    """Return an username for new user. Return current user username
    if user was given.
    """
    return {'username': user.username if user
            else details.get('email').split('@')[0]}
