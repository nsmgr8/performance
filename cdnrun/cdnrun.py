from datetime import datetime
from collections import defaultdict
import json
import os
import random
import time
import configparser
import re
import logging
import glob

from selenium import webdriver
from selenium.common.exceptions import (NoSuchElementException,
                                        ElementNotVisibleException)
from pyvirtualdisplay import Display
import sh

from .browsermobproxy import Server as BMPServer


logger = logging.getLogger('cdnrun')
WAIT_FOR_VIDEO = 300
bmp_location = os.path.join(os.path.dirname(__file__),
                            'bmp', 'bin', 'browsermob-proxy')


class CDNRun(object):
    def __init__(self, **kwargs):
        self.cb_proxy = kwargs.pop('cb_proxy')
        cdn_inis = kwargs.pop('cdn_inis', '/tmp/cdns')
        self.load_url_patterns(cdn_inis)

    def __enter__(self):
        self.setup()
        return self

    def __exit__(self, type, value, traceback):
        self.teardown()

    def setup(self):
        self.start_bmp()
        self.display = Display(visible=0, size=(1280, 1024))
        self.display.start()
        self.start_browser()

    def start_bmp(self):
        self.bmp_server = BMPServer(bmp_location)
        self.bmp_server.start()
        self.bmp_proxy = self.bmp_server.create_proxy(self.cb_proxy)

    def stop_bmp(self):
        try:
            self.bmp_proxy.close()
        except:
            logger.exception('Could not close bmp proxy')
        self.bmp_server.stop()

    def start_browser(self):
        browsers = ('chrome', 'firefox')
        browser = random.choice(browsers)
        if browser == 'chrome':
            cc = webdriver.ChromeOptions()
            cc.add_argument('--proxy-server={}'.format(self.bmp_proxy.proxy))
            self.browser = webdriver.Chrome(chrome_options=cc)
        elif browser == 'firefox':
            self.browser = webdriver.Firefox(
                proxy=self.bmp_proxy.webdriver_proxy())

    def teardown(self):
        self.browser.quit()
        self.display.stop()
        self.stop_bmp()
        sh.rm('-rf', '/tmp/seleniumSslSupport*')

    def reset(self):
        self.teardown()
        self.setup()

    def play_youtube(self):
        logger.info('Starting youtube cdn check')
        self.browser.get('http://www.youtube.com')
        videos = self.browser.find_elements_by_css_selector('a.ux-thumb-wrap')
        for i in range(100):
            try:
                video = random.choice(videos)
                video_url = video.get_attribute('href')
                vtime = video.find_element_by_class_name('video-time').text
                video.click()
                time.sleep(WAIT_FOR_VIDEO)
                self.bmp_proxy.new_har(video_url)
                self.browser.get(video_url)
                time.sleep(WAIT_FOR_VIDEO)
                player = self.browser.find_element_by_id('movie_player')
                pltype = {
                    'div': 'HTML5',
                    'embed': 'Flash',
                }.get(player.tag_name, 'unknown')
                vt = self.browser.find_element_by_id('watch-headline-title')
                vtitle = vt.text
                break
            except ElementNotVisibleException:
                pass
        else:
            raise Exception('Could not click on any video')

        info = self.get_har()
        info['meta'].update({
            'player': pltype,
            'title': vtitle,
            'video_time': vtime,
        })

        return info

    def play_bliptv(self):
        logger.info('Starting blip.tv cdn check')
        self.browser.get('http://blip.tv')
        videos = self.browser.find_elements_by_css_selector('.EpisodeCardLink')
        for i in range(100):
            try:
                video = random.choice(videos)
                video_url = video.get_attribute('href')
                video.click()
                time.sleep(WAIT_FOR_VIDEO)
                self.bmp_proxy.new_har(video_url)
                self.browser.get(video_url)
                vtitle = self.browser.find_element_by_css_selector(
                    '.TheaterDescription h3').text
                time.sleep(WAIT_FOR_VIDEO)
                break
            except ElementNotVisibleException:
                pass
        else:
            raise Exception('Could not click on any video')

        info = self.get_har()
        info['meta'].update({
            'title': vtitle,
        })
        return info

    def play_vimeo(self):
        logger.info('Starting vimeo cdn check')
        self.browser.get('http://vimeo.com/categories')
        cats = self.browser.find_elements_by_css_selector('ul#categories a')
        random.choice(cats[1:]).click()
        videos = self.browser.find_elements_by_css_selector('li[id^=clip_] a')
        for i in range(100):
            try:
                video = random.choice(videos)
                video_url = video.get_attribute('href')
                video.click()
                self.browser.find_element_by_css_selector(
                    'button[title=Play]').click()
                time.sleep(WAIT_FOR_VIDEO)
                self.bmp_proxy.new_har(video_url)
                self.browser.get(video_url)
                self.browser.find_element_by_css_selector(
                    'button[title=Play]').click()
                vtitle = self.browser.find_element_by_css_selector(
                    '.video_meta h1').text
                time.sleep(WAIT_FOR_VIDEO)
                break
            except ElementNotVisibleException:
                pass
        else:
            raise Exception('Could not click on any video')

        info = self.get_har()
        info['meta'].update({
            'title': vtitle,
        })
        return info

    def play_flickr(self):
        logger.info('Starting flickr cdn check')
        self.browser.get('http://www.flickr.com/explore/video/')
        cats = self.browser.find_elements_by_css_selector('.pl_group h4 a')
        random.choice(cats).click()
        videos = self.browser.find_elements_by_css_selector(
            '[data-photo-media=video] a[data-track=photo-click]')
        for i in range(100):
            try:
                video = random.choice(videos)
                video_url = video.get_attribute('href')
                video.click()
                time.sleep(3)
                self.browser.find_element_by_css_selector(
                    '.yui3-videoplayer-startscreen-button').click()
                time.sleep(WAIT_FOR_VIDEO)
                self.bmp_proxy.new_har(video_url)
                self.browser.get(video_url)
                time.sleep(3)
                self.browser.find_element_by_css_selector(
                    '.yui3-videoplayer-startscreen-button').click()
                vtitle = self.browser.find_element_by_css_selector(
                    '.photo-title').text
                time.sleep(WAIT_FOR_VIDEO)
                break
            except ElementNotVisibleException:
                pass
        else:
            raise Exception('Could not click on any video')

        info = self.get_har()
        info['meta'].update({
            'title': vtitle,
        })
        return info

    def play_veoh(self):
        logger.info('Starting veoh cdn check')
        raise NotImplementedError

    def play_photobucket(self):
        logger.info('Starting photobucket cdn check')
        self.browser.get('http://photobucket.com/popular/videos')
        videos = self.browser.find_elements_by_css_selector(
            '[data-mediatype=video] a')
        for i in range(100):
            try:
                video = random.choice(videos)
                video_url = video.get_attribute('href')
                video.click()
                time.sleep(WAIT_FOR_VIDEO)
                self.bmp_proxy.new_har(video_url)
                self.browser.get(video_url)
                vtitle = self.browser.find_element_by_css_selector(
                    'h1#mediaTitle').text
                time.sleep(WAIT_FOR_VIDEO)
                break
            except ElementNotVisibleException:
                pass
        else:
            raise Exception('Could not click on any video')

        info = self.get_har()
        info['meta'].update({
            'title': vtitle,
        })
        return info

    def play_metacafe(self):
        logger.info('Starting metacafe cdn check')
        self.browser.get('http://www.metacafe.com')
        videos = self.browser.find_elements_by_css_selector(
            'a[class^=Catalog2]')
        for i in range(100):
            try:
                video = random.choice(videos)
                video_url = video.get_attribute('href')
                vtitle = video.get_attribute('title')
                video.click()
                time.sleep(WAIT_FOR_VIDEO)
                self.bmp_proxy.new_har(video_url)
                self.browser.get(video_url)
                time.sleep(WAIT_FOR_VIDEO)
                break
            except ElementNotVisibleException:
                pass
        else:
            raise Exception('Could not click on any video')

        info = self.get_har()
        info['meta'].update({
            'title': vtitle,
        })
        return info

    def play_globo(self):
        logger.info('Starting globotv cdn check')
        self.browser.get('http://globotv.globo.com')
        videos = self.browser.find_elements_by_css_selector(
            'li[data-video-id] a')
        for i in range(100):
            try:
                video = random.choice(videos)
                video_url = video.get_attribute('href')
                video.click()
                time.sleep(WAIT_FOR_VIDEO)
                self.bmp_proxy.new_har(video_url)
                self.browser.get(video_url)
                vtitle = self.browser.find_element_by_css_selector(
                    '.detalhes-video h3').text.strip()
                time.sleep(WAIT_FOR_VIDEO)
                break
            except ElementNotVisibleException:
                pass
        else:
            raise Exception('Could not click on any video')

        info = self.get_har()
        info['meta'].update({
            'title': vtitle,
        })
        return info

    def play_dailymotion(self):
        logger.info('Starting dailymotion cdn check')
        self.browser.get('http://www.dailymotion.com')
        videos = self.browser.find_elements_by_css_selector('a.preview_link')
        for i in range(100):
            try:
                video = random.choice(videos)
                video_url = video.get_attribute('href')
                video.click()
                time.sleep(WAIT_FOR_VIDEO)
                self.bmp_proxy.new_har(video_url)
                self.browser.get(video_url)
                vtitle = self.browser.find_element_by_css_selector(
                    '#video_title').text
                time.sleep(WAIT_FOR_VIDEO)
                break
            except ElementNotVisibleException:
                pass
        else:
            raise Exception('Could not click on any video')

        info = self.get_har()
        info['meta'].update({
            'title': vtitle,
        })
        return info

    @property
    def cachebox_version(self):
        cb_ip = self.cb_proxy.split(':')[0]
        self.browser.get('https://{}/system/upgrade'.format(cb_ip))
        try:
            self.browser.find_element_by_id('loginbox')
            u = self.browser.find_element_by_id('username')
            p = self.browser.find_element_by_id('password')
            u.send_keys('admin')
            p.send_keys('password')
            self.browser.find_element_by_tag_name('button').click()
        except NoSuchElementException:
            pass
        version1 = self.browser.find_elements_by_xpath(
            "//div[@id='footercontent']//td")[-1].text
        version2 = self.browser.find_elements_by_tag_name('td')[0].text
        if '+' in version2:
            return version1[:-1] + '+' + version2.split('+')[-1]
        return version1

    def get_har(self):
        def to_dict(item):
            return {x['name']: x['value'] for x in item}

        def sort_by_name(l):
            return sorted(l, key=lambda x: x['name'])

        har = self.bmp_proxy.har['log']
        entries = har['entries']
        info = {
            'meta': {
                'page_url': har['pages'][0]['id'],
                'browser': '{name} {version}'.format(**har['browser']),
                'checked_at': datetime.utcnow().isoformat(),
                'cachebox': self.cachebox_version,
            },
        }

        urls = []
        total_size, cached_size = 0, 0
        squid_version_re = re.compile(r'squid/([\d+])')
        squid_version = None
        for entry in entries:
            request = entry['request']
            response = entry['response']
            content = response['content']
            resp_hdrs = to_dict(response['headers'])
            cache_status = '/'.join([resp_hdrs.get(k, 'NA').split()[0]
                                     for k in ('X-Cache', 'X-Cache-Lookup')])
            total_size += content['size']
            if not squid_version:
                via = resp_hdrs.get('Via', '')
                matches = squid_version_re.search(via)
                if matches:
                    version = matches.groups()[0]
                squid_version = 'squid' + version

            if cache_status.lower() == 'hit/hit':
                cached_size += content['size']

            urls.append({
                'url': request['url'].split('?')[0],
                'rewritten_url': self.rewrite(request['url'], squid_version),
                'method': request['method'],
                'status': response['status'],
                'cache_status': cache_status,
                'content_type': content['mimeType'],
                'size': content['size'],
                'query_string': sort_by_name(request['queryString']),
                'cookies': sort_by_name(request['cookies']),
                'request_headers': sort_by_name(request['headers']),
                'response_headers': sort_by_name(response['headers']),
            })

        info['data'] = sorted(urls, key=lambda x: -x['size'])
        info['meta'].update({
            'total_size': total_size,
            'cached_size': cached_size,
        })

        return info

    def load_url_patterns(self, cdn_inis):
        cdn_inis = cdn_inis.rstrip('/')
        ini_files = glob.glob('{}/*/*.ini'.format(cdn_inis))
        self.nocache = defaultdict(list)
        self.rw_patterns = defaultdict(list)
        self.available_cdns = set()
        for ini_file in ini_files:
            conf = configparser.SafeConfigParser()
            conf.read(ini_file)
            _, engine, name = ini_file.rsplit('/', 2)
            self.available_cdns.add(name.replace('.ini', ''))

            if conf.has_option('REWRITE', 'transform'):
                transform = conf.get('REWRITE', 'transform')
                patterns = [u.split() for u in transform.splitlines()
                            if u and not u.startswith('#')]
                self.rw_patterns[engine].extend(
                    [(u[0], re.sub('\$(\d+)', r'{\1}', u[1]))
                     for u in patterns])
            if conf.has_option('DIRECTIVES', 'nocache'):
                nocache = conf.get('DIRECTIVES', 'nocache')
                self.nocache[engine].extend([u for u in nocache.splitlines()
                                             if u and not u.startswith('#')])

    def rewrite(self, url, engine):
        for regex in self.nocache.get(engine, []):
            if re.search(regex, url):
                return '__no_cache__'
        for regex, rw in self.rw_patterns.get(engine, []):
            m = re.search(regex, url)
            if m:
                return rw.format('', *m.groups())


if __name__ == '__main__':
    cr = CDNRun()
    info = cr.play_youtube()
    print(json.dumps(info))
    cr.teardown()
