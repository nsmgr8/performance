from django import template


register = template.Library()


@register.filter
def rounded_bytes(value, unit=''):
    """
    """
    units = ((10 ** 3, 'K'),
             (10 ** 6, 'M'),
             (10 ** 9, 'G'),
             (10 ** 12, 'T'),
             (10 ** 15, 'P'))
    for m, u in reversed(units):
        if value > m:
            return '{} {}{}'.format(round(float(value) / float(m), 2), u, unit)
    return '{} {}'.format(value, unit)
