from django.db import models
from django.core.urlresolvers import reverse

from jsonfield import JSONField


class CDNRun(models.Model):
    cdn = models.CharField(max_length=255)
    run_at = models.DateTimeField(auto_now_add=True)
    meta_data = JSONField()
    data = JSONField()

    class Meta:
        ordering = ('-run_at',)

    def get_absolute_url(self):
        return reverse('cdnrun-detail', kwargs={'pk': self.pk})

    @property
    def embed_url(self):
        return self.meta_data['page_url'].replace('watch?v=', 'embed/')

    @property
    def cache_percent(self):
        total, cached = (float(self.meta_data[x])
                         for x in ('total_size', 'cached_size'))
        return round(cached / total * 100.0, 2)

    @property
    def cdn_name(self):
        return {
            'youtube': 'Youtube',
            'bliptv': 'blip.tv',
        }.get(self.cdn, self.cdn)
