"""
A python wrapper around browsermobproxy (http://bmp.lightbody.net/)

This can be used to run selenium UI test and capture the HTTP request/response
in HAR format.
"""

import json
import os
import socket
import subprocess
import time

import requests
from selenium import webdriver


class Server(object):

    def __init__(self, path, **options):
        """
        Initialises a Server object

        :param path: Path to the browsermob proxy binary
        :param options: port value (default 8080)
        """
        path = os.path.expanduser(path)
        if not os.path.isfile(path):
            raise Exception("Browsermob-Proxy binary couldn't be found in path"
                            " provided: %s" % path)

        self.port = options.get('port', 8080)
        self.process = None
        self.command = [path, '--port=%s' % self.port]

    def start(self):
        """
        This will start the browsermob proxy and then wait until it can
        interact with it
        """
        self.log_file = open(os.path.abspath('/tmp/bmp-server.log'), 'w')
        self.process = subprocess.Popen(self.command,
                                        stdout=self.log_file,
                                        stderr=subprocess.STDOUT)
        count = 0
        while not self._is_listening():
            time.sleep(0.5)
            count += 1
            if count == 60:
                self.stop()
                raise Exception("Can't connect to Browsermob-Proxy")

    def stop(self):
        """
        This will stop the process running the proxy
        """
        if self.process.poll() is not None:
            return

        try:
            self.process.kill()
            self.process.wait()
        except AttributeError:
            # kill may not be available under windows environment
            pass

        self.log_file.close()

    @property
    def url(self):
        """
        Gets the url that the proxy is running on. This is not the URL clients
        should connect to.
        """
        return "http://localhost:%d" % self.port

    def create_proxy(self, fwd_proxy=None):
        """
        Gets a client class that allow to set all the proxy details that you
        may need to.

        :param fwd_proxy: address of a upstream proxy
        """
        client = Client(self.url, fwd_proxy)
        return client

    def _is_listening(self):
        try:
            socket_ = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            socket_.settimeout(1)
            socket_.connect(("localhost", self.port))
            socket_.close()
            return True
        except socket.error:
            return False


class Client(object):
    def __init__(self, url, fwd_proxy=None):
        """
        Initialises a new Client object
        :param url: This is where the BrowserMob Proxy lives
        :param fwd_proxy: address of a upstream proxy
        """
        self.host = url
        if fwd_proxy:
            resp = requests.post('%s/proxy?httpProxy=%s' % (self.host,
                                                            fwd_proxy))
        else:
            resp = requests.post('%s/proxy' % self.host)
        jcontent = json.loads(resp.text)
        self.port = jcontent['port']
        url_parts = self.host.split(":")
        self.proxy = url_parts[1][2:] + ":" + str(self.port)

    def close(self):
        """
        shuts down the proxy and closes the port
        """
        r = requests.delete('%s/proxy/%s' % (self.host, self.port))
        return r.status_code

    def webdriver_proxy(self):
        """
        Returns a Selenium WebDriver Proxy class with details of the HTTP Proxy
        """
        return webdriver.Proxy({
            "httpProxy": self.proxy,
            "sslProxy": self.proxy,
        })

    @property
    def har(self):
        """
        Gets the HAR that has been recorded
        """
        r = requests.get('%s/proxy/%s/har' % (self.host, self.port))

        return r.json()

    def new_har(self, ref=None):
        """
        This sets a new HAR to be recorded
        :param ref: A reference for the HAR. Defaults to None
        """
        payload = {'captureHeaders': True}
        if ref:
            payload["initialPageRef"] = ref
        r = requests.put('%s/proxy/%s/har' % (self.host, self.port), payload)
        if r.status_code == 200:
            return (r.status_code, r.json())
        else:
            return (r.status_code, None)
