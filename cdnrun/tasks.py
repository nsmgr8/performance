import logging

from celery import shared_task

from django.conf import settings

from .cdnrun import CDNRun
from .models import CDNRun as CDNModel

logger = logging.getLogger('cdnrun')


@shared_task
def cdn_check():
    with CDNRun(cb_proxy=settings.CACHEBOX_ADDRESS) as cr:
        for cdn in sorted(cr.available_cdns):
            try:
                info = getattr(cr, 'play_{}'.format(cdn))()
                CDNModel(cdn=cdn, meta_data=info['meta'],
                         data=info['data']).save()
            except (AttributeError, NotImplementedError):
                logger.debug('CDN check for {} not implemented yet'.format(cdn))
            except:
                logger.exception('Could not play {}'.format(cdn))
            else:
                logger.info('{} run is complete'.format(cdn),
                            extra={'socketio': {'emit': 'cdnrun_load'}})
