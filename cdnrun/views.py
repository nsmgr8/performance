from urllib.parse import urlparse
from collections import defaultdict
import json

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.views.generic import TemplateView, DetailView, ListView

from braces.views import JSONResponseMixin, AjaxResponseMixin

import celery

from .models import CDNRun
from .tasks import cdn_check


class IndexView(TemplateView):
    template_name = 'cdnrun/index.html'


def json_data(request):
    runs = CDNRun.objects.all().values('pk', 'cdn', 'meta_data')
    json_list = defaultdict(list)
    for run in runs:
        data = {'cdn': run['cdn'], 'id': run['pk'],
                'url': reverse('cdnrun-detail', args=(run['pk'],))}
        data.update(json.loads(run['meta_data']))
        json_list[run['cdn']].append(data)
    return HttpResponse(json.dumps(dict(json_list)),
                        content_type='application/json')


class CDNDetailView(JSONResponseMixin, AjaxResponseMixin, DetailView):
    model = CDNRun

    def get_ajax(self, request, *args, **kwargs):
        return self.render_json_object_response([self.get_object()])


class DiffView(ListView):
    template_name = 'cdnrun/cdn_diff_list.html'
    paginate_by = 7

    def get_queryset(self):
        return CDNRun.objects.filter(cdn=self.kwargs['cdn'])

    def get_context_data(self, *args, **kwargs):
        context = super(DiffView, self).get_context_data(*args, **kwargs)
        runs = context['object_list']
        videos = [c.data[0] for c in runs]
        params = set()
        urls = []
        for v in videos:
            urls.append(urlparse(v['url']))
            for q in v['query_string']:
                params.add(q['name'])
        params = sorted(params)

        table = []
        for u in ('netloc', 'path'):
            data = [u]
            for v in urls:
                data.append(getattr(v, u))
            table.append(data)

        for k in ('method', 'status', 'content_type', 'size', 'cache_status'):
            data = [k]
            for v in videos:
                data.append(v[k])
            table.append(data)

        for param in params:
            data = [param]
            for v in videos:
                for q in v['query_string']:
                    if q['name'] == param:
                        data.append(q['value'])
                        break
                else:
                    data.append('')
            table.append(data)
        context['params'] = table
        return context


def start_cdn_check(request):
    i = celery.current_app.control.inspect()
    for host, tasks in i.active().items():
        for task in tasks:
            if 'cdnrun.tasks.cdn_check' == task.get('name'):
                messages.warning(request, 'A CDN checker is already running')
                return HttpResponseRedirect(reverse('cdnrun-index'))

    messages.info(request, 'A CDN checker is scheduled')
    cdn_check.delay()
    return HttpResponseRedirect(reverse('cdnrun-index'))
