from django.conf.urls import patterns, url

from . import views


urlpatterns = patterns(
    '',
    url('^$', views.IndexView.as_view(), name='cdnrun-index'),
    url('^json/$', views.json_data, name='cdnrun-json'),
    url('^(?P<pk>\d+)/$',
        views.CDNDetailView.as_view(), name='cdnrun-detail'),
    url('^(?P<cdn>\w+)/diff/$',
        views.DiffView.as_view(), name='cdnrun-cdn-diff'),
    url('^start-cdn-check/$', views.start_cdn_check,
        name='cdnrun-start_cdn_check'),
)
