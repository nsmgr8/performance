from datetime import datetime, timedelta

from django.core.management.base import BaseCommand

from cdnrun.models import CDNRun


class Command(BaseCommand):
    """
    """
    help = 'Delete older cdnrun data'

    def handle(self, *args, **kwargs):
        """
        """
        since = datetime.now() - timedelta(days=10)
        runs = CDNRun.objects.filter(run_at__lt=since)
        runs.delete()
