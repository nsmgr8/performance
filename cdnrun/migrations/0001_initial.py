# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CDNRun',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('cdn', models.CharField(max_length=255)),
                ('run_at', models.DateTimeField(auto_now_add=True)),
                ('meta_data', jsonfield.fields.JSONField(default=dict)),
                ('data', jsonfield.fields.JSONField(default=dict)),
            ],
            options={
                'ordering': ('-run_at',),
            },
        ),
    ]
