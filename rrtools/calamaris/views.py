import json

from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView
from django.shortcuts import get_object_or_404

from .models import CalamarisCollection
from .forms import CalamarisCollectionForm
from performance.helpers import CreatorFormMixin


class CalamarisCollectionListView(ListView):
    model = CalamarisCollection


class CalamarisCollectionDetailView(DetailView):
    model = CalamarisCollection

    def get_context_data(self, *args, **kwargs):
        context = super(CalamarisCollectionDetailView,
                        self).get_context_data(*args, **kwargs)
        context['periods'] = ('daily', 'monthly')
        return context


class CalamarisCollectionCreateView(CreatorFormMixin, CreateView):
    model = CalamarisCollection
    form_class = CalamarisCollectionForm


def calamaris_reports(request, pk):
    calamaris = get_object_or_404(CalamarisCollection, pk=pk)
    return HttpResponse(json.dumps(calamaris.reports()),
                        content_type='application/json')
