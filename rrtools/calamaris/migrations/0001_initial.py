# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CalamarisCollection'
        db.create_table(u'calamaris_calamariscollection', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(default='NO NAME', max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('bundle', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal(u'calamaris', ['CalamarisCollection'])


    def backwards(self, orm):
        # Deleting model 'CalamarisCollection'
        db.delete_table(u'calamaris_calamariscollection')


    models = {
        u'calamaris.calamariscollection': {
            'Meta': {'object_name': 'CalamarisCollection'},
            'bundle': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "'NO NAME'", 'max_length': '255'})
        }
    }

    complete_apps = ['calamaris']