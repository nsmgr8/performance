# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'CalamarisCollection.created_on'
        db.add_column(u'calamaris_calamariscollection', 'created_on',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2014, 4, 21, 0, 0), blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'CalamarisCollection.created_on'
        db.delete_column(u'calamaris_calamariscollection', 'created_on')


    models = {
        u'calamaris.calamariscollection': {
            'Meta': {'ordering': "('created_on',)", 'object_name': 'CalamarisCollection'},
            'bundle': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['calamaris']