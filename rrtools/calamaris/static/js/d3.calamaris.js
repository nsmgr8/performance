(function() {
    d3.calamaris = d3.calamaris || {};

    var margin = {top: 20, right: 50, bottom: 30, left: 50},
        width = 1100, height = 400;

    var yFormat = d3.format("s"), nFormat = d3.format('.02s'),
        pFormat = d3.format('%'), tFormat = d3.time.format('%d %B, %Y');

    var x = d3.time.scale(), y = d3.scale.linear(),
        xAxis = d3.svg.axis().scale(x).orient("bottom").ticks(5),
        yAxis = d3.svg.axis().scale(y).orient("left").tickFormat(yFormat).ticks(5),
        ydomain;

    var xDetail = d3.scale.linear(), yRatio = d3.scale.linear(),
        yRatioAxis = d3.svg.axis().scale(yRatio).orient('right').ticks(5).tickFormat(pFormat);

    var area = d3.svg.area()
        .x(function(d) { return x(d.date); })
        .y0(function(d) { return y(d.y0); })
        .y1(function(d) { return y(d.y0 + d.y); });

    var line = d3.svg.line()
        .x(function(d) { return x(d.date); })
        .y(function(d) { return yRatio(d.value); });

    var stack = d3.layout.stack()
        .values(function(d) { return d.values; });

    function create_svg(elem) {
        return elem.append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
        .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    }

    var calamaris_summary = function(elem, data, proto) {
        var svg_elem = elem + ' .tcp-requests';
        height = 200;
        width = $(svg_elem).width() - margin.left - margin.right;
        var hover_line = [[0, 0], [0, height]];

        d3.select(svg_elem).selectAll('svg').remove();
        var svg = create_svg(d3.select(svg_elem));

        x.domain(d3.extent(data, function(d) { return d.date; }))
            .range([0, width]);
        yAxis.orient('left');

        var color_summary = d3.scale.category10();

        var draw = function(field, yrange) {
            ydomain = d3.max(data.map(function(d) { return d.summary[proto][field].total; }));
            y.domain([0, ydomain]).range(yrange);

            var columns = ['hit', 'miss', 'error'];
            var stacked_data = stack(columns.map(function(name) {
                return {
                    name: name,
                    values: data.map(function(d) {
                        return {
                            date: d.date,
                            y: d.summary[proto][field][name]
                        };
                    })
                };
            }));

            var class_name = 'stack-' + proto + '-' + field;
            var stack_g = svg.selectAll('.' + class_name)
                .data(stacked_data)
                .enter().append("g")
                .attr("class", class_name);

            stack_g.append("path")
                .attr("class", "area")
                .attr("d", function(d) { return area(d.values); })
                .style("fill", function(d) { return color_summary(field + ' ' + d.name); });

            var label_x = width + 10,
                label_y = (yrange[0] + yrange[1]) / 2;
            svg.append('text')
                .attr('transform',
                      'translate(' + label_x + ',' + label_y + ')rotate(90)')
                .attr('text-anchor', 'middle')
                .text(field);

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis);
        };

        draw('bytes', [height/2 - 10, 0]);
        draw('requests', [height/2 + 10, height]);

        var total = {
            bytes: {hit: 0, miss: 0, total: 0},
            requests: {hit: 0, miss: 0, total: 0},
            period: tFormat(data[0].date) + ' - ' + tFormat(data[data.length - 1].date)
        };
        data.forEach(function(d) {
            ['bytes', 'requests'].forEach(function(field) {
                ['hit', 'miss', 'total'].forEach(function(type) {
                    total[field][type] += d.summary.tcp[field][type];
                });
            });
        });

        function summary_total(caption, tcp) {
            d3.select(elem).select('caption').text(caption);
            ['bytes', 'requests'].forEach(function(field) {
                ['hit', 'miss', 'total'].forEach(function(type) {
                    d3.select(elem).select('.' + field + ' .' + type)
                        .text(nFormat(tcp[field][type]));
                });
                d3.select(elem).select('.' + field + ' .ratio')
                    .text(pFormat(tcp[field].hit / tcp[field].total));
            });
            d3.select(elem).select('.avg-obj-size')
                .text(nFormat(tcp.bytes.total/tcp.requests.total));
        }

        summary_total(total.period, total);

        var xLinear = d3.scale.linear()
            .domain([0, data.length - 1]).range([0, width]);
        svg.selectAll('[class^=stack-]').on('mousemove', function() {
            var xm = d3.event.x - $(this).offset().left;
            var pos = d3.round(xLinear.invert(xm));
            summary_total(tFormat(data[pos].date), data[pos].summary.tcp);
            hover_line[0][0] = xm;
            hover_line[1][0] = xm;
            svg.select('.hover-line')
                .attr('stroke', 'red')
                .attr('d', function(d) { return d3.svg.line()(hover_line); });
        }).on('mouseout', function(d) {
            summary_total(total.period, total);
            svg.select('.hover-line')
                .attr('stroke', 'none');
        });

        svg.append('path')
            .attr('class', 'hover-line')
            .attr('d', function(d) { return d3.svg.line()(hover_line); });

        var legend = svg.selectAll(".legend")
            .data(['bytes hit', 'bytes miss', 'bytes error',
                   'requests hit', 'requests miss', 'requests error'])
            .enter().append("g")
            .attr("class", "legend")
            .attr("transform", function(d, i) {
                return "translate("+ (i * 120 + 20) + "," + (height + 10) + ")";
            });

        legend.append("rect")
            .attr("x", -18)
            .attr("width", 18)
            .attr("height", 18)
            .style("fill", color_summary);

        legend.append("text")
            .attr("x", 5)
            .attr("y", 9)
            .attr("dy", ".35em")
            .text(function(d) { return d; });

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + (height/2 - 14) + ")")
            .call(xAxis);
    };

    var calamaris_section = function(elem, data, section) {
        var $summary = d3.select(elem).select('.section-summary'),
            $detail = d3.select(elem).select('.section-detail'),
            $detail_table = d3.select(elem).select('table');
        $summary.selectAll('svg').remove();
        $detail.selectAll('svg').remove();

        var color_section = d3.scale.category20c();
        var hit_miss = data.map(function(d, i) {
            var section_data = d.sections[section];
            var y0 = [0, 0];
            var values = ['bytes', 'requests'].map(function(field, i) {
                return section_data.map(function(x) {
                    return ['hit', 'miss'].map(function(type, j) {
                        return {
                            label: x.label,
                            date: d.date,
                            color: d3.rgb(color_section(x.label)).darker(1 - j),
                            y0: y0[i],
                            y: y0[i] += x[field][type]
                        };
                    });
                });
            });
            return {
                date: d.date,
                index: i,
                bytes: d3.merge(values[0]),
                requests: d3.merge(values[1])
            };
        });

        height = 400;
        width = $(elem + ' .section-summary').width() - margin.left - margin.right;
        var svg = create_svg($summary);

        x.domain(d3.extent(data, function(d) { return d.date; }))
            .range([0, width]);
        yAxis.orient('left');

        var stack_width = width / data.length;
        var draw = function(field, yrange) {
            ydomain = d3.max(data.map(function(d) { return d.summary.tcp[field].total; }));
            y.domain([ydomain, 0]).range(yrange);

            var rect_y, rect_h;
            if (yrange[0] > yrange[1]) {
                rect_y = function(d) { return y(d.y0); };
                rect_h = function(d) { return y(d.y) - y(d.y0); };
            } else {
                rect_y = function(d) { return y(d.y); };
                rect_h = function(d) { return y(d.y0) - y(d.y); };
            }

            var class_name = 'stack-' + section + '-' + field;
            var stack_g = svg.selectAll('.' + class_name)
                .data(hit_miss)
                .enter().append("g")
                .attr("class", class_name)
                .attr("transform", function(d) {
                    return "translate(" + x(d.date) + ",0)";
                });

            stack_g.selectAll('.bar')
                .data(function(d) { return d[field]; })
                .enter()
                .append('rect')
                .attr('class', 'bar')
                .attr('width', stack_width)
                .style('fill', function(d) { return d.color; })
                .attr("y", rect_y)
                .attr("height", rect_h);

            var label_x = width + stack_width + 2,
                label_y = (yrange[0] + yrange[1]) / 2;
            svg.append('text')
                .attr('transform',
                      'translate(' + label_x + ',' + label_y + ')rotate(90)')
                .attr('text-anchor', 'middle')
                .text(field);

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis);
        };

        draw('bytes', [0, height/2 - 10]);
        draw('requests', [height, height/2 + 10]);

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + (height/2 - 14) + ")")
            .call(xAxis);

        $summary.selectAll('g[class^=stack-]')
            .on('mouseover', function(d) {
                draw_detail(d.index);
                d3.select(this).selectAll('.bar')
                    .style('stroke', function(d) {
                        return d3.rgb(d3.select(this).style('fill')).brighter(5);
                    })
                    .style('stroke-width', 2);
            })
            .on('mouseout', function(d) {
                d3.select(this).selectAll('.bar')
                    .style('stroke', 'none');
            });

        height = 200;
        var svg_detail = create_svg($detail);
        var draw_detail = function(index) {
            svg_detail.selectAll('*').remove();
            d3.select(elem + ' .section-date').text(tFormat(data[index].date));
            var color_section = d3.scale.category20c();
            var section_data = data[index].sections[section];
            section_data.forEach(function(d) {
                ['bytes', 'requests'].map(function(x) {
                    d[x].ratio = d[x].total / data[index].summary.tcp[x].total;
                });
            });
            var values = section_data.map(function(x) {
                var vals = ['bytes', 'requests'].map(function(field, i) {
                    var y0 = [0, 0];
                    return ['hit', 'miss'].map(function(type, j) {
                        return {
                            label: x.label,
                            date: data[index].date,
                            color: d3.rgb(color_section(x.label+field)).darker(1 - j),
                            y0: y0[i],
                            y: y0[i] += x[field][type]
                        };
                    });
                });
                return {
                    bytes: vals[0],
                    requests: vals[1]
                };
            });

            xDetail.domain([0, values.length]).range([0, width]);
            var stack_width = width / values.length;

            var draw = function(field, yrange) {
                ydomain = d3.max(section_data.map(function(d) { return d[field].total; }));
                y.domain([ydomain, 0]).range(yrange);
                yRatio.range($.extend([], yrange).reverse());

                var rect_y, rect_h;
                if (yrange[0] > yrange[1]) {
                    rect_y = function(d) { return y(d.y0); };
                    rect_h = function(d) { return y(d.y) - y(d.y0); };
                    rr_y = yRatio(0);
                    rr_h = function(d, i) { return yRatio(section_data[i][field].ratio) - yRatio(0); };
                } else {
                    rect_y = function(d) { return y(d.y); };
                    rect_h = function(d) { return y(d.y0) - y(d.y); };
                    rr_y = function(d, i) { return yRatio(section_data[i][field].ratio); };
                    rr_h = function(d, i) { return yRatio(1 - section_data[i][field].ratio); };
                }

                var class_name = 'stack-' + field;
                var stack_g = svg_detail.selectAll('.' + class_name)
                    .data(values)
                    .enter().append("g")
                    .attr("class", class_name)
                    .attr("transform", function(d, i) {
                        return "translate(" + xDetail(i) + ",0)";
                    });

                stack_g.selectAll('.bar')
                    .data(function(d) { return d[field]; })
                    .enter()
                    .append('rect')
                    .attr('class', 'bar')
                    .attr('width', stack_width)
                    .style('fill', function(d) { return d.color; })
                    .attr("y", rect_y)
                    .attr("height", rect_h);

                stack_g.append('rect')
                    .attr({
                        x: stack_width / 4,
                        width: stack_width / 2,
                        y: rr_y,
                        height: rr_h,
                        'fill-opacity': 0.3
                    });

                var label_x = width + 10,
                    label_y = (yrange[0] + yrange[1]) / 2;
                svg_detail.append('text')
                    .attr('transform',
                        'translate(' + label_x + ',' + label_y + ')rotate(90)')
                    .attr('text-anchor', 'middle')
                    .text(field);

                svg_detail.append("g")
                    .attr("class", "y axis")
                    .call(yAxis);
            };

            draw('bytes', [0, height/2 - 10]);
            draw('requests', [height, height/2 + 10]);

            svg_detail.append('g')
                .selectAll('.label')
                .data(values)
                .enter()
                .append('text')
                .attr({
                    'class': 'label',
                    'text-anchor': 'middle',
                    'transform': function(d, i) {
                        return 'translate(' + (xDetail(i) + stack_width / 2) + ',' + (height / 2 + 5) + ')';
                    },
                })
                .text(function(d) { return d.bytes[0].label.substr(0, 10); });

            function summary_total(caption, tcp) {
                var $table = d3.select(elem + ' .section-detail-table');
                $table.select('caption').text(caption);
                ['bytes', 'requests'].forEach(function(field) {
                    ['hit', 'miss', 'total'].forEach(function(type) {
                        $table.select('.' + field + ' .' + type)
                            .text(nFormat(tcp[field][type]));
                    });
                    $table.select('.' + field + ' .percentage')
                        .text(pFormat(tcp[field].ratio));
                    $table.select('.' + field + ' .ratio')
                        .text(pFormat(tcp[field].hit / tcp[field].total));
                });
                $table.select('.avg-obj-size')
                    .text(nFormat(tcp.bytes.total/tcp.requests.total));
            }

            summary_total(section_data[0].label, section_data[0]);
            svg_detail.selectAll('g[class^=stack-]')
                .on('mouseover', function(d, i) {
                    var detail_data = section_data[i % section_data.length];
                    summary_total(detail_data.label, detail_data);
                });
        };
        draw_detail(data.length - 1);
    };

    var calamaris_trend = function(elem, data, section, row) {
        d3.select(elem).selectAll('svg').remove();
        var svg = create_svg(d3.select(elem + ' .trend-graph'));

        var hit_miss = ['bytes', 'requests'].map(function(field) {
            return ['hit', 'miss'].map(function(name) {
                return {
                    name: name,
                    values: data.map(function(d, i) {
                        var section_data = d.sections[section];
                        var values = section_data.filter(function(x) {
                            return x.label === row;
                        });
                        return {
                            date: d.date,
                            y: values[0] ? values[0][field][name] : 0,
                            percent: values[0] ? values[0][field][name] / values[0][field].total : 0
                        };
                    })
                };
            });
        });
        var bytes_requests = {
            bytes: hit_miss[0],
            requests: hit_miss[1]
        };

        x.domain(d3.extent(data, function(d) { return d.date; }))
            .range([0, width]);
        yAxis.orient('left');
        var color_summary = d3.scale.category20();
        var hover_line = [[0, 0], [0, height]];

        var draw = function(field, yrange) {
            ydomain = d3.max(data.map(function(d) {
                var section_data = d.sections[section];
                var values = section_data.filter(function(x) {
                    return x.label === row;
                });
                return values[0] ? values[0][field].total : 0;
            }));
            y.domain([0, ydomain]).range(yrange);
            yRatio.range(yrange);

            var columns = ['hit', 'miss'];
            var stacked_data = stack(bytes_requests[field]);

            var class_name = 'stack-' + field;
            var stack_g = svg.selectAll('.' + class_name)
                .data(stacked_data)
                .enter().append("g")
                .attr("class", class_name);

            stack_g.append("path")
                .attr("class", "area")
                .attr("d", function(d) { return area(d.values); })
                .style("fill", function(d) { return color_summary(field + ' ' + d.name); });

            svg.append("path")
                .datum(stacked_data.map(function(d) {
                    return d.values.map(function(x) {
                        return {
                            date: x.date,
                            value: x.percent
                        };
                    });
                })[0])
                .attr("class", "bhr")
                .style('stroke', color_summary('bhr'))
                .style('stroke-width', 2)
                .style('fill', 'none')
                .attr("d", line);

            var label_x = width / 2,
                label_y = (yrange[0] + yrange[1]) / 2;
            svg.append('text')
                .attr('transform',
                      'translate(0,' + yrange[1] + ')')
                .attr('text-anchor', 'start')
                .text(field);

            svg.append('text')
                .attr('transform',
                      'translate(' + width + ',' + yrange[1] + ')')
                .attr('text-anchor', 'end')
                .text(field === 'bytes' ? 'BHR' : 'DHR');

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis);

            svg.append("g")
                .attr("class", "y axis")
                .attr('transform', 'translate(' + width + ',0)')
                .call(yRatioAxis);
        };

        draw('bytes', [height/2 - 10, 0]);
        draw('requests', [height/2 + 10, height]);

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + (height/2 - 14) + ")")
            .call(xAxis);

        var total = {
            bytes: {hit: 0, miss: 0, total: 0},
            requests: {hit: 0, miss: 0, total: 0},
            period: tFormat(data[0].date) + ' - ' + tFormat(data[data.length - 1].date)
        };
        var grand_total = {
            bytes: {total: 0},
            requests: {total: 0}
        };
        data.forEach(function(d) {
            ['bytes', 'requests'].forEach(function(field) {
                grand_total[field].total += d.summary.tcp[field].total;
            });

            var values = d.sections[section].filter(function(x) {
                return x.label === row;
            });
            if (values.length > 0) {
                ['bytes', 'requests'].forEach(function(field) {
                    ['hit', 'miss', 'total'].forEach(function(type) {
                        total[field][type] += values[0][field][type];
                    });
                });
            }
        });

        function summary_total(caption, tcp, total) {
            d3.select(elem).select('caption').text(caption);
            ['bytes', 'requests'].forEach(function(field) {
                ['hit', 'miss', 'total'].forEach(function(type) {
                    d3.select(elem).select('.' + field + ' .' + type)
                        .text(nFormat(tcp[field][type]));
                });
                d3.select(elem).select('.' + field + ' .ratio')
                    .text(pFormat(tcp[field].hit / tcp[field].total));
                if (total) {
                    d3.select(elem).select('.' + field + ' .percentage')
                        .text(pFormat(tcp[field].total / total[field].total));
                }
            });
            d3.select(elem).select('.avg-obj-size')
                .text(nFormat(tcp.bytes.total/tcp.requests.total));
        }

        summary_total(total.period, total, grand_total);

        var xLinear = d3.scale.linear()
            .domain([0, data.length - 1]).range([0, width]);
        svg.selectAll('[class^=stack-]').on('mousemove', function() {
            var xm = d3.event.x - $(this).offset().left;
            var pos = d3.round(xLinear.invert(xm));
            var sec_row = data[pos].sections[section], column;
            for(var i=0; i<sec_row.length; i++) {
                if (sec_row[i].label === row) {
                    column = sec_row[i];
                    break;
                }
            }
            summary_total(tFormat(data[pos].date), column, data[pos].summary.tcp);
            hover_line[0][0] = xm;
            hover_line[1][0] = xm;
            svg.select('.hover-line')
                .attr('stroke', 'red')
                .attr('d', function(d) { return d3.svg.line()(hover_line); });
        }).on('mouseout', function(d) {
            summary_total(total.period, total, grand_total);
            svg.select('.hover-line')
                .attr('stroke', 'none');
        });

        svg.append('path')
            .attr('class', 'hover-line')
            .attr('d', function(d) { return d3.svg.line()(hover_line); });

    };

    function show_section(elem, data, sections, section) {
        calamaris_section(elem, data, section);
        d3.select(elem + ' .section-title').text(sections[section].title);
        d3.select(elem + ' .section-label-title').text(sections[section].label_title);
        var rows = d3.set(d3.merge(data.map(function(d) {
            return d.sections[section].map(function(x) {
                return x.label;
            });
        }))).values();
        $(elem + ' .section-labels').select2({
            data: rows.map(function(x) { return {id: x, text: x}; })
        }).on('change', function(e) {
            calamaris_trend(elem + ' .section-trend', data, section, e.val);
        });
        $(elem + ' .section-labels').select2('val', rows[0]);
        calamaris_trend(elem + ' .section-trend', data, section, rows[0]);
    }

    d3.calamaris.render = function(elem, data, sections) {
        if (!data || data.length === 0) return;
        calamaris_summary(elem, data, 'tcp');
        var section_keys = d3.keys(sections)
            .filter(function(x) { return x !== 'http_method'; });

        var section_radios = d3.select(elem + ' .section-reports').selectAll('.radio')
            .data(section_keys).enter()
            .append('div')
            .attr('class', 'radio')
            .append('label');

        section_radios.append('input')
            .attr('type', 'radio')
            .attr('name', elem.slice(1) + '-section')
            .attr('value', function(d) { return d; })
            .on('click', function(x) {
                show_section(elem, data, sections, x);
            });

        section_radios.append('span')
            .text(function(d) { return sections[d].title; });

        $(elem + ' input[value=second_domain]').prop('checked', true);
        show_section(elem, data, sections, 'second_domain');
    };
})();
