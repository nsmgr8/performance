from django.conf.urls import patterns, url

from . import views


urlpatterns = patterns('',
    url(r'^$', views.CalamarisCollectionListView.as_view(),
        name='calamaris-list'),
    url(r'^create/$', views.CalamarisCollectionCreateView.as_view(),
        name='calamaris-create'),
    url(r'^(?P<pk>\d+)/$', views.CalamarisCollectionDetailView.as_view(),
        name='calamaris-detail'),
    url(r'^(?P<pk>\d+)/reports/$', views.calamaris_reports,
        name='calamaris-reports'),
)
