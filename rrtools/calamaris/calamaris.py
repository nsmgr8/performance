# -*- coding: utf-8 -*-

import os
import re
from collections import defaultdict
from datetime import datetime
import math


RBHT = ('requests', 'bytes', 'requests_hit', 'bytes_hit', 'elapsed')
RBTH = ('requests', 'bytes', 'elapsed', 'requests_hit', 'bytes_hit')
RBT = ('requests', 'bytes', 'elapsed')

is_ascii_re = re.compile(r'^[ -~]*$')


class Calamaris(object):
    SECTIONS = {
        'http_method': {
            'index': '3',
            'title': 'HTTP Methods',
            'label_title': 'HTTP Method',
            'columns': RBT,
        },
        'second_domain': {
            'index': '8',
            'title': 'Top 2nd Level Domains',
            'label_title': '2nd Level Domain',
            'columns': RBHT,
        },
        'top_domain': {
            'index': '9',
            'title': 'Top Domains',
            'label_title': 'Domain',
            'columns': RBHT,
        },
        'content_type': {
            'index': '11',
            'title': 'Content Types',
            'label_title': 'Content Type',
            'columns': RBHT,
        },
        'extensions': {
            'index': '12',
            'title': 'Extensions',
            'label_title': 'Extension',
            'columns': RBHT,
        },
        'size_dist': {
            'index': '15',
            'title': 'Size Distribution',
            'label_title': 'Size Range',
            'columns': RBTH,
        },
        'response_code': {
            'index': '20',
            'title': 'Response Codes',
            'label_title': 'HTTP Response Code',
            'columns': RBHT,
        },
    }

    def __init__(self, fname):
        self.cache_file = fname
        self.summary = {}
        self.sections = {}
        self.load(fname)

    def load(self, fname):
        with open(fname, 'rb') as f:
            content = f.read()

        matrix = []
        for line in content.splitlines():
            row = []
            for column in line.split(b'\xb5'):
                try:
                    row.append(column.decode())
                except:
                    row.append('0')
            matrix.append(row)

        if matrix and matrix[0][0] == '0':
            self._load_summary(matrix)
        self._load_sections(matrix)

    def _load_summary(self, matrix):
        data = [float(x) if '.' in x else int(x) for x in matrix[0][1:]]
        tcp, udp = data[16:28], data[7:16]
        keyval = lambda x: dict(zip(('total', 'hit', 'miss', 'error'), x))
        self.summary = {
            'tcp': {
                'requests': keyval(tcp[::3]),
                'bytes': keyval(tcp[1::3]),
                'elapsed': keyval(tcp[2::3]),
            },
            'udp': {
                'requests': keyval(udp[::3]),
                'bytes': keyval(udp[1::3]),
                'elapsed': keyval(udp[2::3]),
            },
        }

    def _load_sections(self, matrix):
        skeys = {v['index']: k for k, v in self.SECTIONS.items()}

        data = defaultdict(list)
        for row in matrix:
            if row[0] not in skeys:
                continue

            skey = skeys[row[0]]
            section = self.SECTIONS[skey]
            row_dict = {
                'label': is_ascii_re.match(row[1]) and row[1] or '<unknown>',
            }

            for i, column in enumerate(section['columns'], 2):
                value = float(row[i]) if column == 'elapsed' else int(row[i])

                if column.endswith('_hit'):
                    ctype, _ = column.split('_')
                    row_dict[ctype] = {'total': row_dict[ctype],
                                       'hit': value,
                                       'miss': row_dict[ctype] - value}
                else:
                    row_dict[column] = value

            data[skey].append(row_dict)

        self.sections = dict(data)
        if self.sections.get('size_dist'):
            for row in self.sections['size_dist']:
                order = int(row['label'])
                row['label'] = '{}-{}'.format(humanize(int(10 ** order)),
                                              humanize(10 ** (order + 1) - 1))


class CalamarisCollection(object):
    def __init__(self, cache_dir, file_prefix='cache'):
        files = sorted([f for f in os.listdir(cache_dir)])
        patterns = {
            'daily': r'^%s\.\d{4}-\d{2}-\d{2}$' % file_prefix,
            'monthly': r'^%s\.\d{4}-\d{2}$' % file_prefix,
        }
        self.reports = {k: [os.path.join(cache_dir, f)
                            for f in files if re.match(v, f)]
                        for k, v in patterns.items()}

    def get_report(self, as_datetime=False):
        def date_formatter_func():
            if as_datetime:
                return lambda x: datetime.strptime(x, date_fmt)
            return lambda x: x
        date_formatter = date_formatter_func()

        dfmt = {'daily': '%Y-%m-%d', 'monthly': '%Y-%m'}
        reports = defaultdict(list)
        for period, files in self.reports.items():
            date_fmt = dfmt[period]
            for f in files:
                calamaris = Calamaris(f)
                date_str = f.rsplit('.', 1)[-1]
                reports[period].append({
                    'date': date_formatter(date_str),
                    'summary': calamaris.summary,
                    'sections': calamaris.sections,
                })

        return {
            'sections': Calamaris.SECTIONS,
            'data': reports,
        }


ORDERS = "yzafpnµm kMGTPEZY"


def float_format(number):
    """Format a float to a precision of 3, without zeroes or dots"""
    return ("%.3f" % number).rstrip('0').rstrip('.')


def humanize(number):
    """Format a number to engineer scale"""
    order = number and int(math.floor(math.log(abs(number)) / math.log(1000)))
    human_readable = ORDERS.split(" ")[int(order > 0)]
    if order == 0 or order > len(human_readable):
        return float_format(number / (1000 ** int(order)))
    return (
        float_format(number / (1000 ** int(order))) +
        human_readable[int(order) - int(order > 0)])
