import logging
import os
import tarfile
import zipfile

from django.conf import settings
from django.db import models
from django.db.models import signals

from . import calamaris

logger = logging.getLogger('django.db')

BUNDLE_HELP = 'Only .zip and .tar.gz files are recognised'


class CalamarisCollection(models.Model):
    def bundle_upload(inst, fname):
        return 'calamaris/{}'.format(os.path.basename(fname))

    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    bundle = models.FileField('bundle', upload_to=bundle_upload,
                              help_text=BUNDLE_HELP)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey('auth.User')

    class Meta:
        ordering = ('-created_on',)

    @models.permalink
    def get_absolute_url(self):
        return ('calamaris-detail', (self.pk,))

    @property
    def report_dir(self):
        """
        """
        return os.path.join(settings.MEDIA_ROOT, 'calamaris', str(self.pk))

    def reports(self, as_datetime=False):
        """
        """
        collection = calamaris.CalamarisCollection(self.report_dir)
        return collection.get_report(as_datetime)


def unzip_upload(sender, instance=None, created=None, **kwargs):
    try:
        os.makedirs(instance.report_dir)
    except OSError:
        pass

    try:
        with zipfile.ZipFile(instance.bundle.path) as zf:
            zf.extractall(instance.report_dir)
    except (ValueError, IOError, zipfile.BadZipfile):
        pass

    try:
        with tarfile.open(instance.bundle.path) as tf:
            tf.extractall(instance.report_dir)
    except (ValueError, IOError, tarfile.ReadError):
        pass

    for root, dirs, files in os.walk(instance.report_dir):
        if root == instance.report_dir:
            continue
        for f in files:
            os.rename(os.path.join(root, f),
                      os.path.join(instance.report_dir, f))

    if os.path.isfile(instance.bundle.path):
        os.unlink(instance.bundle.path)


signals.post_save.connect(unzip_upload, sender=CalamarisCollection)
