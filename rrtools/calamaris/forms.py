from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Reset, Submit

from .models import CalamarisCollection


class CalamarisCollectionForm(forms.ModelForm):
    class Meta:
        model = CalamarisCollection
        fields = ('title', 'description', 'bundle')

    def __init__(self, *args, **kwargs):
        super(CalamarisCollectionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-8'
        self.helper.layout = Layout(
            Div(*self.Meta.fields, css_class='panel-body'),
            Div(
                Div(
                    Submit('submit', 'Submit'),
                    Reset('reset', 'Reset'),
                    css_class='col-sm-offset-3 col-sm-8'),
                Div(css_class='clearfix'),
                css_class='panel-footer')
        )
