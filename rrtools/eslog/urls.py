from django.conf.urls import patterns, url

from . import views


urlpatterns = patterns('',
    url(r'^$', views.AccessLogView.as_view(), name='access-log-view'),
)
