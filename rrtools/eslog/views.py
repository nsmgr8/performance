from django.views.generic import FormView
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages

from .forms import AccessLogForm
from rrtools.eslog.tasks import load_access_log


class AccessLogView(FormView):
    template_name = 'eslog/access_log.html'
    form_class = AccessLogForm
    success_url = reverse_lazy('access-log-view')

    def form_valid(self, form):
        messages.success(self.request, 'Indexing uploaded log file')
        try:
            file_path = form.cleaned_data['alog_file'].file.name
            label = form.cleaned_data['title']
            load_access_log.delay(file_path=file_path, label=label)
        except AttributeError:
            load_access_log.delay(form=form)
        return super(AccessLogView, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, str(form.errors))
        return super(AccessLogView, self).form_invalid(form)
