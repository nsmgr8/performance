from datetime import datetime
import os

from django import forms
from django.conf import settings

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit

from . import es


class AccessLogForm(forms.Form):
    title = forms.CharField()
    alog_file = forms.FileField(label='Squid Access Log')

    def __init__(self, *args, **kwargs):
        super(AccessLogForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-4'
        self.helper.field_class = 'col-sm-8'
        self.helper.layout = Layout(
            Div('title', 'alog_file', css_class='panel-body'),
            Div(
                Div(
                    Submit('submit', 'Submit'),
                    css_class='col-sm-offset-3 col-sm-8'),
                Div(css_class='clearfix'),
                css_class='panel-footer')
        )

    def handle_upload(self):
        title = self.cleaned_data['title']
        timestamp = datetime.now()
        fname = '{}-{}.log'.format(title, timestamp.isoformat())
        fpath = os.path.join(settings.MEDIA_ROOT, 'squid_access_logs', fname)
        uploaded_file = self.cleaned_data['alog_file']
        with open(fpath, 'w') as dest:
            for chunk in uploaded_file.chunks():
                dest.write(chunk)

        es.load(fpath, title)
