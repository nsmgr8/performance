from datetime import datetime
import re
import urllib.parse
import logging

from elasticsearch import Elasticsearch
import pandas as pd


logger = logging.getLogger('eslog')
is_ascii = re.compile(r'^[ -~]*$')
column_names = ('timestamp', 'elapsed', 'source', 'status', 'reply_size',
                'method', 'url', 'user', 'destination', 'mime_type',
                'request_header', 'response_header')
required_columns = (12, 10)
use_columns = (0, 3, 4, 5, 6, 9, 10, 11)


def header_dict(x):
    unquoted = urllib.parse.unquote(str(x)).splitlines()
    hdrs_list = [y.split(': ') for y in unquoted
                 if ':' in y and
                 is_ascii.match(y) and
                 not y.lower().startswith('cookie') and
                 not y.lower().startswith(' ')]
    return dict([y for y in hdrs_list if len(y) == 2])


def ts_to_dt(x):
    return datetime.fromtimestamp(float(x))


def url_dict(x):
    url = urllib.parse.urlparse(x)
    d = {'scheme': url.scheme, 'netloc': url.netloc, 'path': url.path}
    for part in ('params', 'query', 'fragment'):
        value = getattr(url, part)
        if value:
            d[part] = value
    return d


def status_split(x):
    return dict(zip(('cache', 'http'), x.split('/')))


def load(fname, flabel):
    with open(fname) as f:
        row = f.readline()
        num_columns = len(row.split())
        if num_columns not in required_columns:
            return

    names = column_names[:num_columns]
    if num_columns == 12:
        usecols = use_columns
    else:
        usecols = use_columns[:-2]

    df = pd.read_csv(fname, sep=' ',
                     skipinitialspace=True, error_bad_lines=False,
                     iterator=True, header=None,
                     names=names, usecols=usecols,
                     converters={
                         'url': url_dict,
                         'status': status_split,
                         'request_header': header_dict,
                         'response_header': header_dict,
                         'timestamp': ts_to_dt,
                     })

    es = Elasticsearch()
    for chunk in df:
        for _, row in chunk.iterrows():
            try:
                es.index(index='squid-access-log', doc_type=flabel or 'logs',
                         body=dict(row))
            except:
                logger.exception('Could not index')
