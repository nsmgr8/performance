from celery import shared_task

from . import es


@shared_task
def load_access_log(form=None, file_path=None, label=None):
    """
    """
    if form:
        form.handle_upload()
    elif file_path:
        es.load(file_path, label)
