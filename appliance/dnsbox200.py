import logging
from collections import namedtuple

from pyquery import PyQuery
from netaddr import IPAddress, IPNetwork

from .web_session import WebSession
from .roadrunner import Roadrunner


logger = logging.getLogger('dhcperf')

SUBNET_LIST_URL = '/dhcp/subnet_list.php'


class DNSbox200(Roadrunner):
    """
    DNSbox200 api
    """
    name = 'DNSbox200'

    def __init__(self, addr, port=6700):
        super(DNSbox200, self).__init__(addr)
        self.set_port(port)

    def set_port(self, port=6700):
        with WebSession(self.address) as ws:
            ws.post('/dhcp/configuration_custom.php', data={
                'custom_contents': 'local-port {};'.format(port),
                'action_save': 'Save changes'})

    def clear_subnets(self):
        """
        Remove all unused subnets
        """
        with WebSession(self.address) as ws:
            resp = ws.get(SUBNET_LIST_URL)
            checks = resp.query('.content_table_row-1 input[type=checkbox]')
            if checks:
                data = {'action_delete': 'Remove'}
                for ch in checks:
                    c = PyQuery(ch)
                    data[c.attr('name')] = c.attr('value')
                ws.post(SUBNET_LIST_URL, data=data)

    def set_interface(self, nic):
        """
        Set dhcpd interface
        """
        with WebSession(self.address) as ws:
            ws.post('/dhcp/options_edit.php',
                    data={'network-adapter': nic,
                          'action_save': 'Save changes'})

    def dhcpd_setup(self, netmask):
        """
        Setup dhcpd pool

        @returns network size
        """
        Pool = namedtuple('Pool', ['low', 'high', 'size'])
        nw = IPNetwork('10.10.0.0/{}'.format(netmask))
        self.pool = Pool(str(IPAddress(nw.first)), str(IPAddress(nw.last)),
                         nw.size)

        with WebSession(self.address) as ws:
            resp = ws.get(SUBNET_LIST_URL)
            action_url = resp.query('.content_table_row-2 a').attr('href')
            action_url = '/dhcp/{}'.format(action_url)
            ws.get(action_url)
            ws.post('/dhcp/subnet_edit.php',
                    data={'receptacle_name': '10.0.0.0',
                          'subnet_mask': '255.0.0.0',
                          'parameter_value_default-lease-time': '30',
                          'parameter_value_max-lease-time': '30',
                          'parameter_value_min-lease-time': '30',
                          'pooldata_0_lowAddress_0': self.pool.low,
                          'pooldata_0_highAddress_0': self.pool.high,
                          'action_save': 'Save changes'})
        return nw.size
