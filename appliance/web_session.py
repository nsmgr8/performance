import time
import logging
import functools
from datetime import datetime

import requests
try:
    requests.packages.urllib3.disable_warnings()
except AttributeError:
    # older versions of requests don't have the `packages` subpackage
    pass
from pyquery import PyQuery

DEFAULT_TIMEOUT = 60

logger = logging.getLogger('rrdev.web_session')


class WebSession(object):
    def __init__(self, host=None,
                 username="admin", password="password",
                 connect_timeout=DEFAULT_TIMEOUT, verbose=False):
        """
        Get a login session cookie, used for subsequent requests to this host
        """
        self.host = host
        self.username = username
        self.password = password
        if verbose:
            logger.setLevel(logging.DEBUG)
        self._session = None
        if host is not None:
            self.open(host, username, password,
                      connect_timeout=connect_timeout)

    def open(self, host=None, username=None, password=None,
             connect_timeout=60):
        """
        @param host: hostname/ip address to connect to.
        @param username: username to use
        @param password: password to use
        @param connect_timeout: keep trying for up to this long
        """
        if host is not None:
            self.host = host
        if username is not None:
            self.username = username
        if password is not None:
            self.password = password
        self._session = requests.session()
        self._session.verify = False
        end_time = time.time() + connect_timeout + 0.1
        data = {'username': self.username,
                'password': self.password,
                'from_page': '/?'}
        resp = None
        retry_backoff = 2
        while time.time() < end_time:

            retry_backoff = min(30, int(retry_backoff + retry_backoff / 2))

            # deal with delays first to avoid pointless delay
            # in the case where the end_time has been reached anyway.
            if resp is not None and resp.status_code == 503:
                logger.info('Error 503: Retrying in %d seconds'
                            % int(retry_backoff))
                resp = None
                time.sleep(retry_backoff)

            utcdate = datetime.utcnow()
            data['client_utc_datetime'] = utcdate.strftime('%Y-%m-%dT%H:%M:%S')
            try:
                resp = self.post('/auth/login_submit', data=data)
            except requests.exceptions.RequestException:
                logger.info('Error: Retrying in %d seconds'
                            % int(retry_backoff))
                time.sleep(retry_backoff)
                continue
            if resp.ok:
                logger.info('Succeeded in opening session')
                break
            if resp.query('#sync_time_now') and 'sync_time_now' not in data:
                logger.info('setting time sync flag on login')
                data['sync_time_now'] = 'on'
                continue
            elif 'sync_time_now' in data:
                del data['sync_time_now']  # don't keep it carried over

        else:
            # we never broke free and just timed out :-(
            self._session = None
            raise IOError("Could not create login session - timed out")

    @property
    def session(self):
        if self._session is None:
            raise RuntimeError('No session currently open')
        return self._session

    def close(self):
        try:
            self.post('/auth/logout')
        finally:
            self._session = None

    def __enter__(self):
        if self._session is None:
            self.open()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self.close()
        except Exception:
            logger.warning('Logout failed')

    def _url_transform(self, relurl):
        absurl = 'https://{host}{relurl}'.format(host=self.host, relurl=relurl)
        return absurl

    def _querify(fn):
        @functools.wraps(fn)
        def _(self, relurl, **k):
            resp = fn(self, relurl, **k)

            def query(*o, **k):
                # This implements on-demand PyQuery evaluation
                querier = PyQuery(resp.text)
                result = querier(*o, **k)
                # Next time reuse the same querier.
                resp.query = querier
                return result
            resp.query = query

            return resp
        return _

    @_querify
    def post(self, relurl, **k):
        return self.session.post(self._url_transform(relurl), **k)

    @_querify
    def get(self, relurl, **k):
        return self.session.get(self._url_transform(relurl), **k)

    def head(self, relurl, **k):
        return self.session.head(self._url_transform(relurl), **k)

    def get_query(self, relurl, **k):
        return PyQuery(self.get(relurl, **k).text)

    def post_query(self, relurl, **k):
        return PyQuery(self.post(relurl, **k).text)
