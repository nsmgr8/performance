import logging
import time
import os
import re
import json
import subprocess
from tempfile import NamedTemporaryFile
from collections import namedtuple

import sh

from remote import ssh
from .web_session import WebSession

logger = logging.getLogger('polyrun')
Version = namedtuple('Version', 'major minor')


class API(object):
    def __init__(self, appliance):
        self.appliance = appliance

    def __getattr__(self, name):
        def forwarder(*args, **kwargs):
            return self.__run_api(name, *args, **kwargs)
        return forwarder

    def __run_api(self, method, *args, **kwargs):
        api_runner = 'api_runner.py'
        api_json = 'api_data.json'

        script_path = os.path.join(os.path.dirname(__file__), 'data',
                                   api_runner)
        self.appliance.put_file(script_path, api_runner)

        with NamedTemporaryFile('w') as data_file:
            data_file.write(json.dumps({'method': method, 'args': args,
                                        'kwargs': kwargs}))
            data_file.flush()
            self.appliance.put_file(data_file.name, api_json)

        output = self.appliance.run('python {} {}'.format(api_runner,
                                                          api_json))
        self.appliance.run('rm {}'.format(api_json))
        return output


class Roadrunner(object):
    """
    Roadrunner api and ssh connection manager
    """
    name = 'Appliance'

    def __init__(self, addr):
        self.address = addr
        self.api = API(self)
        self.version()

    def version(self):
        out, err = self.run('cat /etc/firmware_version_full')
        if not out or err:
            return
        self.name, version_full, _ = out.split(' ', 2)
        try:
            marketing, _ = version_full.split('-', 1)
            marketing = map(int, marketing.split('.')[:2])
        except ValueError:
            marketing = [4, 5]

        return Version(*marketing)

    def run(self, command):
        with ssh.SSHConnection(address=self.address, username='admin',
                               password='password') as ssh_client:
            return ssh_client.run(command)

    def cli(self, command, strip_quotes=False):
        out, err = self.run('appliance_cli -c {}'.format(command))
        if out.startswith('*** Unknown syntax:'):
            raise AttributeError('Method does not exist')
        return out.strip("'") if strip_quotes else out

    def get_file(self, remote_path, local_path):
        with ssh.SSHConnection(address=self.address, username='admin',
                               password='password') as ssh_client:
            return ssh_client.get(remote_path, local_path)

    def put_file(self, local_path, remote_path):
        with ssh.SSHConnection(address=self.address, username='admin',
                               password='password') as ssh_client:
            return ssh_client.put(local_path, remote_path)

    def sudo_cmd(self, cmd, say_yes=False):
        with ssh.SSHConnection(address=self.address, username='admin',
                               password='password') as ssh_client:
            ask_pass_path = os.path.join(os.path.dirname(__file__),
                                         'data', 'ask_pass.sh')
            ask_pass = '/home/admin/ask_pass.sh'
            ssh_client.put(ask_pass_path, ask_pass)
            ssh_client.run('chmod u+x {}'.format(ask_pass))
            yes = 'yes | ' if say_yes else ''
            return ssh_client.run('{}SUDO_ASKPASS={} sudo -A {}'
                                  .format(yes, ask_pass, cmd))

    def restart(self):
        logger.info('Restarting {}'.format(self.name))
        self.api.add({'__table__': 'action_restart',
                      'when_do': 'Immediate',
                      'time': int(time.time()),
                      'type': 'Restart',
                      'reason': 'polyrun'})
        self.has_rebooted()

    def has_rebooted(self):
        while self._is_up():
            time.sleep(1)

        while self._is_down():
            logger.info('Waiting for {} reboot'.format(self.name))
            time.sleep(10)

        while True:
            try:
                logger.info('Connecting to {} API'.format(self.name))
                self.api.ping()
                break
            except:
                time.sleep(10)

    def _ping(self):
        try:
            subprocess.check_call(['ping', '-c1', '-W2', self.address])
        except:
            return False
        else:
            return True

    def _is_up(self):
        return self._ping()

    def _is_down(self):
        return not self._ping()

    def get_results(self, path):
        logger.info('Getting Appliance data')
        self.cli('persist_rrd_files')
        rrd_tgz = os.path.join(path, 'rrd.tgz')
        self.get_file('/var/conf/collectd/rrd_persist.tgz', rrd_tgz)

        sh.rm('-rf', os.path.join(path, 'localhost'))
        sh.tar('-zxf', rrd_tgz, '-C', path)
        sh.rm(rrd_tgz)

        diag_zip = '{0}/diag.zip'.format(path)
        diag_dir = os.path.join(path, 'diag')
        with WebSession(self.address) as ws, open(diag_zip, 'wb') as f:
            resp = ws.get('/system/diagnostic_info')
            f.write(resp.content)
        sh.mkdir('-p', diag_dir)
        sh.unzip('-o', diag_zip, '-d', diag_dir)

    def clear_table(self, table):
        results = self.cli('get_item {} id'.format(table))
        ids = [int(x) for x in re.findall(r'\d+', results)]

        records = [
            {'__table__': table,
             'id': id,
             'meta_is_deleted': True}
            for id in ids
        ]
        self.api.set(records)
