import sys
import json

from pyroserver import connector

api = connector.Connector()

cmd = json.load(open(sys.argv[1]))
method = cmd['method']
args = cmd.get('args', [])
kwargs = cmd.get('kwargs', {})

func = getattr(api, method)
result = func(*args, **kwargs)
try:
    print(json.dumps(result))
except:
    print(result)
