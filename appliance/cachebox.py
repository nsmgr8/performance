import logging
import json
import time
import os
import tempfile
from string import Template

from .roadrunner import Roadrunner
from .web_session import WebSession


logger = logging.getLogger('polyrun')
AVAILABLE_CACHE_ENGINES = ('squid2', 'squid3')


class CACHEbox(Roadrunner):
    """
    CACHEbox api
    """
    name = 'CACHEbox'

    def initialise(self):
        logger.info('Initialising CACHEbox')
        self.api.cachebox_delete_all_cached_objects()
        self.restart()
        self.wait_for_cache_engine()

    def restart_squid(self):
        logger.info('Restarting Squid')
        # stop squid; wait for watchdog to restart it
        self.api.service_stop('squid')
        # wait a bit for squid to stop listening to new connections,
        # otherwise it might accept one before starting its shutdown
        time.sleep(2)
        self.wait_for_cache_engine()

    def wait_for_cache_engine(self):
        logger.info('Checking whether cache engine is running')
        start = time.time()
        while True:
            out, err = self.api.get_cache_engine_status()
            if err and not out:
                out, err = self.api.cachebox_get_squid_status()

            if out.strip('"').strip("'").lower().startswith('running'):
                break

            now = time.time()
            logger.info('Waited {} seconds for cache engine startup'
                        .format(round(now - start, 1)))
            time.sleep(5)

        logger.info('Cache engine is running')

    def set_cache_engine(self, engine):
        if engine not in AVAILABLE_CACHE_ENGINES:
            if engine:
                logger.error('{} is not supported. Using default cache engine'
                             .format(engine))
            return

        try:
            out, err = self.api.get_cache_engine()
            current_engine = out.strip('"')
            if engine != current_engine:
                logger.warning('Changing cache engine to {}'.format(engine))
                self.sudo_cmd('/opt/sbin/change-cache-engine {}'
                              .format(engine), say_yes=True)
                self.has_rebooted()
        except:
            logger.exception('Cannot change cache engine')

    def get_results(self, path):
        super(CACHEbox, self).get_results(path)

        logger.info('Getting CACHEbox data')

        conf_path = os.path.join(path, 'squid_conf.txt')
        with WebSession(self.address) as ws, open(conf_path, 'w') as conf_file:
            resp = ws.get('/cache/view_squid_conf')
            conf_file.write(resp.text)

    def update_routes(self, polygraphs):
        logger.info('Updating CACHEbox routes')
        table = 'static_routes'
        self.clear_table(table)

        records = []
        for poly in polygraphs:
            network = '10.{}.0.0/16'.format(poly.split('.')[-1])
            gateway = poly.split('@')[-1]
            records.append({
                '__table__': table,
                'description': network.rsplit('.', 2)[0],
                'subnet': network,
                'gateway': gateway,
            })
        self.api.set(records)

    def setup_reverse(self, servers):
        logger.info("Setting up reverse cache for %s", servers)
        table = 'reverse_backend'
        self.clear_table(table)

        records = []
        for i, server in enumerate(servers, 1):
            records.append({
                "__table__": table,
                "ip_address": server,
                "port": 80,
                "name": 'server{}'.format(i),
            })
        self.api.set(records)

    def setup_cdn(self, zone):
        logger.info('Setting up {} CDN'.format(zone))
        try:
            current_engine = self.cli('get_cache_engine', strip_quotes=True)
        except:
            current_engine = 'squid2'

        cb_version = self.version()
        if cb_version < (4, 8):
            cdn_version = '-pre-refresh'
        else:
            cdn_version = ''

        template_name = '{}{}.ini'.format(current_engine, cdn_version)
        tpath = os.path.join(os.path.dirname(__file__), 'data', template_name)
        with open(tpath, 'r') as f:
            s = Template(f.read())
            result = s.substitute(BENCH=zone)

        bfile = tempfile.mktemp('-bench', 'cdn-')
        bench_ini = '/home/admin/bench.ini'
        with open(bfile, 'w') as f:
            f.write(result)

        self.put_file(bfile, bench_ini)

        return self.cli('import_cdn {}'.format(bench_ini))

    def enable_cdn(self, zone, enable=True):
        logger.info('Enable {} CDN: {}'.format(zone, enable))
        if enable:
            self.setup_cdn(zone)

        out, err = self.api.get('cdn', match=('cdn_id', zone))
        try:
            records = json.loads(out)
        except ValueError:
            out, err = self.apii.get('cdn', match=('cdn_id', zone), ro=False)
            records = json.loads(out)

        if records:
            records[0].update({'__table__': 'cdn', 'enabled': enable})
            self.api.set(records)
