#!/bin/bash

set -e

PERFORMANCE=$(dirname $(dirname ${BASH_SOURCE[0]}))
LOGFILE=${PERFORMANCE}/assets/logs/gunicorn.log
ACCESSLOGFILE=${PERFORMANCE}/assets/logs/access.log

GUNICORN_BIN=${1:-${PERFORMANCE}/venv/bin/gunicorn}
cd $PERFORMANCE
$GUNICORN_BIN \
	--bind 127.0.0.1:8000 \
	--error-logfile=$LOGFILE \
	--access-logfile=$ACCESSLOGFILE \
	performance.wsgi:application
