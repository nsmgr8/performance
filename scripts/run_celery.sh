#!/bin/bash

set -e

PERFORMANCE=$(dirname $(dirname ${BASH_SOURCE[0]}))
LOGFILE=${PERFORMANCE}/assets/logs/celery-%N.log
PIDFILE=${PERFORMANCE}/assets/celery-%N.pid

OPTIONS="\
--app=performance \
--hostname=performance.coventry \
--events \
--loglevel=info \
--logfile=${LOGFILE} \
--pidfile=${PIDFILE} \
"

if [[ -z "${CELERY_BIN}" ]]; then
    CELERY_BIN=${PERFORMANCE}/venv/bin/celery
fi

start_celery() {
	cd $PERFORMANCE
	$CELERY_BIN multi start worker $OPTIONS
}

stop_celery() {
	cd $PERFORMANCE
	$CELERY_BIN multi stopwait worker $OPTIONS
}

restart_celery() {
	cd $PERFORMANCE
	$CELERY_BIN multi restart worker $OPTIONS
}

case "$1" in
    start)
        start_celery
    ;;
    stop)
        stop_celery
    ;;
    restart)
        restart_celery
    ;;
    *)
        echo "Usage: run_celery.sh {start|stop|restart}"
        exit 64
    ;;
esac
