#!/bin/bash

set -e

PERFORMANCE=$(dirname $(dirname ${BASH_SOURCE[0]}))
cd $PERFORMANCE/angular
node socket.io.server
