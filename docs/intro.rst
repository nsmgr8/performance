Performance setup
=================

Performance infrastructure is setup in its own sub-network. Only performance
controller (performance.coventry) box has the outer world connection. The
network is as follows:

Network
   10.0.0.0/16

Main controller
   172.16.1.2, 10.0.0.10, http://performance.coventry/

Default route
   10.0.0.10

Time server
   10.0.0.10

Proxy for accessing Roadrunner UI
   172.16.1.2:3128

Polygraph servers
   10.0.0.14 (polysrv1),
   10.0.0.15 (polysrv2),
   10.0.0.18 (polysrv3),
   10.0.0.19 (polysrv4)

Polygraph clients
   10.0.0.12 (polyclt1),
   10.0.0.13 (polyclt2),
   10.0.0.16 (polyclt3),
   10.0.0.17 (polyclt4)

Monitoring tools
   http://performance.coventry/monitor/,
   http://sentry.coventry/

See also
   http://performance.coventry/resources/

Concepts
========

Performance controller site is built on three different units. These units are
namely ``Resource`` :py:class:`cf. <performance.models.Resource>`,
``Test run`` :py:class:`cf. <performance.models.TestRun>` and
``Script`` :py:class:`cf. <polygraph.polyrun.Polyrun>`.

The units are as follows:

Resource
--------

Resource is the main basic unit of the performance system. A test run should
require a number of resource units to perform its tasks. For example, a polyrun
test requires polygraph server, client and a cachebox. All these machines are
defined as resource in the system.

A resource contains title, type, IP address and description. Although there is
no restriction, a resource should not change its type and IP once defined.
Some resource types are hard-coded into the system. These are:

#. **cachebox** (for CACHEbox machines)

#. **dnsbox** (for DNSbox machines)

#. **polygraph** (for Polygraph Server/Client machines)

These types are required for running Polyrun and dhcperf scripts.

When a script requires a resource to perform its task, the resource is marked
as *In use*. In this state the resource cannot be used with any other script
that requires it.

There are some pre-defined group of resources used by scripts. For example,
polyrun script uses 4 (four) polygraph machines as ``Polyrig 1`` setup which
includes ``polysrv1``, ``polysrv2``, ``polyclt1``, ``polyclt2``.

Test run
--------

TestRun is the main unit that is more interesting for a user. A test run is
a definition of a performance test that holds all information about a single
test. A well-defined test should have a description which describes what the
test is about, the goal of the test, etc. A test-run may have tags so that it
can be compared with other test-runs with same tags.

It should have the initial parameters defined and after completion of the
execution the outcomes. A test can run scripts already included in the system
(e.g, polyrun, dhcperf) or can have its own scripts running somewhere else.

A successful test run should include a report. A failed test may include the
reasons for the failure in the report.

Script
------

The system has pre-defined scripts that can be automatically run for a test.
These pre-defined tests require some setup in the performance network. Given
appropriate resources and inputs the scripts can run in the background. If the
script returns no error the test run is marked successful, while on any error
on running the script will result in test run to be marked failed.

A running script can be reset while its running. In that case, test run will
become available to run another script.
