# -*- coding: utf-8 -*-

import sys
import os
import subprocess

import sphinx_rtd_theme


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "performance.settings")
sys.path.insert(0, os.path.dirname(os.path.abspath('.')))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.doctest',
    'sphinx.ext.todo',
    'sphinx.ext.viewcode',
]

source_suffix = '.rst'
master_doc = 'index'

project = u'performance'
copyright = u'2013, M Nasimul Haque'
release = subprocess.check_output('git symbolic-ref HEAD'.split()).split('/')[-1].strip()
version = subprocess.check_output('git rev-parse HEAD'.split())[:5].strip()
today_fmt = '%B %d, %Y'

exclude_patterns = ['_build']
add_module_names = True
pygments_style = 'sphinx'

html_theme = 'sphinx_rtd_theme'
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

latex_elements = {
    'papersize': 'a4paper',
}

latex_documents = [
    ('index', 'performance.tex', u'performance Documentation',
     u'M Nasimul Haque', 'manual'),
]

man_pages = [
    ('index', 'performance', u'performance Documentation',
     [u'M Nasimul Haque'], 1)
]

texinfo_documents = [
    ('index', 'performance', u'performance Documentation',
     u'M Nasimul Haque', 'performance', 'One line description of project.',
     'Miscellaneous'),
]

htmlhelp_basename = 'performancedoc'

epub_title = u'performance'
epub_author = u'M Nasimul Haque'
epub_publisher = u'M Nasimul Haque'
epub_copyright = u'2013, M Nasimul Haque'
