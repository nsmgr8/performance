.. performance documentation master file, created by
   sphinx-quickstart on Wed Oct  2 21:13:26 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to performance's documentation!
=======================================

This document describes the concept, usage and development of
http://performance.coventry/.

Contents:

.. toctree::
   :maxdepth: 2

   Introduction <intro>
   Usage <usage>
   Development <install>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

