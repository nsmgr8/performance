Development
============

Performance site is installed and running at http://performance.coventry/. The
source code is available at ssh://prodev.coventry/srv/git/performance.git/.

During development it can be run via django development server. Following are
the steps for running it in a development machine.

Requirements
------------

#. Python 2.7
#. Virtualenv (recommended, so that the dependencies for the project do not
   mess up OS python packages)
#. Postgres (by default, sqlite3 is used)
#. Several other packages might be required depending on what python packages
   will be installed

Steps
-----

#. Clone the repository:

   .. code-block:: sh

      $ git clone ssh://prodev.coventry/srv/git/performance.git

#. Create a virtualenv for installing dependencies:

   .. code-block:: sh

      $ cd performance
      $ virutalenv venv
      $ source venv/bin/activate

#. Install the dependencies:

   .. code-block:: sh

      $ pip install -r requirements.txt

#. If pip install fails, check you have required OS packages installed.

#. Setup the database:

   .. code-block:: sh

      $ python manage.py syncdb --all
      $ python manage.py migrate --fake
      $ python manage.py collectstatic

#. Run the development server:

   .. code-block:: sh

      $ python manage.py runserver 8001

#. You also need to run celery worker. Open another terminal and cd into the
   source folder. Now run the following command:

   .. code-block:: sh

      $ python manage.py celery worker -n performance.coventry -l info

#. Now point your browser at http://localhost:8001/

Deployment
==========

The site is deployed at `perf@performance.coventry/performance`. Update the
codebase via `git pull origin master`. Then restart the upstart jobs. There are
two upstart jobs for running performance site, called `performance-gunicorn`
and `performance-celery`. Hence the steps for updating the deployment are:

   .. code-block:: sh

      $ sudo su perf
      $ cd ~/performance
      $ git pull origin master
      $ exit
      $ sudo restart performance-gunicorn
      $ sudo restart performance-celery
