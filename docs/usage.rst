Usage
=====

Pages
~~~~~

Dashboard/Home page
-------------------

The home page shows a dashboard of current status of the performance rig. It
shows currently running tests, the upcoming queue, idle/available resources
list and recently completed tests.

Test run list
-------------

This page lists all the test runs grouped by their current state. Test runs
have five states: *Waiting*, *Scheduled*, *Running*, *Successful* and *Failed*.
This page also includes link to the new test run form to create tests.

Test run detail
---------------

Test run detail has several tabs depending on the state and type of script run.

View tab shows the description, parameters, report, current status and progress
(if running). Edit tab allows to update the test description, report,
parameters. Result tab gives the overview of the executed script output. For
a polyrun test, there is a squid.conf tab that shows the squid config during
the run. There are log and rrd tabs which shows log files and rrd graphs during
the test.

Resource list
-------------

This is a list of resources known to the performance setup. It shows all
resources with their current status.

Resource detail
---------------

Resource detail page shows the description of the resource. It also lists all
the test runs that used it. Again, test runs are grouped by their states.

Analyse/compare page
--------------------

This view allows users to compare different sets of test-runs in a single
table. A user can select as many tags from the drop-down list and all
test-runs common to those tags are listed in a table. Note that, this only
lists successful test-runs.

Workflow
~~~~~~~~

#. Create all the resources required by the test (if those do not exist yet).
   Navigate to ``Resources`` > ``Create a resource``.

#. Create a test run by navigating to ``Test Runs`` > ``Create a test run``.

#. Choose a script from the list of scripts in the test run detail view. Click
   on the chosen script and if presented enter the script inputs and save.

#. Script will start running in the background. Wait for the script to be
   completed. There will be status bubbles while it's running with the current
   status.

#. If the script completes its execution successfully, the test will be marked
   successful and closed. If the script fails to finish, the test will also be
   closed with failed status.

#. After a test run is completed update the output parameters from the script
   results. Write a report on the test run. If failed and you know the reason
   please put it in the report. If you don't know the reason of the failure,
   you may open a ticket for investigation.

#. A running test can be reset by clicking the reset button in the test run
   detail view. It will stop the associated script and make the test run
   available for another run.

#. A successful/failed test can also be re-run by clicking reset button next to
   Clone button. However, it is recommended that testrun should not be reset.
   Use cloning instead.

**Tips**
   - All the drop-downs in the UI have auto-completion. Start typing and select
     by clicking or arrow keys and <ENTER>.

**Known issues**
   - Polyrig 2 is not available yet, do not select it to run polyrun script.

   - Bridge mode test setup does not work out of the box. It requires manual
     setup of the network so that CACHEbox is physically in between polygraph
     servers and clients.
